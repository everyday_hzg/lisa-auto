package com.zhiche.idc.admin;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.lisa.auto.entity.C3ReportCarInfo;
import com.lisa.auto.entity.c3.requst.RemoteControlResultRequest;
import com.lisa.auto.entity.c3.requst.ScRemoteControlRequest;
import com.lisa.auto.enums.C3UrlEnum;
import demo.C3ApiDemo;
import demo.request.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = C3APITests.class)
public class C3APITests {
    private static final Logger LOGGER = LoggerFactory.getLogger(C3APITests.class);

    @Value("${c3.url}")
    private String c3url;
    @Value("${c3.appKey}")
    private String appKey;
    @Value("${c3.secretKey}")
    private String secretKey;

    @Test
    public void getModels() {
        try {
            String url = "http://c3uat.timanetwork.cn/ccf/v2/manage/models/list";
            //VehicleCreateRequest request= new VehicleCreateRequest();
            //ModelListRequest request = new ModelListRequest();
            ModelRequest request = new ModelRequest();
//            request.setVin("LXWAEPG22JH000971");
//            request.setPlateLicenseNo("粤B00000");
//            request.setModelNo("MD0000000000000000001673806388");
//            request.setGateWayNo("GW0000000000000000001504215038");
            // request.setSeriesNo("MD0000000000000000001673806388");
            request.setModelNo("MD0000000000000000001673806388");
            request.setAppKey("JiRChvAFDCEFgjg");
            String requstJson = JSON.toJSONString(request);
            String secretKey = "onxvpJKyRUCgkCk";
            String result = C3ApiDemo.get(url, requstJson, secretKey);
            System.out.println("返回结果为:" + result);
        } catch (Exception e) {
            LOGGER.error(" ERROR {}", e.getMessage());
        }

    }

    @Test
    public void contextLoads() {
        try {
            String url = "http://c3uat.timanetwork.cn/ccf/v2/manage/vehicles";
            VehicleCreateRequest request = new VehicleCreateRequest();
            request.setVin("LXWAEPG22JH000971");
            request.setPlateLicenseNo("粤B00000");
            request.setModelNo("MD0000000000000000001673806388");
            request.setGateWayNo("GW0000000000000000001504215038");
            request.setAppKey("JiRChvAFDCEFgjg");
            String requstJson = JSON.toJSONString(request);
            String secretKey = "onxvpJKyRUCgkCk";
            String result = C3ApiDemo.post(url, requstJson, secretKey);
            System.out.println("返回结果为:" + result);
        } catch (Exception e) {
            LOGGER.error(" ERROR {}", e.getMessage());
        }

    }

    @Test
    public void createVehicle() {
        try {
            String url = "http://c3uat.timanetwork.cn/ccf/v2/manage/vehicles";
            VehicleCreateRequest request = new VehicleCreateRequest();
            //CarInfoRequest request = new CarInfoRequest();
            request.setVin("LXWAEPG22JH000971");
            request.setAppKey("JiRChvAFDCEFgjg");
            String secretKey = "onxvpJKyRUCgkCk";
            String requstJson = JSON.toJSONString(request);
            String result = C3ApiDemo.get(url, requstJson, secretKey);
            System.out.println("返回结果为:" + result);
        } catch (Exception e) {
            LOGGER.error(" ERROR {}", e.getMessage());
        }

    }

    @Test
    public void updateVehicle() {
        try {
            String url = "http://c3uat.timanetwork.cn/ccf/v2/manage/vehicles";
            //VehicleCreateRequest request= new VehicleCreateRequest();
            //CarInfoRequest request = new CarInfoRequest();
            VehicleUpdateRequest request = new VehicleUpdateRequest();
            request.setVin("LXWAEPG22JH000971");
            request.setColor("black");
            request.setPlateLicenseNo("粤B00001");
            request.setModelNo("MD0000000000000000001673806388");
            request.setAppKey("JiRChvAFDCEFgjg");
            String secretKey = "onxvpJKyRUCgkCk";
            String requstJson = JSON.toJSONString(request);
            String result = C3ApiDemo.put(url, requstJson, secretKey);
            System.out.println("返回结果为:" + result);
        } catch (Exception e) {
            LOGGER.error(" ERROR {}", e.getMessage());
        }

    }


    @Test
    public void deleteVehicle() {
        try {
            String url = "http://c3uat.timanetwork.cn/ccf/v2/manage/vehicles";
            //VehicleCreateRequest request= new VehicleCreateRequest();
            //CarInfoRequest request = new CarInfoRequest();
            //VehicleUpdateRequest request = new VehicleUpdateRequest();
            VehicleUpdateRequest request = new VehicleUpdateRequest();
            request.setVin("LEFAFCG25KHN82646");
            request.setAppKey("JiRChvAFDCEFgjg");
            String secretKey = "onxvpJKyRUCgkCk";
            String requstJson = JSON.toJSONString(request);
            String result = C3ApiDemo.delete(url, requstJson, secretKey);
            System.out.println("返回结果为:" + result);
        } catch (Exception e) {
            LOGGER.error(" ERROR {}", e.getMessage());
        }

    }

    @Test
    public void getCarInfo() {
        try {
            String url = "http://c3uat.timanetwork.cn/ccf/v1/manage/report/carinfo";
            //VehicleCreateRequest request= new VehicleCreateRequest();
            CarInfoRequest request = new CarInfoRequest();
            //VehicleUpdateRequest request = new VehicleUpdateRequest();
            //VehicleUpdateRequest request = new VehicleUpdateRequest();
            request.setVin("LXWAEPG22JH000971");
            request.setAppKey("JiRChvAFDCEFgjg");
            String secretKey = "onxvpJKyRUCgkCk";
            String requstJson = JSON.toJSONString(request);
            String result = C3ApiDemo.get(url, requstJson, secretKey);
            System.out.println("返回结果为:" + result);
        } catch (Exception e) {
            LOGGER.error(" ERROR {}", e.getMessage());
        }

    }

    @Test
    public void getVehicleHistoryTrail() {
        try {
            String url = "http://c3uat.timanetwork.cn/data-service/manage/history-service/getVehicleHistoryTrail";
            //VehicleCreateRequest request= new VehicleCreateRequest();
            //CarInfoRequest request = new CarInfoRequest();
            HistoryCarInfoRequest request = new HistoryCarInfoRequest();
            //VehicleUpdateRequest request = new VehicleUpdateRequest();
            //VehicleUpdateRequest request = new VehicleUpdateRequest();
            request.setVin("LXWAEPG22JH000971");
            request.setStartTime(1576252800000L);
            request.setEndTime(1576645200000L);
            request.setAppKey("JiRChvAFDCEFgjg");
            String secretKey = "onxvpJKyRUCgkCk";
            String requstJson = JSON.toJSONString(request);
            String result = C3ApiDemo.get(url, requstJson, secretKey);
            System.out.println("返回结果为:" + result);
        } catch (Exception e) {
            LOGGER.error(" ERROR {}", e.getMessage());
        }

    }

    @Test
    public void serviceProvider() {
        try {
            String url = "http://c3uat.timanetwork.cn/basebiz/v2/manage/serviceProvider";
            ServiceProviderInterfaceCreateRequest request = new ServiceProviderInterfaceCreateRequest();
            request.setInterfaceUrl("https://lisa.unlcn.com/idc-admin/dams/v1/report");
            request.setSubscribe(true);
            request.setType("REPORT_MONITOR_EVENT");
            request.setAppKey("JiRChvAFDCEFgjg");
            String secretKey = "onxvpJKyRUCgkCk";
            String requstJson = JSON.toJSONString(request);
            String result = C3ApiDemo.post(url, requstJson, secretKey);
            System.out.println("返回结果为:" + result);
        } catch (Exception e) {
            LOGGER.error(" ERROR {}", e.getMessage());
        }

    }

    @Test
    public void serviceProvider1() {
        try {
            Date date = new Date();//获取当前时间    
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.add(Calendar.DATE, -1);//当前时间减去一年，即一年前的时间
            Date time = calendar.getTime();
            System.out.println(calendar);
        } catch (Exception e) {
            LOGGER.error(" ERROR {}", e.getMessage());
        }

    }

    /**
     * 取得制造商列表数据
     */
    @Test
    public void getMakersList() throws ParseException {
        SimpleDateFormat sdf =  new SimpleDateFormat("yyyy-MM");
        Calendar ct=Calendar.getInstance();
        ct.setTime(sdf.parse("2019-05"));
        System.out.println(Calendar.MONTH);
    }

    /**
     * 外部接口获取品牌列表数据并存储到数据库
     */
    @Test
    public void getBrandsList() {
        try {
            String url = c3url + C3UrlEnum.BRANDSURL.getUrl();
            BrandListRequest request = new BrandListRequest();
            request.setAppKey(appKey);
            request.setMakerNo("MK1NQBK49X281Y0000000000000000");
            String requestJson = JSONObject.toJSONString(request);
            String result = C3ApiDemo.get(url, requestJson, secretKey);
            System.out.println("result:" + result);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * 外部接口获取车系列表数据并存储到数据库
     */
    @Test
    public void getCarSeriesList() {
        try {
            String url = c3url + C3UrlEnum.SERIESURL.getUrl();
            SeriesListRequest request = new SeriesListRequest();
            request.setAppKey(appKey);
            request.setBrandNo("bd1NQBK4A4JNKM0000000000000000");
            String requestJson = JSONObject.toJSONString(request);
            String result = C3ApiDemo.get(url, requestJson, secretKey);
            System.out.println("result:" + result);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * 车辆远程控制
     */
    @Test
    public void remoteControl() {
        try {
            String url = c3url + C3UrlEnum.CONTROLURL.getUrl();
            ScRemoteControlRequest request = new ScRemoteControlRequest();
            request.setAppKey(appKey);
            request.setOperation("1");
            request.setOperationType("LIGHT_FLASH");
//            request.setVin("LXWAEPG22JH000802");
//            request.setVin("LXWAEPG22JH000801");
//            request.setVin("LXWAEPG22JH000800");
//            request.setVin("LXWAEPG22JH000900");
            request.setVin("LXWAEPG22JH000971");
//            request.setVin("LJXBMDJD3JT065190");
            String requestJson = JSONObject.toJSONString(request);
            String result = C3ApiDemo.post(url, requestJson, secretKey);
            System.out.println("result:" + result);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //{"returnSuccess":true,"returnErrCode":null,"returnErrMsg":null,"operationId":"2407"}
    }

    /**
     * 车辆远程控制
     */
    @Test
    public void getControlResult() {
        try {
            String url = c3url + C3UrlEnum.RESULTURL.getUrl();
            RemoteControlResultRequest request = new RemoteControlResultRequest();
            request.setAppKey(appKey);
            request.setOperationId("2407");
            String requestJson = JSONObject.toJSONString(request);
            String result = C3ApiDemo.get(url, requestJson, secretKey);
            System.out.println("result:" + result);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test(){
        String json = "{\"sign\":\"348FCAB5E66243C6ADEBDA1E5E6ACD78\",\"appKey\":\"msYjn7g6Ua1tK97\",\"isCurrentData\":true,\"list\":\"[{\\\"acceleratorPedal\\\":0.0,\\\"acquisitionTime\\\":1584600759206,\\\"batteryBoxMismatchFault\\\":0,\\\"batteryOverCharge\\\":0,\\\"batteryPerformance\\\":0,\\\"batteryTemp\\\":[{\\\"val\\\":\\\"1\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"1\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"606.8\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"0.0\\\",\\\"sts\\\":0},{\\\"val\\\":184,\\\"sts\\\":0},{\\\"val\\\":\\\"1\\\",\\\"sts\\\":0},{\\\"val\\\":184,\\\"sts\\\":0},{\\\"val\\\":\\\"3.284\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.281\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.281\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.281\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.282\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.282\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.282\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.282\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.282\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.283\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.281\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.282\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.284\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.28\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.281\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.281\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.28\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.281\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.281\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.28\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.281\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.28\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.28\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.28\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.281\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.281\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.282\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.282\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.281\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.282\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.282\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.281\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.282\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.282\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.281\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.282\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.281\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.281\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.281\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.281\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.281\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.281\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.281\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.28\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.281\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.28\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.283\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.28\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.28\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.28\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.28\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.28\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.281\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.281\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.281\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.281\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.281\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.28\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.284\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.282\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.282\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.282\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.281\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.281\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.282\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.282\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.281\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.281\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.281\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.281\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.281\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.28\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.281\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.281\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.28\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.28\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.281\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.28\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.28\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.28\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.28\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.282\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.282\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.283\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.281\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.281\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.282\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.282\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.282\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.281\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.282\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.281\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.284\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.282\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.283\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.282\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.282\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.282\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.283\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.282\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.282\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.282\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.281\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.283\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.284\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.282\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.282\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.282\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.281\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.282\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.282\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.281\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.281\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.281\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.28\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.281\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.282\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.282\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.282\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.282\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.282\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.283\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.283\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.283\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.283\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.283\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.283\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.283\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.282\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.283\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.283\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.282\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.282\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.283\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.282\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.283\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.282\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.282\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.284\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.28\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.281\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.281\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.281\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.281\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.281\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.281\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.281\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.281\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.28\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.281\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.283\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.281\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.28\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.281\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.28\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.281\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.28\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.28\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.28\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.28\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.279\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.28\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.281\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.28\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.28\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.281\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.28\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.281\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.281\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.281\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.282\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.28\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.281\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.282\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.281\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.282\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.282\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.282\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.281\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.282\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.281\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.281\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.282\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"3.281\\\",\\\"sts\\\":0},{\\\"val\\\":32,\\\"sts\\\":0},{\\\"val\\\":\\\"25\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"25\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"24\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"24\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"24\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"25\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"25\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"25\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"25\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"25\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"24\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"25\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"24\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"25\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"25\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"25\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"25\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"25\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"24\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"24\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"24\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"24\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"25\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"25\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"25\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"25\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"24\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"24\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"24\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"24\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"25\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"25\\\",\\\"sts\\\":0}],\\\"batteryTempDiffLargeAlarm\\\":0,\\\"batteryUniformityBadAlarm\\\":0,\\\"batteryVoltage\\\":13.88,\\\"batteryVoltageOfbatteryMonitor\\\":606.8,\\\"batterysVoltageHigherAlarm\\\":0,\\\"batterysVoltageLowerAlarm\\\":0,\\\"brakePedalStatus\\\":60.5,\\\"brakingSysFault\\\":0,\\\"canCommunicationFault\\\":0,\\\"canCommunicationFaultOfStorage\\\":0,\\\"chargeStatus\\\":0,\\\"chargeStatusFault\\\":0,\\\"chargingGunConnectFault\\\":0,\\\"corneringLamp\\\":0,\\\"dataId\\\":\\\"JIANGLING-459c958d473747a0be7bdd08b86c23d8-vWAsoEqzSLFqdcNSxf0KzmJlQXvckX70\\\",\\\"dcStatusAlarm\\\":0,\\\"deviceId\\\":\\\"LA9AEPG2XJHLJK506\\\",\\\"deviceType\\\":\\\"TBOX\\\",\\\"dischargeCurrentBiggerAlarm\\\":0,\\\"doorStsFrontLeft\\\":0,\\\"doorStsFrontRight\\\":0,\\\"electricControOverTemp\\\":0,\\\"electricMachineryData\\\":[{\\\"val\\\":\\\"1\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"1\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"4\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"37\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"346\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"-2\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"38\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"600.0\\\",\\\"sts\\\":0},{\\\"val\\\":\\\"0.0\\\",\\\"sts\\\":0}],\\\"electricOverSpeed\\\":0,\\\"electricOverTemp\\\":0,\\\"frontFogLamp\\\":0,\\\"highBeam\\\":0,\\\"inputDcOverCurrent\\\":0,\\\"insulationFault\\\":0,\\\"latitude\\\":22.52712272078104,\\\"longitude\\\":113.95277194674996,\\\"lowBeam\\\":0,\\\"mileage\\\":128.0,\\\"monomerTempHigherAlarm\\\":0,\\\"monomerVoltageHigherAlarm\\\":0,\\\"monomerVoltageLowerAlarm\\\":0,\\\"obcCommunicationFault\\\":0,\\\"otherFaultCount\\\":0,\\\"overTempFault\\\":0,\\\"positionLamp\\\":0,\\\"powerSystemReady\\\":1,\\\"rearFogLamp\\\":0,\\\"runMode\\\":1,\\\"socHigherAlarm\\\":0,\\\"socJumpFault\\\":0,\\\"socLowerAlarm\\\":0,\\\"totalDistance\\\":34820.0,\\\"vehBatSOC\\\":58.0,\\\"vehicleChargeFault\\\":0,\\\"vehicleFault\\\":0,\\\"vehicleHighVoltageInterlockFault\\\":0,\\\"vehicleSpeed\\\":7.88,\\\"vin\\\":\\\"LA9AEPG2XJHLJK506\\\"}]\",\"timestamp\":\"1584600761139\"}";
        JSONObject jsonObject = JSONObject.parseObject(json);
        List<C3ReportCarInfo> conditionList = JSONArray.parseArray(jsonObject.getString("list"), C3ReportCarInfo.class);
        System.out.println(conditionList);
    }


}
