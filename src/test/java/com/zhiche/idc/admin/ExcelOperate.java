package com.zhiche.idc.admin;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import com.alibaba.fastjson.JSONObject;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelOperate {

    public static void main(String[] args) throws Exception {
        File fin = new File("D:\\中联物流\\城配系统\\陶瓷原始单\\明兴.xls");
        List<List<Object>> lists = readExcel(fin);
        System.out.println(JSONObject.toJSONString(lists));
    }


    // 对外提供读取excel文件的接口
    public static List<List<Object>> readExcel(File file) throws IOException {
        String fName = file.getName();
        String extension = fName.lastIndexOf(".") == -1 ? "" : fName
                .substring(fName.lastIndexOf(".") + 1);
        if ("xls".equals(extension) || "XLS".equals(extension)) {// 2003
            System.err.println("读取excel2003文件内容");
            return read2003Excel(file);
        } else if ("xlsx".equals(extension) || "XLSX".equals(extension)) {// 2007
            System.err.println("读取excel2007文件内容");
            return read2007Excel(file);
        } else {
            throw new IOException("不支持的文件类型:" + extension);
        }
    }

    /**
     * 读取2003excel
     *
     * @param file
     * @return
     */
    private static List<List<Object>> read2003Excel(File file)
            throws IOException {
        List<List<Object>> dataList = new ArrayList<List<Object>>();
        HSSFWorkbook wb = new HSSFWorkbook(new FileInputStream(file));
        HSSFSheet sheet = wb.getSheetAt(0);
        HSSFRow row = null;
        HSSFCell cell = null;
        Object val = null;
        DecimalFormat df = new DecimalFormat("0");// 格式化数字
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");// 格式化日期字符串
        for (int i = sheet.getFirstRowNum(); i < sheet
                .getPhysicalNumberOfRows(); i++) {
            row = sheet.getRow(i);
            if (row == null) {
                continue;
            }
            List<Object> objList = new ArrayList<Object>();
            for (int j = row.getFirstCellNum(); j < row.getLastCellNum(); j++) {
                cell = row.getCell(j);
                if (cell == null) {
                    val = null;
                    objList.add(val);
                    continue;
                }
                // System.out.println(cell.getCellType());
                switch (cell.getCellType()) {
                    case STRING:
                        val = cell.getStringCellValue();
                        break;
                    case NUMERIC: {
                        // 判断当前的cell是否为Date
                        if (HSSFDateUtil.isCellDateFormatted(cell)) {
                            val = sdf.format(HSSFDateUtil.getJavaDate(cell
                                    .getNumericCellValue()));
                        }
                        // 如果是纯数字
                        else {
                            // 取得当前Cell的数值
                            val = df.format(cell.getNumericCellValue());
                        }
                        break;
                    }
                    case BOOLEAN:
                        val = cell.getBooleanCellValue();
                        break;
                    case BLANK:
                        val = "";
                        break;
                    default:
                        val = cell.toString();
                        System.out.println(val);
                        break;
                }
                objList.add(val);
            }
            if (row.getLastCellNum() < 87) {
                for (int n = 0; n < 87 - row.getLastCellNum(); n++) {
                    objList.add(null);
                }
            }
            dataList.add(objList);
        }
        return dataList;
    }

    /**
     * 读取excel表头
     *
     * @param file
     * @return
     * @throws IOException
     */
    public static String[] readExcelHead(File file) throws IOException {
        HSSFWorkbook wb = new HSSFWorkbook(new FileInputStream(file));
        HSSFSheet sheet = wb.getSheetAt(0);
        HSSFRow row = null;
        HSSFCell cell = null;
        row = sheet.getRow(0);
        String[] buff = new String[row.getLastCellNum()];
        for (int i = row.getFirstCellNum(); i < row.getLastCellNum(); i++) {
            cell = row.getCell(i);
            buff[i] = cell.getStringCellValue();
        }
        return buff;
    }

    /**
     * 读取2007excel
     *
     * @param file
     * @return
     */

    private static List<List<Object>> read2007Excel(File file)
            throws IOException {
        List<List<Object>> dataList = new ArrayList<List<Object>>();
        XSSFWorkbook xwb = new XSSFWorkbook(new FileInputStream(file));
        XSSFSheet sheet = xwb.getSheetAt(0);
        XSSFRow row = null;
        XSSFCell cell = null;
        Object val = null;
        DecimalFormat df = new DecimalFormat("0");// 格式化数字
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");// 格式化日期字符串
        for (int i = sheet.getFirstRowNum(); i < sheet
                .getPhysicalNumberOfRows(); i++) {
            row = sheet.getRow(i);
            if (row == null) {
                continue;
            }
            List<Object> objList = new ArrayList<Object>();
            for (int j = row.getFirstCellNum(); j < row.getLastCellNum(); j++) {
                cell = row.getCell(j);
                if (cell == null) {
                    val = null;
                    objList.add(val);
                    continue;
                }
                switch (cell.getCellType()) {
                    case STRING:
                        val = cell.getStringCellValue();
                        break;
                    case NUMERIC: {
                        // 判断当前的cell是否为Date
                        if (HSSFDateUtil.isCellDateFormatted(cell)) {
                            val = sdf.format(HSSFDateUtil.getJavaDate(cell
                                    .getNumericCellValue()));
                        }
                        // 如果是纯数字
                        else {
                            // 取得当前Cell的数值
                            val = df.format(cell.getNumericCellValue());
                        }
                        break;
                    }
                    /*
                     * if ("@".equals(cell.getCellStyle().getDataFormatString())) {
                     * val = df.format(cell.getNumericCellValue()); } else if
                     * ("General".equals(cell.getCellStyle().getDataFormatString()))
                     * { val = df.format(cell.getNumericCellValue()); } else { val =
                     * sdf
                     * .format(HSSFDateUtil.getJavaDate(cell.getNumericCellValue()
                     * )); } break;
                     */
                    case BOOLEAN:
                        val = cell.getBooleanCellValue();
                        break;
                    case BLANK:
                        val = "";
                        break;
                    default:
                        val = cell.toString();
                        break;
                }
                objList.add(val);
            }
            if (row.getLastCellNum() < 87) {
                for (int n = 0; n < 87 - row.getLastCellNum(); n++) {
                    objList.add(null);
                }
            }
            dataList.add(objList);
        }
        return dataList;
    }

}
