package com.lisa.auto.enums;

public enum RemoteControlEnum {

    RESULTURL("/basebiz/control/gateway/v3/poll/result", "获取远控结果");


    private final String no;
    private final String explain;

    RemoteControlEnum(String no, String explain) {
        this.no = no;
        this.explain = explain;
    }

    /**
     * Gets no.
     *
     * @return the no
     */
    public String getNo() {
        return no;
    }

    /**
     * Gets explain.
     *
     * @return the explain
     */
    public String getExplain() {
        return explain;
    }

}
