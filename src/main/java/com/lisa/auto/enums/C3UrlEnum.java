package com.lisa.auto.enums;

/**
 * 状态
 * @since 2018-05-30
 * @author lbl
 */
public enum C3UrlEnum {

    DEVICESURL("/ccf/v2/manage/device", "新建设备/取得设备详情/更新设备/删除设备"),
    BINDINGDEVICEURL("/ccf/v2/manage/device/binding", "绑定设备"),
    DEVICETYPESURL("/ccf/v2/manage/device/deviceType", "获取设备类型列表"),
    DEVICESBYSTATUSURL("/ccf/v2/manage/device/listByStatus", "根据绑定状态获取所有设备"),
    DEVICEPAGEURL("/ccf/v2/manage/device/page", "取得设备分页数据"),
    PROTOCOLVERSIONSURL("/basebiz/v2/manage/device/protocol-version", "获取设备协议列表"),
    MAKERSURL("/basebiz/v2/manage/makers/all", "取得制造商列表数据"),
    BRANDSURL("/basebiz/v2/manage/brands/list", "取得品牌列表数据"),
    SERIESURL("/basebiz/v2/manage/series/list", "取得车系列表数据"),
    MODELSURL("/ccf/v2/manage/models/all", "取得所有车型数据"),
    MODELSLISTURL("/basebiz/v2/manage/models/list", "取得车型数据"),
    VEHICLESURL("/ccf/v2/manage/vehicles", "新建车辆,获取车辆详细数据,更新车辆,删除车辆"),
    CARINFOURL("/ccf/v1/manage/report/carinfo", "获取实时车况信息"),
    VEHICLEHISTORYTRAILURL("/data-service/manage/history-service/getVehicleHistoryTrail", "车辆历史轨迹"),
    SERVICEPROVIDERURL("/basebiz/v2/manage/serviceProvider", "服务商接口注册"),
    REPORTURL("/dams/v1/report", "数据上报"),
    CARINFOREPUSHURL("/data-service/manage/car-info-repush", "车况重推"),
    REPORTLOGURL("/data-service/manage/reportLog/page", "取得上报数据分页数据"),
    CONTROLURL("/basebiz/control/gateway/v3/remote/control", "车辆远程控制"),
    RESULTURL("/basebiz/control/gateway/v3/poll/result", "获取远控结果");


    private final String url;
    private final String text;

    C3UrlEnum(String url, String text) {
        this.url = url;
        this.text = text;
    }

    /**
     * Gets code.
     *
     * @return the code
     */
    public String getUrl() {
        return url;
    }

    /**
     * Gets text.
     *
     * @return the text
     */
    public String getText() {
        return text;
    }
}
