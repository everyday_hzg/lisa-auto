//package com.lisa.auto.enums;
//
///**
// * 状态
// * @since 2018-05-30
// * @author lbl
// */
//public enum RiskConfEnum {
//
//    ALARM001,  //	整车故障
//    ALARM002,  //	整车充电故障
//    ALARM003,  //	整车高压互锁故障
//    ALARM004,  //	制动系统故障
//    ALARM005,  //	动力电池过充
//    ALARM006,  //	电池性能
//    ALARM007,  //	动力电池温差过大报警
//    ALARM008,  //	动力电池一致性差报警
//    ALARM009,  //	电池电压-电池监控
//    ALARM010,  //	电池包总电压过高报警
//    ALARM011,  //	电池包总电压过低报警
//    ALARM012,  //	电池箱不匹配故障
//    ALARM013,  //	CAN通讯故障（丢失VCU节点）
//    ALARM014,  //	CAN通讯故障（丢失VCU节点）-可充电储能装置故障
//    ALARM015,  //	充电状态故障报警
//    ALARM016,  //	充电枪连接故障
//    ALARM017,  //	DCDC状态报警
//    ALARM018,  //	放电电流过大报警
//    ALARM019,  //	电机控制器过温
//    ALARM020,  //	电机超速
//    ALARM021,  //	电机过温
//    ALARM022,  //	输入直流过流
//    ALARM023,  //	绝缘故障
//    ALARM024,  //	单体温度过高报警
//    ALARM025,  //	单体电压过高报警
//    ALARM026,  //	单体电压过低报警
//    ALARM027,  //	OBC通讯故障(丢失OBC节点)
//    ALARM028,  //	过温故障
//    ALARM029,  //	SOC过高报警
//    ALARM030,  //	SOC跳变故障
//    ALARM031,  //	SOC过低报警
//
//
//}
