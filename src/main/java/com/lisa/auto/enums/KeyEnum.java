//package com.lisa.auto.enums;
//
///**
// * 状态
// * @since 2018-05-30
// * @author lbl
// */
//public enum KeyEnum {
//
//    APPKEY("JiRChvAFDCEFgjg", "appkey"),
//    SECRETKEY("onxvpJKyRUCgkCk", "secretkey");
//
//
//
//    private final String code;
//    private final String text;
//
//    KeyEnum(String code, String text) {
//        this.code = code;
//        this.text = text;
//    }
//
//    /**
//     * Gets code.
//     *
//     * @return the code
//     */
//    public String getCode() {
//        return code;
//    }
//
//    /**
//     * Gets text.
//     *
//     * @return the text
//     */
//    public String getText() {
//        return text;
//    }
//}
