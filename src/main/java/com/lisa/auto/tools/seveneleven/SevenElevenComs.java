package com.lisa.auto.tools.seveneleven;

import com.google.common.collect.Maps;
import com.lisa.auto.entity.excel.GoodsTmplPo;
import com.lisa.auto.entity.excel.OrderTmplPo;
import com.lisa.auto.util.DateUtil;
import org.apache.commons.lang3.StringUtils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 711 基础数据处理类
 *
 * @author huangzhigang 2019/12/24
 */
public class SevenElevenComs {

    /** 默认值 */
    public static final String ZERO_ = "0";

    public static final String EMPTY = "";

    public static final String EMPTY_ = " ";

    protected static final String SETTLEMENT_TYPE = "默认";

    protected static final String PCL_DESCRIBE = "运单描述";

    /** 发货人 */
    protected static final String CHANG_YUAN = "深圳畅远物流";

    /** 发货地址 */
    protected static final String CHANG_YUAN_ADDR = "广东省 深圳市 龙岗区 宝安粮食交易中心";
    /** 期望送达开始时间 */
    public static String EXT_DELIVERY_START_TIME = " 18:00";
    /** 期望送达结束时间 */
    public static String EXT_DELIVERY_END_TIME = " 20:00";
    /** 取货开始时间 */
    public static String START_PICKING_TIME = " 20:00";
    /** 取货结束时间 */
    public static String END_PICKING_TIME = " 07:00";

    protected static final String EXT_ORDER_NUM_PREFIX = "CY";
    /** 商户编号 */
    protected static final String MERCHANT_CODE = "P20191219165354371799";
    /** 发货地址编号 */
    protected static final String SHIPPING_ADDR_NO = "D201912181216401579263";

    protected static final String ORDER_SHEET_IDX = "笼数";

    protected static final String SHOP_ADDR_SHEET_IDX = "1";

    public static final int ONE_ = 1;

    public static final int TWO_ = 2;

    public static OrderTmplPo getInitData(Long idx) {
        // 外部订单号、运费、保价费、包装费、送货费、上楼费、接货费、其他费用、结算类型、描述、商户、商户编号、发货地址、发货地址编号、发货人、发货人电话
        return new OrderTmplPo(EXT_ORDER_NUM_PREFIX + (System.currentTimeMillis() + (long)(Math.random() * 1000000)), ZERO_, ZERO_, ZERO_, ZERO_, ZERO_,
                ZERO_, ZERO_, SETTLEMENT_TYPE, PCL_DESCRIBE, CHANG_YUAN,
                MERCHANT_CODE, CHANG_YUAN_ADDR, SHIPPING_ADDR_NO, CHANG_YUAN, ZERO_, DateUtil.format(DateUtil.DATE) + START_PICKING_TIME,
                DateUtil.getTomorrowDate() + END_PICKING_TIME, ZERO_, EMPTY);
    }

    protected static String orderDataDb(String orderJson){
        return orderJson.replaceAll("店铺", "shopId").replaceAll("笼车数量", "cageNum").
                replaceAll("笼外胶箱", "glueBoxNum");
    }

    protected static String shipmentDb(String orderJson){
        return orderJson
                .replaceAll("[( )]", "")
                .replaceAll("店铺", "shopId")
                .replaceAll("原箱SP", "originalBoxSP")
                .replaceAll("散货SP", "bulkSP")
                .replaceAll("大点章", "bigChapter")
                .replaceAll("散货胶箱", "bulkBoxNum")
                .replaceAll("笼外胶箱", "glueBoxNum")
                .replaceAll("自动伞262310", "selfOpeningUmbrella")
                .replaceAll("公主伞263299", "princessUmbrella")
                .replaceAll("塑料长柄伞264531", "longUmbrella")
                .replaceAll("格纹伞264557", "plaidUmbrella")
                .replaceAll("素色伞264558", "colouredUmbrella")
                .replaceAll("女士伞264559", "handleUmbrella")
                .replaceAll("铺型", "shopType")
                .replaceAll("原箱笼数", "originalLuggageNum")
                .replaceAll("计费笼数", "priceCageNum")
                .replaceAll("原箱箱数", "originalBoxNum")
                .replaceAll("区域", "region");
    }
    protected static String shopAddrDb(String orderJson){
        return orderJson
                .replaceAll("店铺", "shopId")
                .replaceAll("线", "lineId")
                .replaceAll("站", "stationId")
                .replaceAll("留箱否", "leaveBox")
                .replaceAll("铺型", "shopType")
                .replaceAll("送货时间", "estimatedTime")
                .replaceAll("电话号码", "tel");
    }
    public static String vehicleDb(String orderJson) {
        return orderJson
                .replaceAll("企业", "company")
                .replaceAll("客户/品牌名", "brand")
                .replaceAll("车次编号", "trainNo")
                .replaceAll("车牌号", "plate")
                .replaceAll("司机手机", "driverPhone")
                .replaceAll("剩余体积m³", "residualVolume")
                .replaceAll("剩余重量kg", "residualWeight")
                .replaceAll("取货地址编号", "pickupAddrNo")
                .replaceAll("取货地址", "pickupAddr")
                .replaceAll("车次状态", "status")
                .replaceAll("取货时间段", "pickupDate")
                .replaceAll("外部运单编号", "orderNo")
                .replaceAll("外部店铺编号", "shopId")
                .replaceAll("卸货地址编号", "unloadingAddrNo")
                .replaceAll("卸货联系人电话", "contactPhone")
                .replaceAll("卸货联系人", "contactName")
                .replaceAll("卸货时间段", "unloadingDate")
                .replaceAll("包裹类型", "packageType")
                .replaceAll("包裹数量", "packageNum")
                .replaceAll("包裹容积", "packageVolume")
                .replaceAll("包裹重量", "packageWeight")
                .replaceAll("运费", "shipPrice")
                .replaceAll("备注", "remark")
//                .replaceAll("司机收入￥", "price")
                .replaceAll("司机收入费用", "price")
                .replaceAll("卸货地址", "unloadingAddr")
                .replaceAll("司机", "driverName");

    }
    protected static String shopAddrDataDb(String shopAddrJson){
        return shopAddrJson.replaceAll("店铺", "shopId").replaceAll("送货时间", "estimatedTime").
                replaceAll("电话号码", "tel");
    }

    protected static String szShopDataDb(String szShopJson){
        return szShopJson.replaceAll("店号", "shopId").replaceAll("地址", "shopAddr").replaceAll("店铺简称", "dbbreviation");
    }

    /**
     * 包裹信息处理
     * @param num 数量
     * @param type 类型（1：笼；2：胶箱）
     * @return GoodsTmpl
     */
    public static List<GoodsTmplPo> goodsTmplDb(int num, int type, List<GoodsTmplPo> goodsTmpls){
        if (num == 0){
            return goodsTmpls;
        }
        // 长、宽、高、包裹名称
        String length = "0.6", wide = "0.4", high = "0.3", pclName = "胶箱";
        if (type == 1){
            length = "0.82";
            wide = "0.7";
            high = "1.72";
            pclName = "笼";
        }
        double volume = Double.parseDouble(length) * Double.parseDouble(wide) * Double.parseDouble(high);
        for (int i = 0; i < num; i++) {
            // 包裹名称、包裹类型(容器)、包裹编号、包裹长、包裹宽、包裹高、容积、重量、价格、包裹描述
            GoodsTmplPo goodsTmpl = new GoodsTmplPo(pclName+i, "容器", String.valueOf(System.currentTimeMillis()),
                    length, wide, high,  String.format("%.3f", volume), SevenElevenComs.ZERO_, SevenElevenComs.ZERO_, SevenElevenComs.ZERO_);
            goodsTmpls.add(goodsTmpl);
        }
        return goodsTmpls;
    }

    public static Map<String, Object> getExport() {
        Map<String, Object> maps = Maps.newLinkedHashMap();

        maps.put("lineId", "线路");
        maps.put("stationId", "站点");
        maps.put("shopId", "店铺");
        maps.put("originalBoxSP", "原箱SP");
        maps.put("bulkSP", "散货SP");
        maps.put("shopType", "送货周期");
        maps.put("bigChapter", "大点章");
        maps.put("bulkBoxNum", "散货胶箱");
        maps.put("glueBoxNum", "笼外胶箱");
        maps.put("selfOpeningUmbrella", "自动伞");
        maps.put("princessUmbrella", "公主伞");
        maps.put("longUmbrella", "长柄伞");
        maps.put("plaidUmbrella", "格纹伞");
        maps.put("colouredUmbrella", "素色伞");
        maps.put("handleUmbrella", "手柄伞");
        maps.put("CO2", "CO2");
        maps.put("shopType1", "铺型");
        maps.put("leaveBox", "是否留箱");
        maps.put("originalLuggageNum", "原箱笼数");
        maps.put("priceCageNum", "计费笼数");
        maps.put("originalBoxNum", "原箱箱数");
        maps.put("estimatedTime", "送货时间");
        maps.put("tel", "店铺电话");
        maps.put("remark", "备注");
        return maps;
    }

	/**
     * 期望送达截止时间处理
     * @param time 时间
     * @return String
     */
    public static String deliveryTime(String time){
        String nowHour = DateUtil.format("HH");
        LocalDateTime nowEndTime = LocalDate.now().atTime(0, 0);
        int endHour = nowEndTime.getHour();
        if (StringUtils.isNotBlank(time) && time.contains(":")){
            String[] timeArray = time.split(":");
            LocalDateTime dateTime = LocalDate.now().atTime(Integer.parseInt(timeArray[0]), Integer.parseInt(timeArray[1]));
            if (dateTime.getHour() < Integer.parseInt(nowHour) && dateTime.getHour() >= endHour){
                return DateUtil.getTomorrowDate() + SevenElevenComs.EMPTY_ + time + ":00";
            }else {
                return DateUtil.format(DateUtil.DATE) + SevenElevenComs.EMPTY_ + time + ":00";
            }
        }
        return DateUtil.getTomorrowDate() + END_PICKING_TIME;
    }}
