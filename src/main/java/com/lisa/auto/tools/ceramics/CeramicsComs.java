package com.lisa.auto.tools.ceramics;

import com.lisa.auto.entity.ComdPo;
import com.lisa.auto.entity.ceramics.bo.CeramicsGoodsBo;
import com.lisa.auto.entity.ceramics.po.CeramicsGoodsPo;
import com.lisa.auto.entity.excel.GoodsTmplPo;
import com.lisa.auto.entity.excel.OrderTmplPo;
import com.lisa.auto.util.DateUtil;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 陶瓷运单处理公共类
 *
 * @author huangzhigang 2020/1/14
 */
public class CeramicsComs {

    static final Logger logger = LoggerFactory.getLogger(CeramicsComs.class);

    /**
     * 包裹类型
     */
    static final String PCL_TYPE = "普通";
    /**
     * 深斯陶瓷机构前缀
     */
    static final String COMP_PREFIX = "SSTC";
    /**
     * 默认值[0]
     */
    public static final String ZERO_ = "0";
    /**
     * 结算类型
     */
    static final String SETTLEMENT_TYPE = "默认";
    /**
     * 运单描述
     */
    static final String DESCRIBE_ = "无";

    /**
     * 期望送达时间起
     */
    static final String EXT_DELIVERY_START_TIME = DateUtil.getTomorrowDate(1) + " 00:00:00";

    /**
     * 期望送达时间止
     */
    static final String EXT_DELIVERY_END_TIME = DateUtil.getTomorrowDate(2) + " 00:00:00";

    /**
     * 取货时间起
     */
    static final String START_PICK_UP = DateUtil.format(DateUtil.DATE) + " 20:00:00";

    /**
     * 取货时间止
     */
    static final String END_PICK_UP = DateUtil.getTomorrowDate(1) + " 00:00:00";

    /**
     * 商户
     */
    static final String MERCHANT_ = "深斯陶瓷";

    /**
     * 商户编号
     */
    public static final String MERCHANT_NUM = "P20200107151755985622";

    /**
     * 发货人
     */
    static final String SHIPPER_ = "深斯陶瓷";

    /**
     * 发货人的电话号码
     */
    static final String SHIPPER_TEL_NUM_ = "0";

    /**
     * 发货人的电话号码
     */
    static final String SHIPPING_ADDR_NO = "D202001071521423568107";

    /**
     * 发货人的电话号码
     */
    static final String SHIPPING_ADDR = "广东省 深圳市 龙岗区 横岗228工业区18栋2楼深斯陶瓷";

    public static final String[] HEADER_TITLE = {"单位编号", "客户地址"};

    /**
     * 初始化订单基本数据
     *
     * @return OrderTmplPo
     */
    public static OrderTmplPo initData() {
        String extOrderNum = COMP_PREFIX + System.currentTimeMillis() + (int)(Math.random()*10000+1);
        /** 外部订单号、运费、保价费、包装费、送货费、上楼费、接货费、其他费用、结算类型、描述、期望送达时间起、期望送达时间止 */
        return new OrderTmplPo(extOrderNum, ZERO_, ZERO_, ZERO_, ZERO_, ZERO_, ZERO_, ZERO_, SETTLEMENT_TYPE, DESCRIBE_, MERCHANT_,
                EXT_DELIVERY_START_TIME, EXT_DELIVERY_END_TIME, START_PICK_UP, END_PICK_UP, SHIPPER_, SHIPPER_TEL_NUM_, SHIPPING_ADDR, SHIPPING_ADDR_NO);
    }

    /**
     * 运单包裹信息处理
     *
     * @param ceramicsGoodsPo 包裹信息实体类
     * @param goodQuantity    数量
     * @return List<GoodsTmplPo>
     */
    public static List<GoodsTmplPo> goodsDB(List<ComdPo> comdList, CeramicsGoodsPo ceramicsGoodsPo, Integer goodQuantity, List<GoodsTmplPo> goodsTmplList) {
        try {
            String goodsName = ceramicsGoodsPo.getGoodName();
            String company = ceramicsGoodsPo.getCompany();
            if (StringUtils.isNotBlank(goodsName) && StringUtils.isNotBlank(company) && CollectionUtils.isNotEmpty(comdList)){
                List<ComdPo> comdListGroup = comdList.stream().filter(comd->StringUtils.isNotBlank(comd.getComdName()) && goodsName.equals(comd.getComdName()) &&
                    StringUtils.isNotBlank(comd.getUnit()) && company.equals(comd.getUnit())).collect(Collectors.toList());
                if (CollectionUtils.isEmpty(comdListGroup)){
                    GoodsTmplPo goodsTmpl = new GoodsTmplPo(ceramicsGoodsPo.getGoodName(), PCL_TYPE, DateUtil.format(DateUtil.DATE_TIME_1), ZERO_, ZERO_,
                            ZERO_, ZERO_, ZERO_, ZERO_, ZERO_);
                    goodsTmplList.add(goodsTmpl);
                    return goodsTmplList;
                }
                double length = Double.parseDouble(comdListGroup.get(0).getLength())/100;
                double width = Double.parseDouble(comdListGroup.get(0).getWidth())/100;
                double height = Double.parseDouble(comdListGroup.get(0).getHeight())/100;
                double volumeSum = length * width * height;
                String volume = String.format("%.3f", volumeSum);
                GoodsTmplPo goodsTmpl = new GoodsTmplPo(ceramicsGoodsPo.getGoodName(), PCL_TYPE, DateUtil.format(DateUtil.DATE_TIME_1), String.valueOf(length), String.valueOf(width),
                        String.valueOf(height), volume, ZERO_, ZERO_, ZERO_);
                goodsTmplList.add(goodsTmpl);
                return goodsTmplList;
            }
            GoodsTmplPo goodsTmpl = new GoodsTmplPo(ceramicsGoodsPo.getGoodName(), PCL_TYPE, DateUtil.format(DateUtil.DATE_TIME_1), ZERO_, ZERO_,
                    ZERO_, ZERO_, ZERO_, ZERO_, ZERO_);
            goodsTmplList.add(goodsTmpl);
            return goodsTmplList;
        } catch (Exception e) {
            logger.info(e.getMessage(), e);
        }
        return goodsTmplList;
    }


}
