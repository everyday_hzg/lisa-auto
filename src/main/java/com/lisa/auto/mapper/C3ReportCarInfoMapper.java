package com.lisa.auto.mapper;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.lisa.auto.entity.*;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.lisa.auto.entity.statistics.VehiclesStatisticsPo;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 上报车况信息 Mapper 接口
 * </p>
 *
 * @author suohao
 * @since 2019-12-25
 */
public interface C3ReportCarInfoMapper extends BaseMapper<C3ReportCarInfo> {

    List<C3ReportCarInfoVo> getCarCurrentInfoList(@Param("ew") EntityWrapper<C3ReportCarInfo> ew, @Param("condition") Map<String, Object> condition);

    List<C3ReportCarInfoDto> getCityCarInfoList(@Param("ew") EntityWrapper<C3ReportCarInfo> ew, @Param("condition") Map<String, Object> condition);

    List<Map<String, Object>> getBatList(@Param("ew") EntityWrapper<C3ReportCarInfo> ew);

    Map<String, Object> getCarInfoTotal(@Param("ew") EntityWrapper<C3ReportCarInfo> ew);

    List<AreaCountDto> getAreaVehicleCount(@Param("ew") EntityWrapper<C3ReportCarInfo> ew, @Param("areaCountDto") AreaCountDto areaCountDto);

    List<C3ReportCarInfoDto> getCarInfoRoute(@Param("ew") EntityWrapper<C3ReportCarInfo> ew);

    double getTotalMiles(@Param("ew") EntityWrapper<C3ReportCarInfo> ew);

    List<String> selectStatusList(@Param("ew") EntityWrapper<C3ReportCarInfo> entityWrapper);

    List<Map<String, Object>> getRiskFaultRate(@Param("ew")EntityWrapper<Vehicle> ew,@Param("sort")String sort,@Param("time")long time);

    List<Map<String,Object>> getRiskRateForVin(@Param("ew") EntityWrapper<Vehicle> ew,@Param("minutes")long minutes,@Param("startDate")String startDate,@Param("endDate")String endDate);

    /**
     * 查询所有车队公司
     * @param condition 条件集
     * @return List<VehiclesStatisticsPo>
     */
    List<VehiclesStatisticsPo> selFleets(Map<String, Object> condition);

    /**
     * 查询车队公司行为次数统计
     * @param condition 条件集
     * @return List<VehiclesStatisticsPo>
     */
    List<VehiclesStatisticsPo> selFleetActionStat(Map<String, Object> condition);

    /**
     * 查询车队公司异常新闻给车辆数
     * @param condition 条件集
     * @return List<VehiclesStatisticsPo>
     */
    List<VehiclesStatisticsPo> selFleetUnusualCnt(Map<String, Object> condition);

    /**
     * 车队公司及车辆汇总数据
     * @param condition 车架号
     * @return List<VehiclesStatisticsPo>
     */
    VehiclesStatisticsPo selVehiclesStatisticsList(Map<String, String> condition);


    List<VehiclesStatisticsPo> selCarCondStatisticsData(Map<String, Object> condition);

    List<VehiclesStatisticsPo> selCarCondStatisticsData1(Map<String, Object> condition);

    List<C3ReportCarInfo> selCarCond(Map<String, String> condition);


}
