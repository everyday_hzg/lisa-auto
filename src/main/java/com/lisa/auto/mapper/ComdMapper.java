package com.lisa.auto.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.lisa.auto.entity.ComdPo;

/**
 * @description:
 * @author: huangzhigang
 * @date: 2020-04-01 17:23
 **/
public interface ComdMapper extends BaseMapper<ComdPo> {
}
