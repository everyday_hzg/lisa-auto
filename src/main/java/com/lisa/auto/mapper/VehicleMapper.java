package com.lisa.auto.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.lisa.auto.entity.Vehicle;
import com.lisa.auto.entity.VehicleModelConfigureDto;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 车辆信息 Mapper 接口
 * </p>
 *
 * @author huangzhigang
 * @since 2019-12-02
 */
public interface VehicleMapper extends BaseMapper<Vehicle> {

    List<Vehicle> getVehiclesList(@Param("ew") EntityWrapper<Vehicle> ew, @Param("page")Page<Vehicle> page);

    void updateStatusVehicles(@Param("vin")String vin,@Param("status") String status);

    List<VehicleModelConfigureDto> getVehiclesListNew(@Param("ew") EntityWrapper<VehicleModelConfigureDto> ew, @Param("page") Page<VehicleModelConfigureDto> page);

    List<String> getVehiclesFleet(@Param("ew") EntityWrapper<Vehicle> ew);

    List<String> getPlateList(@Param("ew")EntityWrapper<Vehicle> ew);
}
