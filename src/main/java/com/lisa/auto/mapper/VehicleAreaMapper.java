package com.lisa.auto.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.lisa.auto.entity.VehicleArea;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 车辆信息 Mapper 接口
 * </p>
 *
 * @author huangzhigang
 * @since 2019-12-02
 */
public interface VehicleAreaMapper extends BaseMapper<VehicleArea> {


    List<VehicleArea> queryVehicleArea(@Param("vehicleArea") VehicleArea vehicleArea);

    List<VehicleArea> getVehicleAreaList(@Param("ew") EntityWrapper<VehicleArea> ew);

}
