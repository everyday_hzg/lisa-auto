package com.lisa.auto.mapper;

import com.lisa.auto.entity.C3ServiceProvider;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 服务商接口 Mapper 接口
 * </p>
 *
 * @author suohao
 * @since 2019-12-23
 */
public interface C3ServiceProviderMapper extends BaseMapper<C3ServiceProvider> {

}
