package com.lisa.auto.mapper;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.lisa.auto.entity.C3RiskFault;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 风险故障记录表 Mapper 接口
 * </p>
 *
 * @author suohao
 * @since 2020-02-21
 */
public interface C3RiskFaultMapper extends BaseMapper<C3RiskFault> {

    C3RiskFault selectRiskFault(@Param("ew") EntityWrapper<C3RiskFault> riskFaultEntityWrapper);

    String  selectTypeByVin(@Param("vin") String vin);
}
