package com.lisa.auto.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.lisa.auto.entity.OrderInfo;

/**
 * <p>
 * 订单信息 Mapper 接口
 * </p>
 *
 * @author huangzhigang
 * @since 2019-12-02
 */
public interface OrderInfoMapper extends BaseMapper<OrderInfo> {

}
