package com.lisa.auto.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.lisa.auto.entity.C3CarCondStatistics;
import com.lisa.auto.entity.C3ReportCarInfo;
import org.apache.ibatis.annotations.Param;

/**
 * @description:
 * @author: huangzhigang
 * @date: 2020-03-30 14:42
 **/
public interface C3CarCondStatisticsMapper extends BaseMapper<C3CarCondStatistics> {
    double getTotalMiles(@Param("ew") EntityWrapper<C3ReportCarInfo> entityWrapper);
}
