package com.lisa.auto.mapper;

import com.lisa.auto.entity.C3FaultConfiguration;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 车辆故障配置 Mapper 接口
 * </p>
 *
 * @author suohao
 * @since 2020-02-21
 */
public interface C3FaultConfigurationMapper extends BaseMapper<C3FaultConfiguration> {

}
