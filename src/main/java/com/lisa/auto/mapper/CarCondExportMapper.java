package com.lisa.auto.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.lisa.auto.entity.excel.CarCondPo;

/**
 * @description:
 * @author: huangzhigang
 * @date: 2020-05-07 14:32
 **/
public interface CarCondExportMapper extends BaseMapper<CarCondPo> {
}
