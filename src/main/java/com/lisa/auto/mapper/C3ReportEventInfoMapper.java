package com.lisa.auto.mapper;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.lisa.auto.entity.C3ReportEventInfo;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 上报事件信息 Mapper 接口
 * </p>
 *
 * @author suohao
 * @since 2019-12-25
 */
public interface C3ReportEventInfoMapper extends BaseMapper<C3ReportEventInfo> {

    List<C3ReportEventInfo> getEvent(@Param("ew") EntityWrapper<C3ReportEventInfo> eventInfoEntityWrapper);

    int getEventCount(@Param("event")String event, @Param("vin")String vin);
}
