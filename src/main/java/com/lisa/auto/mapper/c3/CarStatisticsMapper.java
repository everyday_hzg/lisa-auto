package com.lisa.auto.mapper.c3;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.lisa.auto.entity.VehiclePo;
import com.lisa.auto.entity.statistics.VehiclesPo;

import java.util.List;
import java.util.Map;

/**
 * @author huangzhigang 2020/2/26
 */
public interface CarStatisticsMapper extends BaseMapper<VehiclesPo> {

    /**
     * 检索查询车辆信息
     * @param condition 检索条件
     * @return List<VehiclesPo>
     */
    List<VehiclesPo> searchVehiclesInfo(Map<String, String> condition);

    /**
     * 查询异常状态车辆信息
     * @param condition 检索条件
     * @return List<VehiclesPo>
     */
    List<VehiclesPo> selVehiclesUnusuals(Map<String, String> condition);

    /**
     * 查询运行/正常状态车辆信息
     * @param condition 检索条件
     * @return List<VehiclesPo>
     */
    List<VehiclesPo> selNormalVehiclesInfo(Map<String, String> condition);

    Map<String, Object> getNomalTabDetails(Map<String, String> condition);

    Map<String, Object> getTabDetails(Map<String, String> condition);
}
