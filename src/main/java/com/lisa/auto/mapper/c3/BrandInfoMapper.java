package com.lisa.auto.mapper.c3;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.lisa.auto.entity.c3.model.BrandInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 品牌管理信息 Mapper 接口
 * </p>
 *
 * @author hxh
 * @since 2019-12-20
 */
public interface BrandInfoMapper extends BaseMapper<BrandInfo> {

    List<BrandInfo> getBrandsListPage(@Param("ew") EntityWrapper<BrandInfo> ew, @Param("page") Page<BrandInfo> page);

}
