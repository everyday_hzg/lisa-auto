package com.lisa.auto.mapper.c3;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.lisa.auto.entity.c3.model.CarSeries;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 车系管理 Mapper 接口
 * </p>
 *
 * @author hxh
 * @since 2019-12-20
 */
public interface CarSeriesMapper extends BaseMapper<CarSeries> {

    List<CarSeries> getCarSeriesListPage(@Param("ew") EntityWrapper<CarSeries> ew, @Param("page")Page<CarSeries> page);

}
