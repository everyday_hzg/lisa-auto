package com.lisa.auto.mapper.c3;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.lisa.auto.entity.c3.model.Maker;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 制造商信息 Mapper 接口
 * </p>
 *
 * @author hxh
 * @since 2019-12-20
 */
public interface MakerMapper extends BaseMapper<Maker> {

    List<Maker> getMakersListPage(@Param("ew") EntityWrapper<Maker> ew, @Param("page")Page<Maker> page);

}
