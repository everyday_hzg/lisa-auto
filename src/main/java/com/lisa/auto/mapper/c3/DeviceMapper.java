package com.lisa.auto.mapper.c3;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.lisa.auto.entity.c3.model.Device;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 设备信息表 Mapper 接口
 * </p>
 *
 * @author hxh
 * @since 2019-12-24
 */
public interface DeviceMapper extends BaseMapper<Device> {

    List<Device> getDevicesList(@Param("ew")EntityWrapper<Device> ew, @Param("page")Page<Device> page);
}
