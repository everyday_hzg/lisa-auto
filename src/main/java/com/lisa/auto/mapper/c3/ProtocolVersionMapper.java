package com.lisa.auto.mapper.c3;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.lisa.auto.entity.c3.model.ProtocolVersion;

/**
 * <p>
 * 协议版本信息 Mapper 接口
 * </p>
 *
 * @author hxh
 * @since 2019-12-24
 */
public interface ProtocolVersionMapper extends BaseMapper<ProtocolVersion> {

}
