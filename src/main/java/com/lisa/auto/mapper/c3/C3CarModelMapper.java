package com.lisa.auto.mapper.c3;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.lisa.auto.entity.c3.model.CarModel;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 车型管理 Mapper 接口
 * </p>
 *
 * @author suohao
 * @since 2019-12-23
 */
public interface C3CarModelMapper extends BaseMapper<CarModel> {

    List<CarModel> getModelsList(@Param("ew") EntityWrapper<CarModel> ew, @Param("page") Page<CarModel> page);

    List<CarModel> getAllCarModel();
    List<CarModel> getModelsListPage(@Param("ew") EntityWrapper<CarModel> ew, @Param("page") Page<CarModel> page);
}
