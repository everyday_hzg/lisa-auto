package com.lisa.auto.mapper.c3;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.lisa.auto.entity.c3.model.CarModelConfigure;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 车型管理 Mapper 接口
 * </p>
 *
 * @author hxh
 * @since 2020-01-07
 */
public interface C3CarModelConfigureMapper extends BaseMapper<CarModelConfigure> {

    List<CarModelConfigure> getConfiguresListPage(@Param("ew") EntityWrapper<CarModelConfigure> ew, @Param("page") Page<CarModelConfigure> page);

    List<CarModelConfigure> getAllCarModel();
}
