package com.lisa.auto.mapper;

import com.lisa.auto.entity.C3RiskConfiguration;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 车辆风险配置 Mapper 接口
 * </p>
 *
 * @author suohao
 * @since 2020-02-21
 */
public interface C3RiskConfigurationMapper extends BaseMapper<C3RiskConfiguration> {

}
