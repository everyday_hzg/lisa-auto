package com.lisa.auto.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.lisa.auto.entity.C3CarInfo;
import com.lisa.auto.entity.Vehicle;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 车况信息 Mapper 接口
 * </p>
 *
 * @author suohao
 * @since 2019-12-20
 */
public interface C3CarInfoMapper extends BaseMapper<C3CarInfo> {

    List<Map<String, Object>> getVehicleList(@Param("ew") EntityWrapper<Vehicle> ew);

    List<Map<String, Object>> getTotalDistance(@Param("vin")String vin, @Param("startTime")String startTime, @Param("endTime")String endTime);

    List<Map<String, Object>> getVehicleSpeed(@Param("vin")String vin, @Param("startTime")String startTime, @Param("endTime")String endTime);
}
