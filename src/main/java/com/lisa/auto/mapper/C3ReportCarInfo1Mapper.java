package com.lisa.auto.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.lisa.auto.entity.*;
import com.lisa.auto.entity.statistics.VehiclesStatisticsPo;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 上报车况信息 Mapper 接口
 * </p>
 *
 * @author suohao
 * @since 2019-12-25
 */
public interface C3ReportCarInfo1Mapper extends BaseMapper<C3ReportCar1Info> {

    /**
     * 删除历史数据
     */
    void delHistoryData();

}
