package com.lisa.auto.mapper;

import com.lisa.auto.entity.C3ExportLog;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 接口日志 Mapper 接口
 * </p>
 *
 * @author suohao
 * @since 2019-12-23
 */
public interface C3ExportLogMapper extends BaseMapper<C3ExportLog> {

}
