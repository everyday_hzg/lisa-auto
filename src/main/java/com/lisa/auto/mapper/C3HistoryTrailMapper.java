package com.lisa.auto.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.lisa.auto.entity.C3HistoryTrail;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 车辆历史轨迹信息 Mapper 接口
 * </p>
 *
 * @author suohao
 * @since 2019-12-20
 */
public interface C3HistoryTrailMapper extends BaseMapper<C3HistoryTrail> {

    List<C3HistoryTrail> getHistoryTrailList(@Param("ew") EntityWrapper<C3HistoryTrail> ew);
}
