package com.lisa.auto.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;

import java.util.Date;

import java.io.Serializable;

/**
 * <p>
 * 商品信息
 * </p>
 *
 * @author huangzhigang
 * @since 2019-12-02
 */
public class GoodsInfo implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * 数据id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 商品信息
     */
    @TableField("goods_name")
    private String goodsName;

    /**
     * 长度（单位：mm）
     */
    @TableField("lengths")
    private Integer lengths;

    /**
     * 宽度（单位：mm）
     */
    @TableField("widths")
    private Integer widths;

    /**
     * 高度（单位：mm）
     */
    @TableField("heights")
    private Integer heights;

    /**
     * 重量（单位：mm）
     */
    @TableField("weights")
    private Integer weights;

    /**
     * 胶箱重量（单位：mm）
     */
    @TableField("box_weight")
    private Integer boxWeight;

    /**
     * 备注
     */
    @TableField("remark")
    private String remark;

    /**
     * 创建人
     */
    @TableField("create_user")
    private String createUser;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;

    /**
     * 更新人
     */
    @TableField("update_user")
    private String updateUser;

    /**
     * 更新时间
     */
    @TableField("update_time")
    private Date updateTime;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public Integer getLengths() {
        return lengths;
    }

    public void setLengths(Integer lengths) {
        this.lengths = lengths;
    }

    public Integer getWidths() {
        return widths;
    }

    public void setWidths(Integer widths) {
        this.widths = widths;
    }

    public Integer getHeights() {
        return heights;
    }

    public void setHeights(Integer heights) {
        this.heights = heights;
    }

    public Integer getWeights() {
        return weights;
    }

    public void setWeights(Integer weights) {
        this.weights = weights;
    }

    public Integer getBoxWeight() {
        return boxWeight;
    }

    public void setBoxWeight(Integer boxWeight) {
        this.boxWeight = boxWeight;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}
