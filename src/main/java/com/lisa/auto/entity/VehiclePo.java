package com.lisa.auto.entity;

import cn.afterturn.easypoi.excel.annotation.Excel;

import java.io.Serializable;

public class VehiclePo  implements Serializable {

    @Excel(name = "制造商")
    private String makerName;

    @Excel(name = "品牌")
    private String brandName;

    @Excel(name = "车系")
    private String seriesName;

    @Excel(name = "车型")
    private String modelName;

    @Excel(name = "车牌号")
    private String plateLicenseNo;

    @Excel(name = "车架号")
    private String vin;

    @Excel(name = "省")
    private String province;

    @Excel(name = "市")
    private String city;

    @Excel(name = "区/县")
    private String county;

    @Excel(name = "备注")
    private String mark;

    public VehiclePo(String makerName, String brandName, String plateLicenseNo, String seriesName, String modelName, String vin, String province, String city, String county, String mark) {
        this.makerName = makerName;
        this.brandName = brandName;
        this.seriesName = seriesName;
        this.modelName = modelName;
        this.vin = vin;
        this.province = province;
        this.city = city;
        this.county = county;
        this.mark = mark;
        this.plateLicenseNo = plateLicenseNo;
    }

    public String getPlateLicenseNo() {
        return plateLicenseNo;
    }

    public void setPlateLicenseNo(String plateLicenseNo) {
        this.plateLicenseNo = plateLicenseNo;
    }

    public String getMakerName() {
        return makerName;
    }

    public void setMakerName(String makerName) {
        this.makerName = makerName;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getSeriesName() {
        return seriesName;
    }

    public void setSeriesName(String seriesName) {
        this.seriesName = seriesName;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }
}
