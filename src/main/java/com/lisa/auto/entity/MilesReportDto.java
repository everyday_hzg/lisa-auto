package com.lisa.auto.entity;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author : suohao
 * Date: 2020/2/25
 */
public class MilesReportDto implements Serializable {

    private String fleetList;
    private String plateList;

    private BigDecimal totalMiles;

    private int vehiclesSum;

    private BigDecimal averageMiles;

    public String getFleetList() {
        return fleetList;
    }

    public void setFleetList(String fleetList) {
        this.fleetList = fleetList;
    }

    public String getPlateList() {
        return plateList;
    }

    public void setPlateList(String plateList) {
        this.plateList = plateList;
    }

    public BigDecimal getTotalMiles() {
        return totalMiles;
    }

    public void setTotalMiles(BigDecimal totalMiles) {
        this.totalMiles = totalMiles;
    }

    public int getVehiclesSum() {
        return vehiclesSum;
    }

    public void setVehiclesSum(int vehiclesSum) {
        this.vehiclesSum = vehiclesSum;
    }

    public BigDecimal getAverageMiles() {
        return averageMiles;
    }

    public void setAverageMiles(BigDecimal averageMiles) {
        this.averageMiles = averageMiles;
    }
}
