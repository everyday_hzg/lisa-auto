package com.lisa.auto.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 上报车况信息
 * </p>
 *
 * @author suohao
 * @since 2019-12-25
 */
public class C3ReportCarInfoVo implements Serializable {


    /**
     * id
     */
	private Long id;
    /**
     * 车辆VIN码
     */
	private String vin;
    /**
     * 设备Id
     */
	private String deviceId;
    /**
     * 设备类型
     */
	private String deviceType;
	/**
     * 省
     */
	private String province;
	/**
     * 市
     */
	private String city;
	/**
     * 区
     */
	private String county;
	/**
     * 区
     */
	private String type;
    /**
     * 数据采集时间ms
     */
	private Date acquisitionTime;
    /**
     * ACC状态,0：关闭，1：打开
     */
	private Long accStatus;
    /**
     * 制动踏板状态,1：使用中，0：未使用
     */
	private Long brkPedalStatus;
    /**
     * 电瓶电压v
     */
	private BigDecimal batteryVoltage;
    /**
     * 发动机转速r/min
     */
	private BigDecimal engineSpeed;
    /**
     * 剩余油量L
     */
	private BigDecimal fuel;
    /**
     * 纬度
     */
	private BigDecimal latitude;
    /**
     * 经度
     */
	private BigDecimal longitude;
    /**
     * 总里程km
     */
	private BigDecimal totalDistance;
    /**
     * 电池电量百分比%
     */
	private BigDecimal vehBatSoc;
    /**
     * 速度km/h
     */
	private BigDecimal vehicleSpeed;
    /**
     * 创建时间
     */
	private Date gmtCreate;
    /**
     * 更新时间
     */
	private Date gmtModified;

	private String plateLicenseNo;

	private String tboxNo;

	private String areaType;
	private String fleet;
	private String equipmentFaultItem;
	private String vehicleEquipment;
	private String faultType;

	public String getFleet() {
		return fleet;
	}

	public void setFleet(String fleet) {
		this.fleet = fleet;
	}

	public String getEquipmentFaultItem() {
		return equipmentFaultItem;
	}

	public void setEquipmentFaultItem(String equipmentFaultItem) {
		this.equipmentFaultItem = equipmentFaultItem;
	}

	public String getVehicleEquipment() {
		return vehicleEquipment;
	}

	public void setVehicleEquipment(String vehicleEquipment) {
		this.vehicleEquipment = vehicleEquipment;
	}

	public String getFaultType() {
		return faultType;
	}

	public void setFaultType(String faultType) {
		this.faultType = faultType;
	}

	public String getAreaType() {
		return areaType;
	}

	public void setAreaType(String areaType) {
		this.areaType = areaType;
	}

	public String getPlateLicenseNo() {
		return plateLicenseNo;
	}

	public void setPlateLicenseNo(String plateLicenseNo) {
		this.plateLicenseNo = plateLicenseNo;
	}

	public String getTboxNo() {
		return tboxNo;
	}

	public void setTboxNo(String tboxNo) {
		this.tboxNo = tboxNo;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getVin() {
		return vin;
	}

	public void setVin(String vin) {
		this.vin = vin;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	public Date getAcquisitionTime() {
		return acquisitionTime;
	}

	public void setAcquisitionTime(Date acquisitionTime) {
		this.acquisitionTime = acquisitionTime;
	}

	public Long getAccStatus() {
		return accStatus;
	}

	public void setAccStatus(Long accStatus) {
		this.accStatus = accStatus;
	}

	public Long getBrkPedalStatus() {
		return brkPedalStatus;
	}

	public void setBrkPedalStatus(Long brkPedalStatus) {
		this.brkPedalStatus = brkPedalStatus;
	}

	public BigDecimal getBatteryVoltage() {
		return batteryVoltage;
	}

	public void setBatteryVoltage(BigDecimal batteryVoltage) {
		this.batteryVoltage = batteryVoltage;
	}

	public BigDecimal getEngineSpeed() {
		return engineSpeed;
	}

	public void setEngineSpeed(BigDecimal engineSpeed) {
		this.engineSpeed = engineSpeed;
	}

	public BigDecimal getFuel() {
		return fuel;
	}

	public void setFuel(BigDecimal fuel) {
		this.fuel = fuel;
	}

	public BigDecimal getLatitude() {
		return latitude;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setLatitude(BigDecimal latitude) {
		this.latitude = latitude;
	}

	public BigDecimal getLongitude() {
		return longitude;
	}

	public void setLongitude(BigDecimal longitude) {
		this.longitude = longitude;
	}

	public BigDecimal getTotalDistance() {
		return totalDistance;
	}

	public void setTotalDistance(BigDecimal totalDistance) {
		this.totalDistance = totalDistance;
	}

	public BigDecimal getVehBatSoc() {
		return vehBatSoc;
	}

	public void setVehBatSoc(BigDecimal vehBatSoc) {
		this.vehBatSoc = vehBatSoc;
	}

	public BigDecimal getVehicleSpeed() {
		return vehicleSpeed;
	}

	public void setVehicleSpeed(BigDecimal vehicleSpeed) {
		this.vehicleSpeed = vehicleSpeed;
	}

	public Date getGmtCreate() {
		return gmtCreate;
	}

	public void setGmtCreate(Date gmtCreate) {
		this.gmtCreate = gmtCreate;
	}

	public Date getGmtModified() {
		return gmtModified;
	}

	public void setGmtModified(Date gmtModified) {
		this.gmtModified = gmtModified;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}


	@Override
	public String toString() {
		return "C3ReportCarInfo{" +
			", id=" + id +
			", vin=" + vin +
			", deviceId=" + deviceId +
			", deviceType=" + deviceType +
			", acquisitionTime=" + acquisitionTime +
			", accStatus=" + accStatus +
			", brkPedalStatus=" + brkPedalStatus +
			", batteryVoltage=" + batteryVoltage +
			", engineSpeed=" + engineSpeed +
			", fuel=" + fuel +
			", latitude=" + latitude +
			", longitude=" + longitude +
			", totalDistance=" + totalDistance +
			", vehBatSoc=" + vehBatSoc +
			", vehicleSpeed=" + vehicleSpeed +
			", gmtCreate=" + gmtCreate +
			", gmtModified=" + gmtModified +
			"}";
	}
}
