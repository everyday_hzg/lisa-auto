package com.lisa.auto.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 上报事件信息
 * </p>
 *
 * @author suohao
 * @since 2019-12-25
 */
@TableName("c3_report_event_info")
public class C3ReportEventInfo extends Model<C3ReportEventInfo> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
	@TableId(value="id", type= IdType.AUTO)
	private Long id;
    /**
     * 车辆VIN码
     */
	private String vin;
    /**
     * 偏离类型,1：左侧偏离 , 2：右侧偏离 , 仅报警类型为车道偏离时有效
     */
	@TableField("deviate_type")
	private String deviateType;
    /**
     * 范围1~10。数值越大表示疲劳程度越严重，仅在报警类型为疲劳报警时有效
     */
	@TableField("fatigue_degree")
	private String fatigueDegree;
    /**
     * 前车/行人距离,单位ms，仅报警类型为前向碰撞、车距过近、行人碰撞时有效
     */
	@TableField("front_car_distance")
	private String frontCarDistance;
    /**
     * 道路标志识别数据,识别到道路标志的数据
     */
	@TableField("road_recognition_data")
	private String roadRecognitionData;
    /**
     * 前车车速,单位Km/h。范围0~250，仅报警类型为前车碰撞、车距过近时有效
     */
	@TableField("front_car_speed")
	private String frontCarSpeed;
    /**
     * 道路标志识别类型,1：限速标志,2：限高标志, 3：限重标志, 仅报警类型为标识超限、标志识别时有效
     */
	@TableField("road_recognition_mark")
	private Long roadRecognitionMark;
    /**
     * 该字段仅适用于有开始和结束标志类型的报警或事件，报警类型或事件类型无开始和结束标志，则该位不可用。标志状态0：不可用 1：开始标志 2：结束标志
     */
	@TableField("alarm_status")
	private Long alarmStatus;
    /**
     * 报警类型
     */
	private String event;
    /**
     * 报警等级
     */
	@TableField("alarm_level")
	private String alarmLevel;
    /**
     * 报警数据id
     */
	@TableField("alarm_id")
	private String alarmId;
    /**
     * 报警时间
     */
	@TableField("alarm_time")
	private Date alarmTime;
    /**
     * gps时间
     */
	@TableField("gps_time")
	private Date gpsTime;
    /**
     * 设备Id
     */
	@TableField("device_id")
	private String deviceId;
    /**
     * 电子围栏编号
     */
	@TableField("geo_fencing_no")
	private String geoFencingNo;
    /**
     * 设备类型
     */
	@TableField("device_type")
	private String deviceType;
    /**
     * 数据采集时间ms
     */
	@TableField("acquisition_time")
	private Date acquisitionTime;
    /**
     * ACC状态,0：关闭，1：打开
     */
	@TableField("acc_status")
	private Long accStatus;
    /**
     * 制动踏板状态,1：使用中，0：未使用
     */
	@TableField("brk_pedal_status")
	private Long brkPedalStatus;
    /**
     * 电瓶电压v
     */
	@TableField("battery_voltage")
	private BigDecimal batteryVoltage;
    /**
     * 发动机转速r/min
     */
	@TableField("engine_speed")
	private BigDecimal engineSpeed;
    /**
     * 剩余油量L
     */
	private BigDecimal fuel;
    /**
     * 纬度
     */
	private BigDecimal latitude;
    /**
     * 经度
     */
	private BigDecimal longitude;
    /**
     * 总里程km
     */
	@TableField("total_distance")
	private BigDecimal totalDistance;
    /**
     * 电池电量百分比%
     */
	@TableField("veh_bat_soc")
	private BigDecimal vehBatSoc;
    /**
     * 速度km/h
     */
	@TableField("vehicle_speed")
	private BigDecimal vehicleSpeed;
    /**
     * 真实加速踏板位置(%)
     */
	@TableField("acc_pedal_ratio")
	private BigDecimal accPedalRatio;
    /**
     * 创建时间
     */
	@TableField("gmt_create")
	private Date gmtCreate;
    /**
     * 更新时间
     */
	@TableField("gmt_modified")
	private Date gmtModified;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getVin() {
		return vin;
	}

	public void setVin(String vin) {
		this.vin = vin;
	}

	public String getDeviateType() {
		return deviateType;
	}

	public void setDeviateType(String deviateType) {
		this.deviateType = deviateType;
	}

	public String getFatigueDegree() {
		return fatigueDegree;
	}

	public void setFatigueDegree(String fatigueDegree) {
		this.fatigueDegree = fatigueDegree;
	}

	public String getFrontCarDistance() {
		return frontCarDistance;
	}

	public void setFrontCarDistance(String frontCarDistance) {
		this.frontCarDistance = frontCarDistance;
	}

	public String getRoadRecognitionData() {
		return roadRecognitionData;
	}

	public void setRoadRecognitionData(String roadRecognitionData) {
		this.roadRecognitionData = roadRecognitionData;
	}

	public String getFrontCarSpeed() {
		return frontCarSpeed;
	}

	public void setFrontCarSpeed(String frontCarSpeed) {
		this.frontCarSpeed = frontCarSpeed;
	}

	public Long getRoadRecognitionMark() {
		return roadRecognitionMark;
	}

	public void setRoadRecognitionMark(Long roadRecognitionMark) {
		this.roadRecognitionMark = roadRecognitionMark;
	}

	public Long getAlarmStatus() {
		return alarmStatus;
	}

	public void setAlarmStatus(Long alarmStatus) {
		this.alarmStatus = alarmStatus;
	}

	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}

	public String getAlarmLevel() {
		return alarmLevel;
	}

	public void setAlarmLevel(String alarmLevel) {
		this.alarmLevel = alarmLevel;
	}

	public String getAlarmId() {
		return alarmId;
	}

	public void setAlarmId(String alarmId) {
		this.alarmId = alarmId;
	}

	public Date getAlarmTime() {
		return alarmTime;
	}

	public void setAlarmTime(Date alarmTime) {
		this.alarmTime = alarmTime;
	}

	public Date getGpsTime() {
		return gpsTime;
	}

	public void setGpsTime(Date gpsTime) {
		this.gpsTime = gpsTime;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getGeoFencingNo() {
		return geoFencingNo;
	}

	public void setGeoFencingNo(String geoFencingNo) {
		this.geoFencingNo = geoFencingNo;
	}

	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	public Date getAcquisitionTime() {
		return acquisitionTime;
	}

	public void setAcquisitionTime(Date acquisitionTime) {
		this.acquisitionTime = acquisitionTime;
	}

	public Long getAccStatus() {
		return accStatus;
	}

	public void setAccStatus(Long accStatus) {
		this.accStatus = accStatus;
	}

	public Long getBrkPedalStatus() {
		return brkPedalStatus;
	}

	public void setBrkPedalStatus(Long brkPedalStatus) {
		this.brkPedalStatus = brkPedalStatus;
	}

	public BigDecimal getBatteryVoltage() {
		return batteryVoltage;
	}

	public void setBatteryVoltage(BigDecimal batteryVoltage) {
		this.batteryVoltage = batteryVoltage;
	}

	public BigDecimal getEngineSpeed() {
		return engineSpeed;
	}

	public void setEngineSpeed(BigDecimal engineSpeed) {
		this.engineSpeed = engineSpeed;
	}

	public BigDecimal getFuel() {
		return fuel;
	}

	public void setFuel(BigDecimal fuel) {
		this.fuel = fuel;
	}

	public BigDecimal getLatitude() {
		return latitude;
	}

	public void setLatitude(BigDecimal latitude) {
		this.latitude = latitude;
	}

	public BigDecimal getLongitude() {
		return longitude;
	}

	public void setLongitude(BigDecimal longitude) {
		this.longitude = longitude;
	}

	public BigDecimal getTotalDistance() {
		return totalDistance;
	}

	public void setTotalDistance(BigDecimal totalDistance) {
		this.totalDistance = totalDistance;
	}

	public BigDecimal getVehBatSoc() {
		return vehBatSoc;
	}

	public void setVehBatSoc(BigDecimal vehBatSoc) {
		this.vehBatSoc = vehBatSoc;
	}

	public BigDecimal getVehicleSpeed() {
		return vehicleSpeed;
	}

	public void setVehicleSpeed(BigDecimal vehicleSpeed) {
		this.vehicleSpeed = vehicleSpeed;
	}

	public BigDecimal getAccPedalRatio() {
		return accPedalRatio;
	}

	public void setAccPedalRatio(BigDecimal accPedalRatio) {
		this.accPedalRatio = accPedalRatio;
	}

	public Date getGmtCreate() {
		return gmtCreate;
	}

	public void setGmtCreate(Date gmtCreate) {
		this.gmtCreate = gmtCreate;
	}

	public Date getGmtModified() {
		return gmtModified;
	}

	public void setGmtModified(Date gmtModified) {
		this.gmtModified = gmtModified;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "C3ReportEventInfo{" +
			", id=" + id +
			", vin=" + vin +
			", deviateType=" + deviateType +
			", fatigueDegree=" + fatigueDegree +
			", frontCarDistance=" + frontCarDistance +
			", roadRecognitionData=" + roadRecognitionData +
			", frontCarSpeed=" + frontCarSpeed +
			", roadRecognitionMark=" + roadRecognitionMark +
			", alarmStatus=" + alarmStatus +
			", event=" + event +
			", alarmLevel=" + alarmLevel +
			", alarmId=" + alarmId +
			", alarmTime=" + alarmTime +
			", gpsTime=" + gpsTime +
			", deviceId=" + deviceId +
			", geoFencingNo=" + geoFencingNo +
			", deviceType=" + deviceType +
			", acquisitionTime=" + acquisitionTime +
			", accStatus=" + accStatus +
			", brkPedalStatus=" + brkPedalStatus +
			", batteryVoltage=" + batteryVoltage +
			", engineSpeed=" + engineSpeed +
			", fuel=" + fuel +
			", latitude=" + latitude +
			", longitude=" + longitude +
			", totalDistance=" + totalDistance +
			", vehBatSoc=" + vehBatSoc +
			", vehicleSpeed=" + vehicleSpeed +
			", accPedalRatio=" + accPedalRatio +
			", gmtCreate=" + gmtCreate +
			", gmtModified=" + gmtModified +
			"}";
	}
}
