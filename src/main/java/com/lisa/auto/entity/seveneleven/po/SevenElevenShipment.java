package com.lisa.auto.entity.seveneleven.po;

import cn.afterturn.easypoi.excel.annotation.Excel;

/**
 * @author : suohao     原始单
 * Date: 2020/1/3
 */
public class SevenElevenShipment {
        @Excel(name = "店铺")
        private String shopId;

        @Excel(name = "原箱SP")
        private String  originalBoxSP;

        @Excel(name = "散货SP")
        private String bulkSP;

        @Excel(name = "大点章")
        private String bigChapter;

        @Excel(name = "散货胶箱")
        private String bulkBoxNum;

        @Excel(name = "笼外胶箱")
        private String glueBoxNum;

        @Excel(name = "自动伞")
        private String selfOpeningUmbrella;

        @Excel(name = "公主伞")
        private String princessUmbrella;

        @Excel(name = "长柄伞")
        private String longUmbrella;

        @Excel(name = "格纹伞")
        private String plaidUmbrella;

        @Excel(name = "素色伞")
        private String colouredUmbrella;

        @Excel(name = "手柄伞")
        private String handleUmbrella;

        @Excel(name = "铺型")
        private String shopType;

        @Excel(name = "原箱笼数")
        private String originalLuggageNum;

        @Excel(name = "计费笼数")
        private String priceCageNum;

        @Excel(name = "原箱箱数")
        private String originalBoxNum;

        @Excel(name = "区域")
        private String region;

    public SevenElevenShipment(String shopId, String originalBoxSP, String region, String bulkSP, String bigChapter, String bulkBoxNum, String glueBoxNum, String selfOpeningUmbrella, String princessUmbrella, String longUmbrella, String plaidUmbrella, String colouredUmbrella, String handleUmbrella, String shopType, String originalLuggageNum, String priceCageNum, String originalBoxNum) {
        this.shopId = shopId;
        this.originalBoxSP = originalBoxSP;
        this.bulkSP = bulkSP;
        this.bigChapter = bigChapter;
        this.bulkBoxNum = bulkBoxNum;
        this.glueBoxNum = glueBoxNum;
        this.selfOpeningUmbrella = selfOpeningUmbrella;
        this.princessUmbrella = princessUmbrella;
        this.longUmbrella = longUmbrella;
        this.plaidUmbrella = plaidUmbrella;
        this.colouredUmbrella = colouredUmbrella;
        this.handleUmbrella = handleUmbrella;
        this.shopType = shopType;
        this.originalLuggageNum = originalLuggageNum;
        this.priceCageNum = priceCageNum;
        this.originalBoxNum = originalBoxNum;
        this.region = region;
    }


    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getOriginalBoxSP() {
        return originalBoxSP;
    }

    public void setOriginalBoxSP(String originalBoxSP) {
        this.originalBoxSP = originalBoxSP;
    }

    public String getBulkSP() {
        return bulkSP;
    }

    public void setBulkSP(String bulkSP) {
        this.bulkSP = bulkSP;
    }

    public String getBigChapter() {
        return bigChapter;
    }

    public void setBigChapter(String bigChapter) {
        this.bigChapter = bigChapter;
    }

    public String getBulkBoxNum() {
        return bulkBoxNum;
    }

    public void setBulkBoxNum(String bulkBoxNum) {
        this.bulkBoxNum = bulkBoxNum;
    }

    public String getGlueBoxNum() {
        return glueBoxNum;
    }

    public void setGlueBoxNum(String glueBoxNum) {
        this.glueBoxNum = glueBoxNum;
    }

    public String getSelfOpeningUmbrella() {
        return selfOpeningUmbrella;
    }

    public void setSelfOpeningUmbrella(String selfOpeningUmbrella) {
        this.selfOpeningUmbrella = selfOpeningUmbrella;
    }

    public String getPrincessUmbrella() {
        return princessUmbrella;
    }

    public void setPrincessUmbrella(String princessUmbrella) {
        this.princessUmbrella = princessUmbrella;
    }

    public String getLongUmbrella() {
        return longUmbrella;
    }

    public void setLongUmbrella(String longUmbrella) {
        this.longUmbrella = longUmbrella;
    }

    public String getPlaidUmbrella() {
        return plaidUmbrella;
    }

    public void setPlaidUmbrella(String plaidUmbrella) {
        this.plaidUmbrella = plaidUmbrella;
    }

    public String getColouredUmbrella() {
        return colouredUmbrella;
    }

    public void setColouredUmbrella(String colouredUmbrella) {
        this.colouredUmbrella = colouredUmbrella;
    }

    public String getHandleUmbrella() {
        return handleUmbrella;
    }

    public void setHandleUmbrella(String handleUmbrella) {
        this.handleUmbrella = handleUmbrella;
    }

    public String getShopType() {
        return shopType;
    }

    public void setShopType(String shopType) {
        this.shopType = shopType;
    }

    public String getOriginalLuggageNum() {
        return originalLuggageNum;
    }

    public void setOriginalLuggageNum(String originalLuggageNum) {
        this.originalLuggageNum = originalLuggageNum;
    }

    public String getPriceCageNum() {
        return priceCageNum;
    }

    public void setPriceCageNum(String priceCageNum) {
        this.priceCageNum = priceCageNum;
    }

    public String getOriginalBoxNum() {
        return originalBoxNum;
    }

    public void setOriginalBoxNum(String originalBoxNum) {
        this.originalBoxNum = originalBoxNum;
    }
}
