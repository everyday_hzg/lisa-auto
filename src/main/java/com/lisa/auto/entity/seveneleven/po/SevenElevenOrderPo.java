package com.lisa.auto.entity.seveneleven.po;

import cn.afterturn.easypoi.excel.annotation.Excel;

import java.io.Serializable;

/**
 * 711 订单信息(原始表) Po 类
 *
 * @author huangzhigang 2019/12/24
 */
public class SevenElevenOrderPo implements Serializable {

    @Excel(name = "店铺")
    private String shopId;

    @Excel(name = "笼车数量")
    private String cageNum;

    @Excel(name = "笼外胶箱")
    private String glueBoxNum;

    public SevenElevenOrderPo() {
        super();
    }

    public SevenElevenOrderPo(String shopId, String cageNum, String glueBoxNum) {
        this.shopId = shopId;
        this.cageNum = cageNum;
        this.glueBoxNum = glueBoxNum;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getCageNum() {
        return cageNum;
    }

    public void setCageNum(String cageNum) {
        this.cageNum = cageNum;
    }

    public String getGlueBoxNum() {
        return glueBoxNum;
    }

    public void setGlueBoxNum(String glueBoxNum) {
        this.glueBoxNum = glueBoxNum;
    }

}
