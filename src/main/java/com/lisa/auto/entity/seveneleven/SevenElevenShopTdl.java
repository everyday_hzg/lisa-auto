package com.lisa.auto.entity.seveneleven;

import cn.afterturn.easypoi.excel.annotation.Excel;

/**
 * 711店铺模版实体类
 *
 * @author huangzhigang 2019/12/2
 */
public class SevenElevenShopTdl implements java.io.Serializable{

    /**
     * 店铺ID
     */
    @Excel(name = "店号")
    private String shopId;

    /**
     * 店铺地址
     */
    @Excel(name = "地址")
    private String shopAddr;

    /**
     * 联系电话
     */
    @Excel(name = "电话")
    private String telephone;

    /**
     * 联系电话
     */
    @Excel(name = "店铺简称")
    private String shopName;

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getShopAddr() {
        return shopAddr;
    }

    public void setShopAddr(String shopAddr) {
        this.shopAddr = shopAddr;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }
}
