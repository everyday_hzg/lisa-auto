package com.lisa.auto.entity.seveneleven;

import cn.afterturn.easypoi.excel.annotation.Excel;

/**
 * 711模版实体类
 * @author huangzhigang 2019/12/2
 */
public class SevenElevenTdl {

    @Excel(name = "店铺")
    private String shopId;

    @Excel(name = "原箱笼数")
    private String cageNum;

    @Excel(name = "笼外胶箱")
    private String glueBoxNum;

    @Excel(name = "备注")
    private String estimatedTime;

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getCageNum() {
        return cageNum;
    }

    public void setCageNum(String cageNum) {
        this.cageNum = cageNum;
    }

    public String getGlueBoxNum() {
        return glueBoxNum;
    }

    public void setGlueBoxNum(String glueBoxNum) {
        this.glueBoxNum = glueBoxNum;
    }

    public String getEstimatedTime() {
        return estimatedTime;
    }

    public void setEstimatedTime(String estimatedTime) {
        this.estimatedTime = estimatedTime;
    }
}
