package com.lisa.auto.entity.seveneleven.po;

import cn.afterturn.easypoi.excel.annotation.Excel;

/**
 * @author : suohao
 * Date: 2020/1/4
 */
public class SevenElevenShipmentDto {

    private String driverName;
    private String plate;
    @Excel(name = "店铺", orderNum = "3", width = 8)
    private String shopId;
    @Excel(name = "线路", width = 8)
    private String lineId;
    @Excel(name = "站点", orderNum = "1", width = 8)
    private String stationId;
    @Excel(name = "次序", orderNum = "2", width = 8)
    private Integer orderId;
    @Excel(name = "是否留箱", orderNum = "20", width = 10)
    private String leaveBox;
    @Excel(name = "送货周期", orderNum = "6", width = 10)
    private String shopType;
    @Excel(name = "备注", orderNum = "25", width = 10)
    private String estimatedTime;
    @Excel(name = "店铺电话", orderNum = "24", width = 10)
    private String tel;
    @Excel(name = "原箱SP", orderNum = "4", width = 8)
    private String originalBoxSP;
    @Excel(name = "散货SP", orderNum = "5", width = 8)
    private String bulkSP;
    @Excel(name = "大点章", orderNum = "7", width = 8)
    private String bigChapter;
    @Excel(name = "散货胶箱", orderNum = "8", width = 10)
    private Integer bulkBoxNum;
    @Excel(name = "笼外胶箱", orderNum = "9", width = 10)
    private Integer glueBoxNum;
    @Excel(name = "自动伞", orderNum = "10", width = 8)
    private Integer selfOpeningUmbrella;
    @Excel(name = "公主伞", orderNum = "11", width = 8)
    private Integer princessUmbrella;
    @Excel(name = "长柄伞", orderNum = "12", width = 8)
    private Integer longUmbrella;
    @Excel(name = "格纹伞", orderNum = "13", width = 8)
    private Integer plaidUmbrella;
    @Excel(name = "素色伞", orderNum = "14", width = 8)
    private Integer colouredUmbrella;
    @Excel(name = "手柄伞", orderNum = "15", width = 8)
    private Integer handleUmbrella;
    @Excel(name = "铺型", orderNum = "19", width = 8)
    private String shopType1;
    @Excel(name = "原箱笼数", orderNum = "21", width = 10)
    private Integer originalLuggageNum;
    @Excel(name = "计费笼数", orderNum = "22", width = 10)
    private Double priceCageNum;
    @Excel(name = "原箱箱数", orderNum = "23", width = 10)
    private Integer originalBoxNum;
    @Excel(name = "CO2", orderNum = "16", width = 8)
    private String cO2;
    @Excel(name = "备用一", orderNum = "17", width = 8)
    private String spare;
    @Excel(name = "区域", orderNum = "18", width = 8)
    private String region;

    public SevenElevenShipmentDto() {
        super();
    }

    public SevenElevenShipmentDto(Integer orderId, String cO2, String spare,String bigChapter) {
        this.orderId = orderId;
        this.cO2 = cO2;
        this.spare = spare;
        this.bigChapter = bigChapter;
    }

    public SevenElevenShipmentDto(String driverName, String plate, String shopId, String lineId, String stationId, Integer orderId, String leaveBox,
                                  String shopType, String estimatedTime, String tel, String originalBoxSP, String bulkSP, String bigChapter, Integer bulkBoxNum,
                                  Integer glueBoxNum, Integer selfOpeningUmbrella, Integer princessUmbrella, Integer longUmbrella, Integer plaidUmbrella,
                                  Integer colouredUmbrella, Integer handleUmbrella, String shopType1, Integer originalLuggageNum, Double priceCageNum,
                                  Integer originalBoxNum, String cO2, String spare, String region) {
        this.driverName = driverName;
        this.plate = plate;
        this.shopId = shopId;
        this.lineId = lineId;
        this.stationId = stationId;
        this.orderId = orderId;
        this.leaveBox = leaveBox;
        this.shopType = shopType;
        this.estimatedTime = estimatedTime;
        this.tel = tel;
        this.originalBoxSP = originalBoxSP;
        this.bulkSP = bulkSP;
        this.bigChapter = bigChapter;
        this.bulkBoxNum = bulkBoxNum;
        this.glueBoxNum = glueBoxNum;
        this.selfOpeningUmbrella = selfOpeningUmbrella;
        this.princessUmbrella = princessUmbrella;
        this.longUmbrella = longUmbrella;
        this.plaidUmbrella = plaidUmbrella;
        this.colouredUmbrella = colouredUmbrella;
        this.handleUmbrella = handleUmbrella;
        this.shopType1 = shopType1;
        this.originalLuggageNum = originalLuggageNum;
        this.priceCageNum = priceCageNum;
        this.originalBoxNum = originalBoxNum;
        this.cO2 = cO2;
        this.spare = spare;
        this.region = region;
    }

    public SevenElevenShipmentDto(String shopId, Integer bulkBoxNum, Integer glueBoxNum, Integer selfOpeningUmbrella, Integer princessUmbrella, Integer longUmbrella,
                                  Integer plaidUmbrella, Integer colouredUmbrella, Integer handleUmbrella, Integer originalLuggageNum, Double priceCageNum, Integer originalBoxNum) {
        this.shopId = shopId;
        this.bulkBoxNum = bulkBoxNum;
        this.glueBoxNum = glueBoxNum;
        this.selfOpeningUmbrella = selfOpeningUmbrella;
        this.princessUmbrella = princessUmbrella;
        this.longUmbrella = longUmbrella;
        this.plaidUmbrella = plaidUmbrella;
        this.colouredUmbrella = colouredUmbrella;
        this.handleUmbrella = handleUmbrella;
        this.originalLuggageNum = originalLuggageNum;
        this.priceCageNum = priceCageNum;
        this.originalBoxNum = originalBoxNum;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getLineId() {
        return lineId;
    }

    public void setLineId(String lineId) {
        this.lineId = lineId;
    }

    public String getStationId() {
        return stationId;
    }

    public void setStationId(String stationId) {
        this.stationId = stationId;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public String getLeaveBox() {
        return leaveBox;
    }

    public void setLeaveBox(String leaveBox) {
        this.leaveBox = leaveBox;
    }

    public String getShopType() {
        return shopType;
    }

    public void setShopType(String shopType) {
        this.shopType = shopType;
    }

    public String getEstimatedTime() {
        return estimatedTime;
    }

    public void setEstimatedTime(String estimatedTime) {
        this.estimatedTime = estimatedTime;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getOriginalBoxSP() {
        return originalBoxSP;
    }

    public void setOriginalBoxSP(String originalBoxSP) {
        this.originalBoxSP = originalBoxSP;
    }

    public String getBulkSP() {
        return bulkSP;
    }

    public void setBulkSP(String bulkSP) {
        this.bulkSP = bulkSP;
    }

    public String getBigChapter() {
        return bigChapter;
    }

    public void setBigChapter(String bigChapter) {
        this.bigChapter = bigChapter;
    }

    public Integer getBulkBoxNum() {
        return bulkBoxNum;
    }

    public void setBulkBoxNum(Integer bulkBoxNum) {
        this.bulkBoxNum = bulkBoxNum;
    }

    public Integer getGlueBoxNum() {
        return glueBoxNum;
    }

    public void setGlueBoxNum(Integer glueBoxNum) {
        this.glueBoxNum = glueBoxNum;
    }

    public Integer getSelfOpeningUmbrella() {
        return selfOpeningUmbrella;
    }

    public void setSelfOpeningUmbrella(Integer selfOpeningUmbrella) {
        this.selfOpeningUmbrella = selfOpeningUmbrella;
    }

    public Integer getPrincessUmbrella() {
        return princessUmbrella;
    }

    public void setPrincessUmbrella(Integer princessUmbrella) {
        this.princessUmbrella = princessUmbrella;
    }

    public Integer getLongUmbrella() {
        return longUmbrella;
    }

    public void setLongUmbrella(Integer longUmbrella) {
        this.longUmbrella = longUmbrella;
    }

    public Integer getPlaidUmbrella() {
        return plaidUmbrella;
    }

    public void setPlaidUmbrella(Integer plaidUmbrella) {
        this.plaidUmbrella = plaidUmbrella;
    }

    public Integer getColouredUmbrella() {
        return colouredUmbrella;
    }

    public void setColouredUmbrella(Integer colouredUmbrella) {
        this.colouredUmbrella = colouredUmbrella;
    }

    public Integer getHandleUmbrella() {
        return handleUmbrella;
    }

    public void setHandleUmbrella(Integer handleUmbrella) {
        this.handleUmbrella = handleUmbrella;
    }

    public String getShopType1() {
        return shopType1;
    }

    public void setShopType1(String shopType1) {
        this.shopType1 = shopType1;
    }

    public Integer getOriginalLuggageNum() {
        return originalLuggageNum;
    }

    public void setOriginalLuggageNum(Integer originalLuggageNum) {
        this.originalLuggageNum = originalLuggageNum;
    }

    public Double getPriceCageNum() {
        return priceCageNum;
    }

    public void setPriceCageNum(Double priceCageNum) {
        this.priceCageNum = priceCageNum;
    }

    public Integer getOriginalBoxNum() {
        return originalBoxNum;
    }

    public void setOriginalBoxNum(Integer originalBoxNum) {
        this.originalBoxNum = originalBoxNum;
    }

    public String getcO2() {
        return cO2;
    }

    public void setcO2(String cO2) {
        this.cO2 = cO2;
    }

    public String getSpare() {
        return spare;
    }

    public void setSpare(String spare) {
        this.spare = spare;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }
}
