package com.lisa.auto.entity.seveneleven.po;

import cn.afterturn.easypoi.excel.annotation.Excel;

/**
 * @author huangzhigang 2019/12/27
 */
public class SevenElevenShopAddrPo {

    /** 店铺ID */
    @Excel(name = "店号")
    private String shopId;

    /** 店铺地址 */
    @Excel(name = "地址")
    private String shopAddr;

    /** 地址编号 */
    @Excel(name = "地址编号")
    private String shopAddrNo;

    /** 地址编号 */
    @Excel(name = "地址编号")
    private String shopAddrType;

    private String externalNum;

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getShopAddr() {
        return shopAddr;
    }

    public void setShopAddr(String shopAddr) {
        this.shopAddr = shopAddr;
    }

    public String getShopAddrNo() {
        return shopAddrNo;
    }

    public void setShopAddrNo(String shopAddrNo) {
        this.shopAddrNo = shopAddrNo;
    }

    public String getShopAddrType() {
        return shopAddrType;
    }

    public void setShopAddrType(String shopAddrType) {
        this.shopAddrType = shopAddrType;
    }

    public String getExternalNum() {
        return externalNum;
    }

    public void setExternalNum(String externalNum) {
        this.externalNum = externalNum;
    }
}
