package com.lisa.auto.entity.seveneleven.po;

import cn.afterturn.easypoi.excel.annotation.Excel;

import java.util.Objects;

/**
 * @author : suohao
 * Date: 2020/1/3
 */
public class SevenElevenVehiclePo {
    //企业			company
    @Excel(name = "企业")
    private String company;
    @Excel(name = "客户/品牌名")
    private String brand;
    @Excel(name = "车次编号")
    private String trainNo;
    @Excel(name = "司机")
    private String driverName;
    @Excel(name = "车牌号")
    private String plate;
    @Excel(name = "司机手机")
    private String driverPhone;
    @Excel(name = "剩余体积m³")
    private String residualVolume;
    @Excel(name = "剩余重量kg")
    private String residualWeight;
    @Excel(name = "司机收入费用")
    private String price;
    @Excel(name = "车次状态")
    private String status;
    @Excel(name = "取货地址")
    private String pickupAddr;
    @Excel(name = "取货地址编号")
    private String pickupAddrNo;
    @Excel(name = "取货时间段")
    private String pickupDate;
    @Excel(name = "外部运单编号")
    private String orderNo;
    @Excel(name = "卸货地址")
    private String unloadingAddr;
    @Excel(name = "外部店铺编号")
    private String shopId;
    @Excel(name = "卸货地址编号")
    private String unloadingAddrNo;
    @Excel(name = "卸货联系人")
    private String contactName;
    @Excel(name = "卸货联系人电话")
    private String contactPhone;
    @Excel(name = "卸货时间段")
    private String unloadingDate;
    @Excel(name = "包裹类型")
    private String packageType;
    @Excel(name = "包裹数量")
    private String packageNum;
    @Excel(name = "包裹容积")
    private String packageVolume;
    @Excel(name = "包裹重量")
    private String packageWeight;
    @Excel(name = "运费")
    private String shipPrice;
    @Excel(name = "备注")
    private String remark;

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getTrainNo() {
        return trainNo;
    }

    public void setTrainNo(String trainNo) {
        this.trainNo = trainNo;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    public String getDriverPhone() {
        return driverPhone;
    }

    public void setDriverPhone(String driverPhone) {
        this.driverPhone = driverPhone;
    }

    public String getResidualVolume() {
        return residualVolume;
    }

    public void setResidualVolume(String residualVolume) {
        this.residualVolume = residualVolume;
    }

    public String getResidualWeight() {
        return residualWeight;
    }

    public void setResidualWeight(String residualWeight) {
        this.residualWeight = residualWeight;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPickupAddr() {
        return pickupAddr;
    }

    public void setPickupAddr(String pickupAddr) {
        this.pickupAddr = pickupAddr;
    }

    public String getPickupAddrNo() {
        return pickupAddrNo;
    }

    public void setPickupAddrNo(String pickupAddrNo) {
        this.pickupAddrNo = pickupAddrNo;
    }

    public String getPickupDate() {
        return pickupDate;
    }

    public void setPickupDate(String pickupDate) {
        this.pickupDate = pickupDate;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getUnloadingAddr() {
        return unloadingAddr;
    }

    public void setUnloadingAddr(String unloadingAddr) {
        this.unloadingAddr = unloadingAddr;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getUnloadingAddrNo() {
        return unloadingAddrNo;
    }

    public void setUnloadingAddrNo(String unloadingAddrNo) {
        this.unloadingAddrNo = unloadingAddrNo;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactPhone() {
        return contactPhone;
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }

    public String getUnloadingDate() {
        return unloadingDate;
    }

    public void setUnloadingDate(String unloadingDate) {
        this.unloadingDate = unloadingDate;
    }

    public String getPackageType() {
        return packageType;
    }

    public void setPackageType(String packageType) {
        this.packageType = packageType;
    }

    public String getPackageNum() {
        return packageNum;
    }

    public void setPackageNum(String packageNum) {
        this.packageNum = packageNum;
    }

    public String getPackageVolume() {
        return packageVolume;
    }

    public void setPackageVolume(String packageVolume) {
        this.packageVolume = packageVolume;
    }

    public String getPackageWeight() {
        return packageWeight;
    }

    public void setPackageWeight(String packageWeight) {
        this.packageWeight = packageWeight;
    }

    public String getShipPrice() {
        return shipPrice;
    }

    public void setShipPrice(String shipPrice) {
        this.shipPrice = shipPrice;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SevenElevenVehiclePo that = (SevenElevenVehiclePo) o;
        return Objects.equals(company, that.company) &&
                Objects.equals(brand, that.brand) &&
                Objects.equals(trainNo, that.trainNo) &&
                Objects.equals(driverName, that.driverName) &&
                Objects.equals(plate, that.plate) &&
                Objects.equals(driverPhone, that.driverPhone) &&
                Objects.equals(residualVolume, that.residualVolume) &&
                Objects.equals(residualWeight, that.residualWeight) &&
                Objects.equals(price, that.price) &&
                Objects.equals(status, that.status) &&
                Objects.equals(pickupAddr, that.pickupAddr) &&
                Objects.equals(pickupAddrNo, that.pickupAddrNo) &&
                Objects.equals(pickupDate, that.pickupDate) &&
                Objects.equals(orderNo, that.orderNo) &&
                Objects.equals(unloadingAddr, that.unloadingAddr) &&
                Objects.equals(shopId, that.shopId) &&
                Objects.equals(unloadingAddrNo, that.unloadingAddrNo) &&
                Objects.equals(contactName, that.contactName) &&
                Objects.equals(contactPhone, that.contactPhone) &&
                Objects.equals(unloadingDate, that.unloadingDate) &&
                Objects.equals(packageType, that.packageType) &&
                Objects.equals(packageNum, that.packageNum) &&
                Objects.equals(packageVolume, that.packageVolume) &&
                Objects.equals(packageWeight, that.packageWeight) &&
                Objects.equals(shipPrice, that.shipPrice) &&
                Objects.equals(remark, that.remark);
    }

    @Override
    public int hashCode() {
        return Objects.hash(company, brand, trainNo, driverName, plate, driverPhone, residualVolume, residualWeight, price, status, pickupAddr, pickupAddrNo, pickupDate, orderNo, unloadingAddr, shopId, unloadingAddrNo, contactName, contactPhone, unloadingDate, packageType, packageNum, packageVolume, packageWeight, shipPrice, remark);
    }

    public SevenElevenVehiclePo(String company, String brand, String trainNo, String driverName, String plate, String driverPhone, String residualVolume, String residualWeight, String price, String status, String pickupAddr, String pickupAddrNo, String pickupDate, String orderNo, String unloadingAddr, String shopId, String unloadingAddrNo, String contactName, String contactPhone, String unloadingDate, String packageType, String packageNum, String packageVolume, String packageWeight, String shipPrice, String remark) {
        this.company = company;
        this.brand = brand;
        this.trainNo = trainNo;
        this.driverName = driverName;
        this.plate = plate;
        this.driverPhone = driverPhone;
        this.residualVolume = residualVolume;
        this.residualWeight = residualWeight;
        this.price = price;
        this.status = status;
        this.pickupAddr = pickupAddr;
        this.pickupAddrNo = pickupAddrNo;
        this.pickupDate = pickupDate;
        this.orderNo = orderNo;
        this.unloadingAddr = unloadingAddr;
        this.shopId = shopId;
        this.unloadingAddrNo = unloadingAddrNo;
        this.contactName = contactName;
        this.contactPhone = contactPhone;
        this.unloadingDate = unloadingDate;
        this.packageType = packageType;
        this.packageNum = packageNum;
        this.packageVolume = packageVolume;
        this.packageWeight = packageWeight;
        this.shipPrice = shipPrice;
        this.remark = remark;
    }
}
