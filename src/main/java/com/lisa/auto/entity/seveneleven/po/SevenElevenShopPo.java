package com.lisa.auto.entity.seveneleven.po;

import cn.afterturn.easypoi.excel.annotation.Excel;

import java.io.Serializable;

/**
 * 711 店铺信息Po
 *
 * @author huangzhigang 2019/12/24
 */
public class SevenElevenShopPo implements Serializable {

    @Excel(name = "店铺")
    private String shopId;

    @Excel(name = "送货时间")
    private String estimatedTime;

    @Excel(name = "电话号码")
    private String tel;

    public SevenElevenShopPo() {
        super();
    }

    public SevenElevenShopPo(String shopId, String estimatedTime, String tel) {
        this.shopId = shopId;
        this.estimatedTime = estimatedTime;
        this.tel = tel;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getEstimatedTime() {
        return estimatedTime;
    }

    public void setEstimatedTime(String estimatedTime) {
        this.estimatedTime = estimatedTime;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }
}

