package com.lisa.auto.entity.seveneleven.po;

import cn.afterturn.easypoi.excel.annotation.Excel;

import java.io.Serializable;

/**
 * @author huangzhigang 2019/12/27
 */
public class SevenElevenAddrPo implements Serializable {

    @Excel(name = "地址编号")
    private String addrNum;

    @Excel(name = "店铺简称")
    private String addrName;

    public SevenElevenAddrPo() {
        super();
    }

    public SevenElevenAddrPo(String addrNum, String addrName) {
        this.addrNum = addrNum;
        this.addrName = addrName;
    }

    public String getAddrNum() {
        return addrNum;
    }

    public void setAddrNum(String addrNum) {
        this.addrNum = addrNum;
    }

    public String getAddrName() {
        return addrName;
    }

    public void setAddrName(String addrName) {
        this.addrName = addrName;
    }
}
