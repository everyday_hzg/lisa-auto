package com.lisa.auto.entity.seveneleven.po;

import cn.afterturn.easypoi.excel.annotation.Excel;

/**
 * @author : suohao
 * Date: 2020/1/3
 */
public class SevenElevenShopDto {
    @Excel(name = "店铺")
    private String shopId;
    @Excel(name = "线")
    private String lineId;
    @Excel(name = "站")
    private String stationId;
    @Excel(name = "是否留箱")
    private String leaveBox;
    @Excel(name = "铺型")
    private String shopType;
    @Excel(name = "送货时间")
    private String estimatedTime;
    @Excel(name = "电话号码")
    private String tel;

    public SevenElevenShopDto(String shopId, String lineId, String stationId, String leaveBox, String shopType, String estimatedTime, String tel) {
        this.shopId = shopId;
        this.lineId = lineId;
        this.stationId = stationId;
        this.leaveBox = leaveBox;
        this.shopType = shopType;
        this.estimatedTime = estimatedTime;
        this.tel = tel;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getLineId() {
        return lineId;
    }

    public void setLineId(String lineId) {
        this.lineId = lineId;
    }

    public String getStationId() {
        return stationId;
    }

    public void setStationId(String stationId) {
        this.stationId = stationId;
    }

    public String getleaveBox() {
        return leaveBox;
    }

    public void setleaveBox(String leaveBox) {
        this.leaveBox = leaveBox;
    }

    public String getShopType() {
        return shopType;
    }

    public void setShopType(String shopType) {
        this.shopType = shopType;
    }

    public String getEstimatedTime() {
        return estimatedTime;
    }

    public void setEstimatedTime(String estimatedTime) {
        this.estimatedTime = estimatedTime;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }
}
