package com.lisa.auto.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 车况信息
 * </p>
 *
 * @author suohao
 * @since 2019-12-20
 */
@TableName("c3_car_info")
public class C3CarInfo extends Model<C3CarInfo> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
	@TableId(value="id", type= IdType.AUTO)
	private Long id;
    /**
     * 车辆VIN码
     */
	private String vin;
    /**
     * 数据采集时间ms
     */
	@TableField("acquisition_time")
	private Date acquisitionTime;
    /**
     * 电瓶电压v
     */
	@TableField("battery_voltage")
	private BigDecimal batteryVoltage;
    /**
     * 发动机转速r/min
     */
	@TableField("engine_speed")
	private BigDecimal engineSpeed;
    /**
     * 剩余油量L
     */
	private BigDecimal fuel;
    /**
     * 纬度
     */
	private BigDecimal latitude;
    /**
     * 经度
     */
	private BigDecimal longitude;
    /**
     * 总里程km
     */
	@TableField("total_distance")
	private BigDecimal totalDistance;
    /**
     * 电池电量百分比%
     */
	@TableField("veh_bat_soc")
	private BigDecimal vehBatSoc;
    /**
     * 速度km/h
     */
	@TableField("vehicle_speed")
	private BigDecimal vehicleSpeed;
    /**
     * 创建时间
     */
	@TableField("gmt_create")
	private Date gmtCreate;
    /**
     * 更新时间
     */
	@TableField("gmt_modified")
	private Date gmtModified;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getVin() {
		return vin;
	}

	public void setVin(String vin) {
		this.vin = vin;
	}

	public Date getAcquisitionTime() {
		return acquisitionTime;
	}

	public void setAcquisitionTime(Date acquisitionTime) {
		this.acquisitionTime = acquisitionTime;
	}

	public BigDecimal getBatteryVoltage() {
		return batteryVoltage;
	}

	public void setBatteryVoltage(BigDecimal batteryVoltage) {
		this.batteryVoltage = batteryVoltage;
	}

	public BigDecimal getEngineSpeed() {
		return engineSpeed;
	}

	public void setEngineSpeed(BigDecimal engineSpeed) {
		this.engineSpeed = engineSpeed;
	}

	public BigDecimal getFuel() {
		return fuel;
	}

	public void setFuel(BigDecimal fuel) {
		this.fuel = fuel;
	}

	public BigDecimal getLatitude() {
		return latitude;
	}

	public void setLatitude(BigDecimal latitude) {
		this.latitude = latitude;
	}

	public BigDecimal getLongitude() {
		return longitude;
	}

	public void setLongitude(BigDecimal longitude) {
		this.longitude = longitude;
	}

	public BigDecimal getTotalDistance() {
		return totalDistance;
	}

	public void setTotalDistance(BigDecimal totalDistance) {
		this.totalDistance = totalDistance;
	}

	public BigDecimal getVehBatSoc() {
		return vehBatSoc;
	}

	public void setVehBatSoc(BigDecimal vehBatSoc) {
		this.vehBatSoc = vehBatSoc;
	}

	public BigDecimal getVehicleSpeed() {
		return vehicleSpeed;
	}

	public void setVehicleSpeed(BigDecimal vehicleSpeed) {
		this.vehicleSpeed = vehicleSpeed;
	}

	public Date getGmtCreate() {
		return gmtCreate;
	}

	public void setGmtCreate(Date gmtCreate) {
		this.gmtCreate = gmtCreate;
	}

	public Date getGmtModified() {
		return gmtModified;
	}

	public void setGmtModified(Date gmtModified) {
		this.gmtModified = gmtModified;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "C3CarInfo{" +
			", id=" + id +
			", vin=" + vin +
			", acquisitionTime=" + acquisitionTime +
			", batteryVoltage=" + batteryVoltage +
			", engineSpeed=" + engineSpeed +
			", fuel=" + fuel +
			", latitude=" + latitude +
			", longitude=" + longitude +
			", totalDistance=" + totalDistance +
			", vehBatSoc=" + vehBatSoc +
			", vehicleSpeed=" + vehicleSpeed +
			", gmtCreate=" + gmtCreate +
			", gmtModified=" + gmtModified +
			"}";
	}
}
