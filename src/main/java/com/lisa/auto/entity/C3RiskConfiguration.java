package com.lisa.auto.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 车辆风险配置
 * </p>
 *
 * @author suohao
 * @since 2020-02-21
 */
@TableName("c3_risk_configuration")
public class C3RiskConfiguration extends Model<C3RiskConfiguration> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
	@TableId(value="id", type= IdType.AUTO)
	private Long id;
    /**
     * 外部唯一标识
     */
	@TableField("event_id")
	private String eventId;
    /**
     * 车辆设备
     */
	@TableField("vehicle_equipment")
	private String vehicleEquipment;
    /**
     * 风险类型
     */
	@TableField("risk_type")
	private String riskType;
    /**
     * 设备详细故障项
     */
	@TableField("equipment_risk_item")
	private String equipmentRiskItem;
    /**
     * 风险等级数值
     */
	@TableField("risk_level")
	private Integer riskLevel;
    /**
     * 备注
     */
	private String remark;
    /**
     * 创建时间
     */
	@TableField("gmt_create")
	private Date gmtCreate;
	/**
     * 创建时间
     */
	@TableField("type")
	private int type;



	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEventId() {
		return eventId;
	}

	public void setEventId(String eventId) {
		this.eventId = eventId;
	}

	public String getVehicleEquipment() {
		return vehicleEquipment;
	}

	public void setVehicleEquipment(String vehicleEquipment) {
		this.vehicleEquipment = vehicleEquipment;
	}

	public String getRiskType() {
		return riskType;
	}

	public void setRiskType(String riskType) {
		this.riskType = riskType;
	}

	public String getEquipmentRiskItem() {
		return equipmentRiskItem;
	}

	public void setEquipmentRiskItem(String equipmentRiskItem) {
		this.equipmentRiskItem = equipmentRiskItem;
	}

	public Integer getRiskLevel() {
		return riskLevel;
	}

	public void setRiskLevel(Integer riskLevel) {
		this.riskLevel = riskLevel;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Date getGmtCreate() {
		return gmtCreate;
	}

	public void setGmtCreate(Date gmtCreate) {
		this.gmtCreate = gmtCreate;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "C3RiskConfiguration{" +
			", id=" + id +
			", eventId=" + eventId +
			", vehicleEquipment=" + vehicleEquipment +
			", riskType=" + riskType +
			", equipmentRiskItem=" + equipmentRiskItem +
			", riskLevel=" + riskLevel +
			", remark=" + remark +
			", gmtCreate=" + gmtCreate +
			"}";
	}
}
