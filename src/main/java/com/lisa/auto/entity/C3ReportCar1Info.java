package com.lisa.auto.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 上报车况信息
 * </p>
 *
 * @author suohao
 * @since 2019-12-25
 */
@TableName("c3_report_car_info_1")
public class C3ReportCar1Info extends Model<C3ReportCar1Info> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
	@TableId(value="id", type= IdType.AUTO)
	private Long id;
    /**
     * 车辆VIN码
     */
	private String vin;
    /**
     * 设备Id
     */
	@TableField("device_id")
	private String deviceId;
    /**
     * 设备类型
     */
	@TableField("device_type")
	private String deviceType;
	/**
     * 省
     */
	@TableField("province")
	private String province;
	/**
     * 市
     */
	@TableField("city")
	private String city;
	/**
     * 区
     */
	@TableField("county")
	private String county;
	/**
	 * 区
	 */
	@TableField("detailed_address")
	private String detailedAddr;
	/**
     * 区
     */
	@TableField("type")
	private String type;
    /**
     * 数据采集时间ms
     */
	@TableField("acquisition_time")
	private Date acquisitionTime;
    /**
     * ACC状态,0：关闭，1：打开
     */
	@TableField("acc_status")
	private Long accStatus;
	/**
     * 充电状态(0：未充电，1：充电中，2：充电完成，3：充电故障)
     */
	@TableField("charge_status")
	private Long chargeStatus;
    /**
     * 制动踏板状态,1：使用中，0：未使用
     */
	@TableField("brk_pedal_status")
	private Long brkPedalStatus;
    /**
     * 电瓶电压v
     */
	@TableField("battery_voltage")
	private BigDecimal batteryVoltage;
    /**
     * 发动机转速r/min
     */
	@TableField("engine_speed")
	private BigDecimal engineSpeed;
    /**
     * 剩余油量L
     */
	private BigDecimal fuel;
    /**
     * 纬度
     */
	private BigDecimal latitude;
    /**
     * 经度
     */
	private BigDecimal longitude;
    /**
     * 总里程km
     */
	@TableField("total_distance")
	private BigDecimal totalDistance;
    /**
     * 电池电量百分比%
     */
	@TableField("veh_bat_soc")
	private BigDecimal vehBatSoc;
    /**
     * 速度km/h
     */
	@TableField("vehicle_speed")
	private BigDecimal vehicleSpeed;
    /**
     * 创建时间
     */
	@TableField("gmt_create")
	private Date gmtCreate;
    /**
     * 更新时间
     */
	@TableField("gmt_modified")
	private Date gmtModified;


	/*刹车踏板状态,%*/
	@TableField("brake_pedal_status")
	private String brakePedalStatus;

	/*加速踏板,%*/
	@TableField("accelerator_pedal")
	private String acceleratorPedal;

	/*近光灯指示,0：关，1：开*/
	@TableField("low_beam")
	private Integer lowBeam;

	/*远光灯指示,0：关，1：开*/
	@TableField("high_beam")
	private Integer highBeam;

	/*位置灯指示,0：关，1：开*/
	@TableField("position_lamp")
	private Integer positionLamp;

	/*后备箱门状态,0：关，1：开*/
	@TableField("door_sts_trunk")
	private Integer doorStsTrunk;

	/*右后门状态,0：关，1：开*/
	@TableField("door_sts_rear_right")
	private Integer doorStsRearRight;

	/*左后门状态,0：关，1：开*/
	@TableField("door_sts_rear_left")
	private Integer doorStsRearLeft;

	/*右前门状态,0：关，1：开*/
	@TableField("door_sts_front_right")
	private Integer doorStsFrontRight;

	/*左前门状态,0：关，1：开*/
	@TableField("door_sts_front_left")
	private Integer doorStsFrontLeft;

	public Long getChargeStatus() {
		return chargeStatus;
	}

	public void setChargeStatus(Long chargeStatus) {
		this.chargeStatus = chargeStatus;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getVin() {
		return vin;
	}

	public void setVin(String vin) {
		this.vin = vin;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	public Date getAcquisitionTime() {
		return acquisitionTime;
	}

	public void setAcquisitionTime(Date acquisitionTime) {
		this.acquisitionTime = acquisitionTime;
	}

	public Long getAccStatus() {
		return accStatus;
	}

	public void setAccStatus(Long accStatus) {
		this.accStatus = accStatus;
	}

	public Long getBrkPedalStatus() {
		return brkPedalStatus;
	}

	public void setBrkPedalStatus(Long brkPedalStatus) {
		this.brkPedalStatus = brkPedalStatus;
	}

	public BigDecimal getBatteryVoltage() {
		return batteryVoltage;
	}

	public void setBatteryVoltage(BigDecimal batteryVoltage) {
		this.batteryVoltage = batteryVoltage;
	}

	public BigDecimal getEngineSpeed() {
		return engineSpeed;
	}

	public void setEngineSpeed(BigDecimal engineSpeed) {
		this.engineSpeed = engineSpeed;
	}

	public BigDecimal getFuel() {
		return fuel;
	}

	public void setFuel(BigDecimal fuel) {
		this.fuel = fuel;
	}

	public BigDecimal getLatitude() {
		return latitude;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setLatitude(BigDecimal latitude) {
		this.latitude = latitude;
	}

	public BigDecimal getLongitude() {
		return longitude;
	}

	public void setLongitude(BigDecimal longitude) {
		this.longitude = longitude;
	}

	public BigDecimal getTotalDistance() {
		return totalDistance;
	}

	public void setTotalDistance(BigDecimal totalDistance) {
		this.totalDistance = totalDistance;
	}

	public BigDecimal getVehBatSoc() {
		return vehBatSoc;
	}

	public void setVehBatSoc(BigDecimal vehBatSoc) {
		this.vehBatSoc = vehBatSoc;
	}

	public BigDecimal getVehicleSpeed() {
		return vehicleSpeed;
	}

	public void setVehicleSpeed(BigDecimal vehicleSpeed) {
		this.vehicleSpeed = vehicleSpeed;
	}

	public Date getGmtCreate() {
		return gmtCreate;
	}

	public void setGmtCreate(Date gmtCreate) {
		this.gmtCreate = gmtCreate;
	}

	public Date getGmtModified() {
		return gmtModified;
	}

	public void setGmtModified(Date gmtModified) {
		this.gmtModified = gmtModified;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public String getBrakePedalStatus() {
		return brakePedalStatus;
	}

	public void setBrakePedalStatus(String brakePedalStatus) {
		this.brakePedalStatus = brakePedalStatus;
	}

	public String getAcceleratorPedal() {
		return acceleratorPedal;
	}

	public void setAcceleratorPedal(String acceleratorPedal) {
		this.acceleratorPedal = acceleratorPedal;
	}

	public Integer getLowBeam() {
		return lowBeam;
	}

	public void setLowBeam(Integer lowBeam) {
		this.lowBeam = lowBeam;
	}

	public Integer getHighBeam() {
		return highBeam;
	}

	public void setHighBeam(Integer highBeam) {
		this.highBeam = highBeam;
	}

	public Integer getPositionLamp() {
		return positionLamp;
	}

	public void setPositionLamp(Integer positionLamp) {
		this.positionLamp = positionLamp;
	}

	public Integer getDoorStsTrunk() {
		return doorStsTrunk;
	}

	public void setDoorStsTrunk(Integer doorStsTrunk) {
		this.doorStsTrunk = doorStsTrunk;
	}

	public Integer getDoorStsRearRight() {
		return doorStsRearRight;
	}

	public void setDoorStsRearRight(Integer doorStsRearRight) {
		this.doorStsRearRight = doorStsRearRight;
	}

	public Integer getDoorStsRearLeft() {
		return doorStsRearLeft;
	}

	public void setDoorStsRearLeft(Integer doorStsRearLeft) {
		this.doorStsRearLeft = doorStsRearLeft;
	}

	public Integer getDoorStsFrontRight() {
		return doorStsFrontRight;
	}

	public void setDoorStsFrontRight(Integer doorStsFrontRight) {
		this.doorStsFrontRight = doorStsFrontRight;
	}

	public Integer getDoorStsFrontLeft() {
		return doorStsFrontLeft;
	}

	public void setDoorStsFrontLeft(Integer doorStsFrontLeft) {
		this.doorStsFrontLeft = doorStsFrontLeft;
	}

	public String getDetailedAddr() {
		return detailedAddr;
	}

	public void setDetailedAddr(String detailedAddr) {
		this.detailedAddr = detailedAddr;
	}

	@Override
	public String toString() {
		return "C3ReportCarInfo{" +
			", id=" + id +
			", vin=" + vin +
			", deviceId=" + deviceId +
			", deviceType=" + deviceType +
			", acquisitionTime=" + acquisitionTime +
			", accStatus=" + accStatus +
			", brkPedalStatus=" + brkPedalStatus +
			", batteryVoltage=" + batteryVoltage +
			", engineSpeed=" + engineSpeed +
			", fuel=" + fuel +
			", latitude=" + latitude +
			", longitude=" + longitude +
			", totalDistance=" + totalDistance +
			", vehBatSoc=" + vehBatSoc +
			", vehicleSpeed=" + vehicleSpeed +
			", gmtCreate=" + gmtCreate +
			", gmtModified=" + gmtModified +
			"}";
	}


}
