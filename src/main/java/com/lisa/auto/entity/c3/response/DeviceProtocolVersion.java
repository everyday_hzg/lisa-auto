package com.lisa.auto.entity.c3.response;

import com.lisa.auto.entity.c3.model.ProtocolVersion;

import java.util.List;

public class DeviceProtocolVersion extends BaseResponse {

    private List<ProtocolVersion> versionList;

    public DeviceProtocolVersion(boolean returnSuccess, String returnErrCode, String returnErrMsg, Object data, List list, List versionList) {
        super(returnSuccess, returnErrCode, returnErrMsg, data, list);
        this.versionList = versionList;
    }

    public List<ProtocolVersion> getVersionList() {
        return versionList;
    }

    public void setVersionList(List<ProtocolVersion> versionList) {
        this.versionList = versionList;
    }
}
