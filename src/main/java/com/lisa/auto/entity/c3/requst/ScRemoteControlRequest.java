package com.lisa.auto.entity.c3.requst;

import demo.request.DemoSignRequest;

public class ScRemoteControlRequest extends DemoSignRequest {

    private String operation;
    private String operationType;
    private String vin;

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public String getOperationType() {
        return operationType;
    }

    public void setOperationType(String operationType) {
        this.operationType = operationType;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }
}
