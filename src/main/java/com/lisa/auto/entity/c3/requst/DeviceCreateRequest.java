package com.lisa.auto.entity.c3.requst;

import com.lisa.auto.entity.c3.model.ProtocolVersion;
import demo.request.DemoSignRequest;

import java.util.List;

public class DeviceCreateRequest extends DemoSignRequest {

    /**
     * 设备ID
     */
    private String deviceNo;
    /**
     * 设备编号
     */
    private String no;
    /**
     * 设备类型
     */
    private String deviceType;
    /**
     * 网关no(非空)
     */
    private String gateWayNo;
    /**
     * 网关服务
     */
    private String gateWayName;
    /**
     * 安装公司
     */
    private String installationCompany;
    /**
     * 安装人员
     */
    private String installationPersonnel;
    /**
     * 厂商（非空）
     */
    private String manufactureNo;
    /**
     * 联系电话
     */
    private String phoneNumber;
    /**
     * simNo
     */
    private String simNo;
    /**
     * 坐标系类型
     */
    private String coordinateType;
    /**
     * 协议版本列表，传参必须同‘获取设备协议版本接口’返回值一致。除TBOX外,必须填协议版本；GB_T_808协议类型必填，GB_JS_T_808，GB_T_1078选填
     */
    private List<ProtocolVersion> versionList;

    public String getDeviceNo() {
        return deviceNo;
    }

    public void setDeviceNo(String deviceNo) {
        this.deviceNo = deviceNo;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getGateWayNo() {
        return gateWayNo;
    }

    public void setGateWayNo(String gateWayNo) {
        this.gateWayNo = gateWayNo;
    }

    public String getGateWayName() {
        return gateWayName;
    }

    public void setGateWayName(String gateWayName) {
        this.gateWayName = gateWayName;
    }

    public String getInstallationCompany() {
        return installationCompany;
    }

    public void setInstallationCompany(String installationCompany) {
        this.installationCompany = installationCompany;
    }

    public String getInstallationPersonnel() {
        return installationPersonnel;
    }

    public void setInstallationPersonnel(String installationPersonnel) {
        this.installationPersonnel = installationPersonnel;
    }

    public String getManufactureNo() {
        return manufactureNo;
    }

    public void setManufactureNo(String manufactureNo) {
        this.manufactureNo = manufactureNo;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getSimNo() {
        return simNo;
    }

    public void setSimNo(String simNo) {
        this.simNo = simNo;
    }

    public String getCoordinateType() {
        return coordinateType;
    }

    public void setCoordinateType(String coordinateType) {
        this.coordinateType = coordinateType;
    }

    public List<ProtocolVersion> getVersionList() {
        return versionList;
    }

    public void setVersionList(List<ProtocolVersion> versionList) {
        this.versionList = versionList;
    }
}
