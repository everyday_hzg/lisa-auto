package com.lisa.auto.entity.c3.model;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;

import java.util.Date;
import java.util.List;


/**
 * <p>
 * 制造商信息
 * </p>
 *
 * @author hxh
 * @since 2019-12-20
 */
@TableName("c3_maker")
public class Maker extends Model<Maker> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 编码
     */
    private String no;
    /**
     * 名称
     */
    private String name;
    /**
     * 创建时间
     */
    @TableField("gmt_create")
    private Date gmtCreate;
    /**
     * 更新时间
     */
    @TableField("gmt_modified")
    private Date gmtModified;
    /**
     * 状态
     */
    @TableField("status")
    private String status;

    /**
     * 其下品牌列表
     * @return
     */
    @TableField(exist = false)
    private List<BrandInfo> brandInfos;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<BrandInfo> getBrandInfos() {
        return brandInfos;
    }

    public void setBrandInfos(List<BrandInfo> brandInfos) {
        this.brandInfos = brandInfos;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "Maker{" +
                ", id=" + id +
                ", no=" + no +
                ", name=" + name +
                ", gmtCreate=" + gmtCreate +
                ", gmtModified=" + gmtModified +
                ", status=" + status +
                "}";
    }
}
