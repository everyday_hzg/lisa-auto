package com.lisa.auto.entity.c3.requst;

import demo.request.DemoSignRequest;

public class DevicePageRequest extends DemoSignRequest {

    private boolean bindingStatus;
    private String deviceNo;
    private String deviceType;
    private int curPage;
    private int pageSize;

    public boolean isBindingStatus() {
        return bindingStatus;
    }

    public void setBindingStatus(boolean bindingStatus) {
        this.bindingStatus = bindingStatus;
    }

    public String getDeviceNo() {
        return deviceNo;
    }

    public void setDeviceNo(String deviceNo) {
        this.deviceNo = deviceNo;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public int getCurPage() {
        return curPage;
    }

    public void setCurPage(int curPage) {
        this.curPage = curPage;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }
}
