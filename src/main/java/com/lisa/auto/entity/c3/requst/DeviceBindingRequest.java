package com.lisa.auto.entity.c3.requst;

import demo.request.DemoSignRequest;

public class DeviceBindingRequest extends DemoSignRequest {

    private DeviceBindingRequestBody body;

    public DeviceBindingRequestBody getBody() {
        return body;
    }

    public void setBody(DeviceBindingRequestBody body) {
        this.body = body;
    }
}
