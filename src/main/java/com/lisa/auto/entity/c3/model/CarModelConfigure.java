package com.lisa.auto.entity.c3.model;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 车型管理
 * </p>
 *
 * @author suohao
 * @since 2019-12-23
 */
@TableName("c3_car_model_configure")
public class CarModelConfigure extends Model<CarModelConfigure> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 编码
     */
    private String no;
    /**
     * 所属车型编码
     */
    @TableField("model_no")
    private String modelNo;
    /**
     * 所属车型名称
     */
    @TableField("model_name")
    private String modelName;
    /**
     * 所属车系编码
     */
    @TableField("series_no")
    private String seriesNo;
    /**
     * 所属车系名称
     */
    @TableField("series_name")
    private String seriesName;
    /**
     * 年代
     */
    private Long year;
    /**
     * 配置名
     */
    private String configuration;
    /**
     * 座位数
     */
    @TableField("seat_count")
    private Long seatCount;
    /**
     * 车门数
     */
    @TableField("door_count")
    private Long doorCount;
    /**
     * 变速箱类型
     */
    @TableField("transmission_type")
    private String transmissionType;
    /**
     * 排量
     */
    private BigDecimal displacement;
    /**
     * 燃油标号
     */
    @TableField("fuel_label")
    private String fuelLabel;
    /**
     * 驱动方式
     */
    @TableField("driving_mode")
    private String drivingMode;
    /**
     * 发动机进气方式
     */
    @TableField("admission_type")
    private String admissionType;
    /**
     * 天窗
     */
    private String sunroof;
    /**
     * 邮箱容量L
     */
    @TableField("fuel_tankage")
    private BigDecimal fuelTankage;
    /**
     * 音箱数
     */
    @TableField("speaker_count")
    private Long speakerCount;
    /**
     * 座椅材质
     */
    @TableField("seat_material")
    private String seatMaterial;
    /**
     * 是否有倒车雷达
     */
    @TableField("parking_sensors")
    private Integer parkingSensors;
    /**
     * 气囊数
     */
    @TableField("air_bag_count")
    private Long airBagCount;
    /**
     * 播放器类型
     */
    @TableField("player_type")
    private String playerType;
    /**
     * 是否有GPS导航
     */
    private Integer gps;
    /**
     * 混合形式
     */
    @TableField("mixed_from")
    private String mixedFrom;
    /**
     * 轴距
     */
    private BigDecimal wheelbase;
    /**
     * 里程
     */
    private BigDecimal mileage;
    /**
     * 马力
     */
    private BigDecimal horsepower;
    /**
     * 创建时间
     */
    @TableField("gmt_create")
    private Date gmtCreate;
    /**
     * 更新时间
     */
    @TableField("gmt_modified")
    private Date gmtModified;

    @TableField("is_delete")
    private Integer isDelete;
    /**
     * 状态
     */
    @TableField("status")
    private String status;

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getModelNo() {
        return modelNo;
    }

    public void setModelNo(String modelNo) {
        this.modelNo = modelNo;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getSeriesNo() {
        return seriesNo;
    }

    public void setSeriesNo(String seriesNo) {
        this.seriesNo = seriesNo;
    }

    public String getSeriesName() {
        return seriesName;
    }

    public void setSeriesName(String seriesName) {
        this.seriesName = seriesName;
    }

    public Long getYear() {
        return year;
    }

    public void setYear(Long year) {
        this.year = year;
    }

    public String getConfiguration() {
        return configuration;
    }

    public void setConfiguration(String configuration) {
        this.configuration = configuration;
    }

    public Long getSeatCount() {
        return seatCount;
    }

    public void setSeatCount(Long seatCount) {
        this.seatCount = seatCount;
    }

    public Long getDoorCount() {
        return doorCount;
    }

    public void setDoorCount(Long doorCount) {
        this.doorCount = doorCount;
    }

    public String getTransmissionType() {
        return transmissionType;
    }

    public void setTransmissionType(String transmissionType) {
        this.transmissionType = transmissionType;
    }

    public BigDecimal getDisplacement() {
        return displacement;
    }

    public void setDisplacement(BigDecimal displacement) {
        this.displacement = displacement;
    }

    public String getFuelLabel() {
        return fuelLabel;
    }

    public void setFuelLabel(String fuelLabel) {
        this.fuelLabel = fuelLabel;
    }

    public String getDrivingMode() {
        return drivingMode;
    }

    public void setDrivingMode(String drivingMode) {
        this.drivingMode = drivingMode;
    }

    public String getAdmissionType() {
        return admissionType;
    }

    public void setAdmissionType(String admissionType) {
        this.admissionType = admissionType;
    }

    public String getSunroof() {
        return sunroof;
    }

    public void setSunroof(String sunroof) {
        this.sunroof = sunroof;
    }

    public BigDecimal getFuelTankage() {
        return fuelTankage;
    }

    public void setFuelTankage(BigDecimal fuelTankage) {
        this.fuelTankage = fuelTankage;
    }

    public Long getSpeakerCount() {
        return speakerCount;
    }

    public void setSpeakerCount(Long speakerCount) {
        this.speakerCount = speakerCount;
    }

    public String getSeatMaterial() {
        return seatMaterial;
    }

    public void setSeatMaterial(String seatMaterial) {
        this.seatMaterial = seatMaterial;
    }

    public Integer getParkingSensors() {
        return parkingSensors;
    }

    public void setParkingSensors(Integer parkingSensors) {
        this.parkingSensors = parkingSensors;
    }

    public Long getAirBagCount() {
        return airBagCount;
    }

    public void setAirBagCount(Long airBagCount) {
        this.airBagCount = airBagCount;
    }

    public String getPlayerType() {
        return playerType;
    }

    public void setPlayerType(String playerType) {
        this.playerType = playerType;
    }

    public Integer getGps() {
        return gps;
    }

    public void setGps(Integer gps) {
        this.gps = gps;
    }

    public String getMixedFrom() {
        return mixedFrom;
    }

    public void setMixedFrom(String mixedFrom) {
        this.mixedFrom = mixedFrom;
    }

    public BigDecimal getWheelbase() {
        return wheelbase;
    }

    public void setWheelbase(BigDecimal wheelbase) {
        this.wheelbase = wheelbase;
    }

    public BigDecimal getMileage() {
        return mileage;
    }

    public void setMileage(BigDecimal mileage) {
        this.mileage = mileage;
    }

    public BigDecimal getHorsepower() {
        return horsepower;
    }

    public void setHorsepower(BigDecimal horsepower) {
        this.horsepower = horsepower;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "CarModel{" +
                "id=" + id +
                ", no='" + no + '\'' +
                ", modelNo='" + modelNo + '\'' +
                ", modelName='" + modelName + '\'' +
                ", seriesNo='" + seriesNo + '\'' +
                ", seriesName='" + seriesName + '\'' +
                ", year=" + year +
                ", configuration='" + configuration + '\'' +
                ", seatCount=" + seatCount +
                ", doorCount=" + doorCount +
                ", transmissionType='" + transmissionType + '\'' +
                ", displacement=" + displacement +
                ", fuelLabel='" + fuelLabel + '\'' +
                ", drivingMode='" + drivingMode + '\'' +
                ", admissionType='" + admissionType + '\'' +
                ", sunroof='" + sunroof + '\'' +
                ", fuelTankage=" + fuelTankage +
                ", speakerCount=" + speakerCount +
                ", seatMaterial='" + seatMaterial + '\'' +
                ", parkingSensors=" + parkingSensors +
                ", airBagCount=" + airBagCount +
                ", playerType='" + playerType + '\'' +
                ", gps=" + gps +
                ", mixedFrom='" + mixedFrom + '\'' +
                ", wheelbase=" + wheelbase +
                ", mileage=" + mileage +
                ", horsepower=" + horsepower +
                ", gmtCreate=" + gmtCreate +
                ", gmtModified=" + gmtModified +
                ", isDelete=" + isDelete +
                ", status='" + status + '\'' +
                '}';
    }
}
