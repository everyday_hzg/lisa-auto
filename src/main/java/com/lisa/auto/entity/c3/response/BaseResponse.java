package com.lisa.auto.entity.c3.response;

import java.io.Serializable;
import java.util.List;

public class BaseResponse<T> implements Serializable {


    private static final long serialVersionUID = 8773289679091072911L;
    //是否成功 Example : false
    private boolean returnSuccess;
    //错误code
    private String returnErrCode;
    //错误描述
    private String returnErrMsg;
    //调用Restful服务返回的数据
    private T data;
    //调用Restful服务返回的数据
    private List<T> list;

    public BaseResponse(boolean returnSuccess, String returnErrCode, String returnErrMsg, T data, List<T> list) {
        this.returnSuccess = returnSuccess;
        this.returnErrMsg = returnErrMsg;
        this.returnErrCode = returnErrCode;
        this.data = data;
        this.list = list;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public List<T> getList() {
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }

    public boolean isReturnSuccess() {
        return returnSuccess;
    }

    public void setReturnSuccess(boolean returnSuccess) {
        this.returnSuccess = returnSuccess;
    }

    public String getReturnErrCode() {
        return returnErrCode;
    }

    public void setReturnErrCode(String returnErrCode) {
        this.returnErrCode = returnErrCode;
    }

    public String getReturnErrMsg() {
        return returnErrMsg;
    }

    public void setReturnErrMsg(String returnErrMsg) {
        this.returnErrMsg = returnErrMsg;
    }

}
