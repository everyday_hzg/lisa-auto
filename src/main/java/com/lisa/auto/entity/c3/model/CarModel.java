package com.lisa.auto.entity.c3.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.TableLogic;
import com.baomidou.mybatisplus.enums.IdType;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;

/**
 * <p>
 * 车型管理
 * </p>
 *
 * @author suohao
 * @since 2019-12-23
 */
@TableName("c3_car_model")
public class CarModel extends Model<CarModel> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 编码
     */
    private String no;
    /**
     * 名称
     */
    private String name;
    /**
     * 所属车系编码
     */
    @TableField("series_no")
    private String seriesNo;
    /**
     * 所属车系名称
     */
    @TableField("series_name")
    private String seriesName;
    /**
     * 动力类型
     */
    @TableField("power_type")
    private String powerType;
    /**
     * 车辆长
     */
    @TableField("vehicle_length")
    private BigDecimal vehicleLength;
    /**
     * 车辆宽
     */
    @TableField("vehicle_width")
    private BigDecimal vehicleWidth;
    /**
     * 车辆高
     */
    @TableField("vehicle_height")
    private BigDecimal vehicleHeight;
    /**
     * 车辆自重
     */
    @TableField("vehicle_weight")
    private BigDecimal vehicleWeight;
    /**
     * 车辆载重
     */
    @TableField("vehicle_load")
    private BigDecimal vehicleLoad;
    /**
     * 车厢长
     */
    @TableField("carriage_length")
    private BigDecimal carriageLength;
    /**
     * 车厢宽
     */
    @TableField("carriage_width")
    private BigDecimal carriageWidth;
    /**
     * 车厢高
     */
    @TableField("carriage_height")
    private BigDecimal carriageHeight;
    /**
     * 车厢体积
     */
    @TableField("carriage_volume")
    private BigDecimal carriageVolume;
    /**
     * 车型照片
     */
    @TableField("image_url")
    private BigDecimal imageUrl;
    /**
     * 创建时间
     */
    @TableField("gmt_create")
    private Date gmtCreate;
    /**
     * 更新时间
     */
    @TableField("gmt_modified")
    private Date gmtModified;

    @TableField("is_delete")
    private Integer isDelete;
    /**
     * 状态
     */
    @TableField("status")
    private String status;

    @TableField(exist = false)
    private List<CarModelConfigure> configures;

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSeriesNo() {
        return seriesNo;
    }

    public void setSeriesNo(String seriesNo) {
        this.seriesNo = seriesNo;
    }

    public String getSeriesName() {
        return seriesName;
    }

    public void setSeriesName(String seriesName) {
        this.seriesName = seriesName;
    }

    public String getPowerType() {
        return powerType;
    }

    public void setPowerType(String powerType) {
        this.powerType = powerType;
    }

    public BigDecimal getVehicleLength() {
        return vehicleLength;
    }

    public void setVehicleLength(BigDecimal vehicleLength) {
        this.vehicleLength = vehicleLength;
    }

    public BigDecimal getVehicleWidth() {
        return vehicleWidth;
    }

    public void setVehicleWidth(BigDecimal vehicleWidth) {
        this.vehicleWidth = vehicleWidth;
    }

    public BigDecimal getVehicleHeight() {
        return vehicleHeight;
    }

    public void setVehicleHeight(BigDecimal vehicleHeight) {
        this.vehicleHeight = vehicleHeight;
    }

    public BigDecimal getVehicleWeight() {
        return vehicleWeight;
    }

    public void setVehicleWeight(BigDecimal vehicleWeight) {
        this.vehicleWeight = vehicleWeight;
    }

    public BigDecimal getVehicleLoad() {
        return vehicleLoad;
    }

    public void setVehicleLoad(BigDecimal vehicleLoad) {
        this.vehicleLoad = vehicleLoad;
    }

    public BigDecimal getCarriageLength() {
        return carriageLength;
    }

    public void setCarriageLength(BigDecimal carriageLength) {
        this.carriageLength = carriageLength;
    }

    public BigDecimal getCarriageWidth() {
        return carriageWidth;
    }

    public void setCarriageWidth(BigDecimal carriageWidth) {
        this.carriageWidth = carriageWidth;
    }

    public BigDecimal getCarriageHeight() {
        return carriageHeight;
    }

    public void setCarriageHeight(BigDecimal carriageHeight) {
        this.carriageHeight = carriageHeight;
    }

    public BigDecimal getCarriageVolume() {
        return carriageVolume;
    }

    public void setCarriageVolume(BigDecimal carriageVolume) {
        this.carriageVolume = carriageVolume;
    }

    public BigDecimal getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(BigDecimal imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<CarModelConfigure> getConfigures() {
        return configures;
    }

    public void setConfigures(List<CarModelConfigure> configures) {
        this.configures = configures;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "CarModel{" +
                "id=" + id +
                ", no='" + no + '\'' +
                ", name='" + name + '\'' +
                ", seriesNo='" + seriesNo + '\'' +
                ", seriesName='" + seriesName + '\'' +
                ", powerType='" + powerType + '\'' +
                ", vehicleLength=" + vehicleLength +
                ", vehicleWidth=" + vehicleWidth +
                ", vehicleHeight=" + vehicleHeight +
                ", vehicleWeight=" + vehicleWeight +
                ", vehicleLoad=" + vehicleLoad +
                ", carriageLength=" + carriageLength +
                ", carriageWidth=" + carriageWidth +
                ", carriageHeight=" + carriageHeight +
                ", carriageVolume=" + carriageVolume +
                ", imageUrl=" + imageUrl +
                ", gmtCreate=" + gmtCreate +
                ", gmtModified=" + gmtModified +
                ", isDelete=" + isDelete +
                ", status='" + status + '\'' +
                '}';
    }
}
