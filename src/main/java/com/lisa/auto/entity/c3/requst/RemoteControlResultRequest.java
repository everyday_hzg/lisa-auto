package com.lisa.auto.entity.c3.requst;

import demo.request.DemoSignRequest;

public class RemoteControlResultRequest extends DemoSignRequest {

    private String operationId;

    public String getOperationId() {
        return operationId;
    }

    public void setOperationId(String operationId) {
        this.operationId = operationId;
    }
}
