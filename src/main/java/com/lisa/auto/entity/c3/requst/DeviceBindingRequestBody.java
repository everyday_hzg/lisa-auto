package com.lisa.auto.entity.c3.requst;

import demo.request.DemoSignRequest;

import java.util.List;

public class DeviceBindingRequestBody extends DemoSignRequest {

    private List<String> noLis;
    private String vin;

    public List<String> getNoLis() {
        return noLis;
    }

    public void setNoLis(List<String> noLis) {
        this.noLis = noLis;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }
}
