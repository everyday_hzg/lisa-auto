package com.lisa.auto.entity.c3.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;

import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;

/**
 * <p>
 * 设备信息表
 * </p>
 *
 * @author hxh
 * @since 2019-12-24
 */
@TableName("c3_device")
public class Device extends Model<Device> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 设备ID
     */
    @TableField("device_no")
    private String deviceNo;
    /**
     * 设备编号
     */
    private String no;
    /**
     * 设备类型
     */
    @TableField("device_type")
    private String deviceType;
    /**
     * 网关no
     */
    @TableField("gate_way_no")
    private String gateWayNo;
    /**
     * 网关服务
     */
    @TableField("gate_way_name")
    private String gateWayName;
    /**
     * 安装公司
     */
    @TableField("installation_company")
    private String installationCompany;
    /**
     * 安装人员
     */
    @TableField("installation_personnel")
    private String installationPersonnel;
    /**
     * 厂商
     */
    @TableField("manufacture_no")
    private String manufactureNo;
    /**
     * 联系电话
     */
    @TableField("phone_number")
    private String phoneNumber;
    /**
     * simNo
     */
    @TableField("sim_no")
    private String simNo;
    /**
     * 坐标系类型
     */
    @TableField("coordinate_type")
    private String coordinateType;
    /**
     * 是否绑定
     */
    @TableField("binding_status")
    private Integer bindingStatus;
    /**
     * 绑定时间
     */
    @TableField("binding_time")
    private Date bindingTime;
    /**
     * 绑定车架号
     */
    @TableField("vin")
    private String vin;
    /**
     * 创建时间
     */
    @TableField("gmt_create")
    private Date gmtCreate;
    /**
     * 更新时间
     */
    @TableField("gmt_modified")
    private Date gmtModified;
    /**
     * 状态
     */
    @TableField("status")
    private String status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDeviceNo() {
        return deviceNo;
    }

    public void setDeviceNo(String deviceNo) {
        this.deviceNo = deviceNo;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getGateWayNo() {
        return gateWayNo;
    }

    public void setGateWayNo(String gateWayNo) {
        this.gateWayNo = gateWayNo;
    }

    public String getGateWayName() {
        return gateWayName;
    }

    public void setGateWayName(String gateWayName) {
        this.gateWayName = gateWayName;
    }

    public String getInstallationCompany() {
        return installationCompany;
    }

    public void setInstallationCompany(String installationCompany) {
        this.installationCompany = installationCompany;
    }

    public String getInstallationPersonnel() {
        return installationPersonnel;
    }

    public void setInstallationPersonnel(String installationPersonnel) {
        this.installationPersonnel = installationPersonnel;
    }

    public String getManufactureNo() {
        return manufactureNo;
    }

    public void setManufactureNo(String manufactureNo) {
        this.manufactureNo = manufactureNo;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getSimNo() {
        return simNo;
    }

    public void setSimNo(String simNo) {
        this.simNo = simNo;
    }

    public String getCoordinateType() {
        return coordinateType;
    }

    public void setCoordinateType(String coordinateType) {
        this.coordinateType = coordinateType;
    }

    public Integer getBindingStatus() {
        return bindingStatus;
    }

    public void setBindingStatus(Integer bindingStatus) {
        this.bindingStatus = bindingStatus;
    }

    public Date getBindingTime() {
        return bindingTime;
    }

    public void setBindingTime(Date bindingTime) {
        this.bindingTime = bindingTime;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "Device{" +
                ", id=" + id +
                ", deviceNo=" + deviceNo +
                ", no=" + no +
                ", deviceType=" + deviceType +
                ", gateWayNo=" + gateWayNo +
                ", gateWayName=" + gateWayName +
                ", installationCompany=" + installationCompany +
                ", installationPersonnel=" + installationPersonnel +
                ", manufactureNo=" + manufactureNo +
                ", phoneNumber=" + phoneNumber +
                ", simNo=" + simNo +
                ", coordinateType=" + coordinateType +
                ", bindingStatus=" + bindingStatus +
                ", bindingTime=" + bindingTime +
                ", vin=" + vin +
                ", gmtCreate=" + gmtCreate +
                ", gmtModified=" + gmtModified +
                ", status=" + status +
                "}";
    }
}
