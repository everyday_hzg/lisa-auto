package com.lisa.auto.entity.statistics;

import java.util.Date;

/**
 * @author huangzhigang 2020/2/26
 */

public class VehiclesPo {

    /** 风险故障记录ID */
    private String rfId;
    /** 车架号 */
    private String vin;
    /** 车队公司 */
    private String fleet;
    /** 车牌号 */
    private String plateLicenseNo;
    /** tbox 编号 */
    private String tboxNo;
    /** 上报时间 */
    private String reportTime;
    /** 经纬度 */
    private String lngLat;
    /** 车辆状态[1:运行、2:风险、3:故障] */
    private Integer state;
    /** 持续周期[单位：小时] */
    private String cycle;
    /** 持续时间 */
    private String continuedTime;
    /** 风险/故障类型 */
    private String riskType;
    /** 风险/故障明细 */
    private String riskItem;
    /** 剩余电量 */
    private String vehBatSoc;
    /** 里程 */
    private String totalDistance;
    /** 速度 */
    private String vehicleSpeed;
    /** 备注 */
    private String remark;
    /** 开始时间 */
    private Date startTime;
    /** 结束时间 */
    private Date endTime;

    public VehiclesPo() {
        super();
    }

    public VehiclesPo(String rfId, String vin, String fleet, String plateLicenseNo, String tboxNo, String reportTime, String lngLat, Integer state, String cycle,
                      String continuedTime, String riskType, String riskItem, String vehBatSoc, String totalDistance, String vehicleSpeed, String remark) {
        this.rfId = rfId;
        this.vin = vin;
        this.fleet = fleet;
        this.plateLicenseNo = plateLicenseNo;
        this.tboxNo = tboxNo;
        this.reportTime = reportTime;
        this.lngLat = lngLat;
        this.state = state;
        this.cycle = cycle;
        this.continuedTime = continuedTime;
        this.riskType = riskType;
        this.riskItem = riskItem;
        this.vehBatSoc = vehBatSoc;
        this.totalDistance = totalDistance;
        this.vehicleSpeed = vehicleSpeed;
        this.remark = remark;
    }

    public String getRfId() {
        return rfId;
    }

    public void setRfId(String rfId) {
        this.rfId = rfId;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getFleet() {
        return fleet;
    }

    public void setFleet(String fleet) {
        this.fleet = fleet;
    }

    public String getPlateLicenseNo() {
        return plateLicenseNo;
    }

    public void setPlateLicenseNo(String plateLicenseNo) {
        this.plateLicenseNo = plateLicenseNo;
    }

    public String getTboxNo() {
        return tboxNo;
    }

    public void setTboxNo(String tboxNo) {
        this.tboxNo = tboxNo;
    }

    public String getReportTime() {
        return reportTime;
    }

    public void setReportTime(String reportTime) {
        this.reportTime = reportTime;
    }

    public String getLngLat() {
        return lngLat;
    }

    public void setLngLat(String lngLat) {
        this.lngLat = lngLat;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getCycle() {
        return cycle;
    }

    public void setCycle(String cycle) {
        this.cycle = cycle;
    }

    public String getContinuedTime() {
        return continuedTime;
    }

    public void setContinuedTime(String continuedTime) {
        this.continuedTime = continuedTime;
    }

    public String getRiskType() {
        return riskType;
    }

    public void setRiskType(String riskType) {
        this.riskType = riskType;
    }

    public String getRiskItem() {
        return riskItem;
    }

    public void setRiskItem(String riskItem) {
        this.riskItem = riskItem;
    }

    public String getVehBatSoc() {
        return vehBatSoc;
    }

    public void setVehBatSoc(String vehBatSoc) {
        this.vehBatSoc = vehBatSoc;
    }

    public String getTotalDistance() {
        return totalDistance;
    }

    public void setTotalDistance(String totalDistance) {
        this.totalDistance = totalDistance;
    }

    public String getVehicleSpeed() {
        return vehicleSpeed;
    }

    public void setVehicleSpeed(String vehicleSpeed) {
        this.vehicleSpeed = vehicleSpeed;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }
}
