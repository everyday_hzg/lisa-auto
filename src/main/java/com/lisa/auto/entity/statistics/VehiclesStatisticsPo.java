package com.lisa.auto.entity.statistics;

/**
 * @author huangzhigang 2020/2/25
 */
public class VehiclesStatisticsPo {

    /** 车队ID */
    private Long fleetId;
    /** 车队公司名称 */
    private String fleetName;
    /** 车辆总里程 */
    private Double totalDistance;
    /** 故障次数 */
    private Integer fault;
    /** 故障车辆数 */
    private Integer faultCnt;
    /** 风险次数 */
    private Integer risk;
    /** 风险车辆数 */
    private Integer riskCnt;
    /** 驾驶行为次数 */
    private Integer drivingBehavior;
    /** 驾驶行为车辆数 */
    private Integer drivingBehaviorCnt;
    /** 疲劳驾驶次数 */
    private Integer fatigueDriving;
    /** 疲劳驾驶车辆数 */
    private Integer fatigueDrivingCnt;
    /** 疲劳驾驶车辆数 */
    private Integer unusualType;
    /** 车辆数 */
    private Integer vehiclesCnt;
    /** 持续时长，单位：分钟 */
    private Integer times;

    public VehiclesStatisticsPo() {
        super();
    }

    public VehiclesStatisticsPo(Long fleetId, String fleetName, Double totalDistance, Integer fault, Integer risk,
                                Integer drivingBehavior, Integer fatigueDriving) {
        this.fleetId = fleetId;
        this.fleetName = fleetName;
        this.totalDistance = totalDistance;
        this.fault = fault;
        this.risk = risk;
        this.drivingBehavior = drivingBehavior;
        this.fatigueDriving = fatigueDriving;
    }

    public Long getFleetId() {
        return fleetId;
    }

    public void setFleetId(Long fleetId) {
        this.fleetId = fleetId;
    }

    public String getFleetName() {
        return fleetName;
    }

    public void setFleetName(String fleetName) {
        this.fleetName = fleetName;
    }

    public Double getTotalDistance() {
        return totalDistance;
    }

    public void setTotalDistance(Double totalDistance) {
        this.totalDistance = totalDistance;
    }

    public Integer getFault() {
        return fault;
    }

    public void setFault(Integer fault) {
        this.fault = fault;
    }

    public Integer getFaultCnt() {
        return faultCnt;
    }

    public void setFaultCnt(Integer faultCnt) {
        this.faultCnt = faultCnt;
    }

    public Integer getRisk() {
        return risk;
    }

    public void setRisk(Integer risk) {
        this.risk = risk;
    }

    public Integer getRiskCnt() {
        return riskCnt;
    }

    public void setRiskCnt(Integer riskCnt) {
        this.riskCnt = riskCnt;
    }

    public Integer getDrivingBehavior() {
        return drivingBehavior;
    }

    public void setDrivingBehavior(Integer drivingBehavior) {
        this.drivingBehavior = drivingBehavior;
    }

    public Integer getDrivingBehaviorCnt() {
        return drivingBehaviorCnt;
    }

    public void setDrivingBehaviorCnt(Integer drivingBehaviorCnt) {
        this.drivingBehaviorCnt = drivingBehaviorCnt;
    }

    public Integer getFatigueDriving() {
        return fatigueDriving;
    }

    public void setFatigueDriving(Integer fatigueDriving) {
        this.fatigueDriving = fatigueDriving;
    }

    public Integer getFatigueDrivingCnt() {
        return fatigueDrivingCnt;
    }

    public void setFatigueDrivingCnt(Integer fatigueDrivingCnt) {
        this.fatigueDrivingCnt = fatigueDrivingCnt;
    }

    public Integer getVehiclesCnt() {
        return vehiclesCnt;
    }

    public void setVehiclesCnt(Integer vehiclesCnt) {
        this.vehiclesCnt = vehiclesCnt;
    }

    public Integer getUnusualType() {
        return unusualType;
    }

    public void setUnusualType(Integer unusualType) {
        this.unusualType = unusualType;
    }

    public Integer getTimes() {
        return times;
    }

    public void setTimes(Integer times) {
        this.times = times;
    }
}
