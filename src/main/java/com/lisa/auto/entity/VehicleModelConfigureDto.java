package com.lisa.auto.entity;

import com.lisa.auto.entity.c3.model.CarModelConfigure;

public class VehicleModelConfigureDto extends CarModelConfigure {

    private Long vehivleId;       //车辆id
    private String vin;       //车架号
    private String plateLicenseNo;       //车牌号
    private String vehicleStatus;       //车辆状态
    private String description;       //车辆备注
    private String makerNo;       //制造商编码
    private String makerName;       //制造商名称
    private String brandNo;       //品牌编码
    private String brandName;       //品牌名称
    private Long areaId;       //品牌名称
    private String province;       //品牌名称
    private String city;       //品牌名称
    private String county;       //品牌名称
    private String fleet;       //车队
    private String createTime;       //车队

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public Long getVehivleId() {
        return vehivleId;
    }

    public void setVehivleId(Long vehivleId) {
        this.vehivleId = vehivleId;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getPlateLicenseNo() {
        return plateLicenseNo;
    }

    public void setPlateLicenseNo(String plateLicenseNo) {
        this.plateLicenseNo = plateLicenseNo;
    }

    public String getVehicleStatus() {
        return vehicleStatus;
    }

    public void setVehicleStatus(String vehicleStatus) {
        this.vehicleStatus = vehicleStatus;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMakerNo() {
        return makerNo;
    }

    public void setMakerNo(String makerNo) {
        this.makerNo = makerNo;
    }

    public String getMakerName() {
        return makerName;
    }

    public void setMakerName(String makerName) {
        this.makerName = makerName;
    }

    public String getBrandNo() {
        return brandNo;
    }

    public void setBrandNo(String brandNo) {
        this.brandNo = brandNo;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public Long getAreaId() {
        return areaId;
    }

    public void setAreaId(Long areaId) {
        this.areaId = areaId;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getFleet() {
        return fleet;
    }

    public void setFleet(String fleet) {
        this.fleet = fleet;
    }
}
