package com.lisa.auto.entity;

public class AreaCountDto {

    private String province;    //区域划分：province 省；city  市

    private String city;
    private String county;
    private String areaType;  //父级区域名称：若areaType为province，则可以不传，默认查全部；若areaType为city，则需要传对应省份名称；

    private String areaName;    //区域名称：若areaType为province，则返回省名称；若areaType为city，则返回市名称；

    private String fleetName;

    private String plateLicenseNo;
    private String type;

    private Integer totalSum;       // 总数
    private Integer normalSum;   // 正常数
    private Integer riskSum;   // 风险数
    private Integer faultSum;   // 故障

    public AreaCountDto() {
        super();
    }

    public AreaCountDto(Integer totalSum, Integer normalSum, Integer riskSum, Integer faultSum) {
        this.totalSum = totalSum;
        this.normalSum = normalSum;
        this.riskSum = riskSum;
        this.faultSum = faultSum;
    }

    public AreaCountDto(String areaType, String areaName, Integer totalSum, Integer normalSum, Integer riskSum, Integer faultSum) {
        this.areaType = areaType;
        this.areaName = areaName;
        this.totalSum = totalSum;
        this.normalSum = normalSum;
        this.riskSum = riskSum;
        this.faultSum = faultSum;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAreaType() {
        return areaType;
    }

    public void setAreaType(String areaType) {
        this.areaType = areaType;
    }

    public Integer getFaultSum() {
        return faultSum;
    }

    public void setFaultSum(Integer faultSum) {
        this.faultSum = faultSum;
    }

    public Integer getRiskSum() {
        return riskSum;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public void setRiskSum(Integer riskSum) {
        this.riskSum = riskSum;
    }

    public String getFleetName() {
        return fleetName;
    }

    public void setFleetName(String fleetName) {
        this.fleetName = fleetName;
    }

    public String getPlateLicenseNo() {
        return plateLicenseNo;
    }

    public void setPlateLicenseNo(String plateLicenseNo) {
        this.plateLicenseNo = plateLicenseNo;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public Integer getTotalSum() {
        return totalSum;
    }

    public void setTotalSum(Integer totalSum) {
        this.totalSum = totalSum;
    }

    public Integer getNormalSum() {
        return normalSum;
    }

    public void setNormalSum(Integer normalSum) {
        this.normalSum = normalSum;
    }


}
