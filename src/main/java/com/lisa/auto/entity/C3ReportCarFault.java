package com.lisa.auto.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;
import java.util.Date;

/**
 * @description:
 * @author: huangzhigang
 * @date: 2020-03-13 10:55
 **/
@TableName("c3_report_car_fault")
public class C3ReportCarFault extends Model<C3ReportCarFault> {

    private static final long serialVersionUID = 1L;

    @TableId(value="id", type= IdType.AUTO)
    private Long id;
    /**
     * 车架号
     */
    private String vin;
    /**
     * 电池箱不匹配故障,0：正常，1：一级故障，2：二级故障，3：三级故障，-1：无效
     */
    @TableField("battery_box_mismatch_fault")
    private Integer batteryBoxMismatchFault;
    /**
     * 动力电池过充,0：正常，1：一级故障，2：二级故障，3：三级故障
     */
    @TableField("battery_over_charge")
    private Integer batteryOverCharge;
    /**
     * 电池性能,0：正常，1：一级故障，2：二级故障，3：三级故障，-1：无效
     */
    @TableField("battery_per_formance")
    private Integer batteryPerFormance;
    /**
     * 动力电池温差过大报警,0：正常，1：一级故障，2：二级故障，3：三级故障，-1：无效
     */
    @TableField("battery_temp_diff_large_alarm")
    private Integer batteryTempDiffLargeAlarm;
    /**
     * 动力电池一致性差报警,0：正常，1：一级故障，2：二级故障，3：三级故障，-1：无效*
     */
    @TableField("battery_uniformity_bad_alarm")
    private Integer batteryUniformityBadAlarm;
    /**
     * 电池电压-电池监控,-1：无效
     */
    @TableField("battery_voltage_of_battery_monitor")
    private String batteryVoltageOfBatteryMonitor;
    /**
     * 电池包总电压过高报警,0：正常，1：一级故障，2：二级故障，3：三级故障
     */
    @TableField("batterys_voltage_higher_alarm")
    private Integer batterysVoltageHigherAlarm;
    /**
     * 电池包总电压过低报警,0：正常，1：一级故障，2：二级故障，3：三级故障
     */
    @TableField("batterys_voltage_lower_alarm")
    private Integer batterysVoltageLowerAlarm;
    /**
     * 制动系统故障,0：正常，1：一级故障，2：二级故障，3：三级故障
     */
    @TableField("braking_sys_fault")
    private Integer brakingSysFault;
    /**
     * CAN通讯故障（丢失VCU节点）,0：正常，1：一级故障，2：二级故障，3：三级故障，-1：无效
     */
    @TableField("can_communication_fault")
    private Integer canCommunicationFault;
    /**
     * CAN通讯故障（丢失VCU节点）-可充电储能装置故障,0：正常，1：一级故障，2：二级故障，3：三级故障，-1：无效
     */
    @TableField("can_communication_fault_0f_storage")
    private Integer canCommunicationFault0fStorage;
    /**
     * 充电状态故障报警,0：正常，1：一级故障，2：二级故障，3：三级故障，-1：无效
     */
    @TableField("charge_status_fault")
    private Integer chargeStatusFault;
    /**
     * 充电枪连接故障,0：正常，1：一级故障，2：二级故障，3：三级故障，-1：无效
     */
    @TableField("charging_gun_connect_fault")
    private Integer chargingGunConnectFault;
    /**
     * 转向指示灯状态,0：关，1：左转向，2：右转向，3：双闪
     */
    @TableField("cornering_lamp")
    private Integer corneringLamp;
    /**
     * DCDC状态报警,0：正常，1：一级故障，2：二级故障，3：三级故障
     */
    @TableField("dcStatus_alarm")
    private Integer dcStatusAlarm;
    /**
     * 放电电流过大报警,0：正常，1：一级故障，2：二级故障，3：三级故障
     */
    @TableField("discharge_current_bigger_alarm")
    private Integer dischargeCurrentBiggerAlarm;
    /**
     * 电机控制器过温,0：正常，1：一级故障，2：二级故障，3：三级故障，-1：无效
     */
    @TableField("electric_contro_over_temp")
    private Integer electricControOverTemp;
    /**
     * 电机超速,0：正常，1：一级故障，2：二级故障，3：三级故障
     */
    @TableField("electric_over_speed")
    private Integer electricOverSpeed;
    /**
     * 电机过温,0：正常，1：一级故障，2：二级故障，3：三级故障，-1：无效
     */
    @TableField("electric_over_temp")
    private Integer electricOverTemp;
    /**
     * 仪表前雾灯状态,0：否，1：是
     */
    @TableField("front_fog_lamp")
    private Integer frontFogLamp;
    /**
     * 输入直流过流,0：正常，1：一级故障，2：二级故障，3：三级故障
     */
    @TableField("input_dc_over_current")
    private Integer inputDcOverCurrent;
    /**
     * 绝缘故障,0：正常，1：一级故障，2：二级故障，3：三级故障
     */
    @TableField("insulation_fault")
    private Integer insulationFault;
    /**
     * 单体温度过高报警,0：正常，1：一级故障，2：二级故障，3：三级故障
     */
    @TableField("monomer_temp_higher_alarm")
    private Integer monomerTempHigherAlarm;
    /**
     * 单体电压过高报警,0：正常，1：一级故障，2：二级故障，3：三级故障
     */
    @TableField("monomer_voltage_higher_alarm")
    private Integer monomerVoltageHigherAlarm;
    /**
     * 单体电压过低报警,0：正常，1：一级故障，2：二级故障，3：三级故障
     */
    @TableField("monomer_voltage_lower_alarm")
    private Integer monomerVoltageLowerAlarm;
    /**
     * OBC通讯故障(丢失OBC节点),0：正常，1：一级故障，2：二级故障，3：三级故障
     */
    @TableField("obc_communication_fault")
    private Integer obcCommunicationFault;
    /**
     * 其他故障总数
     */
    @TableField("other_fault_count")
    private Integer otherFaultCount;
    /**
     * 过温故障,0：正常，1：一级故障，2：二级故障，3：三级故障，-1：无效
     */
    @TableField("over_temp_fault")
    private Integer overTempFault;
    /**
     * 动力系统就绪,0：未就绪，1：就绪
     */
    @TableField("power_system_ready")
    private Integer powerSystemReady;
    /**
     * 仪表后雾灯状态,0：关，1：开，-1：invalid，-2：error
     */
    @TableField("rear_fog_lamp")
    private Integer rearFogLamp;
    /**
     * 运行模式,1：纯电，2：混动，3：燃油，-1：异常，-2：无效
     */
    @TableField("run_mode")
    private Integer runMode;
    /**
     * SOC过高报警,0：正常，1：一级故障，2：二级故障，3：三级故障，-1：无效
     */
    @TableField("soc_higher_alarm")
    private Integer socHigherAlarm;
    /**
     * SOC跳变故障,0：正常，1：一级故障，2：二级故障，3：三级故障，-1：无效
     */
    @TableField("soc_jump_fault")
    private Integer socJumpFault;
    /**
     * SOC过低报警,0：正常，1：一级故障，2：二级故障，3：三级故障，-1：无效
     */
    @TableField("soc_lower_alarm")
    private Integer socLowerAlarm;
    /**
     * 整车充电故障,0：正常，1：一级故障，2：二级故障，3：三级故障，-1：无效
     */
    @TableField("vehicle_charge_fault")
    private Integer vehicleChargeFault;
    /**
     * 整车故障,0：正常，1：一级故障，2：二级故障，3：三级故障
     */
    @TableField("vehicle_fault")
    private Integer vehicleFault;
    /**
     * 整车高压互锁故障,0：正常，1：一级故障，2：二级故障，3：三级故障，-1：无效
     */
    @TableField("vehicle_high_voltage_inter_lock_fault")
    private Integer vehicleHighVoltageInterLockFault;
    /**
     * 电机数据JSON
     */
    @TableField("electric_machinery_data")
    private String electricMachineryData;
    /**
     * 电池温度JSON
     */
    @TableField("battery_temp")
    private String batteryTemp;
    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public Integer getBatteryBoxMismatchFault() {
        return batteryBoxMismatchFault;
    }

    public void setBatteryBoxMismatchFault(Integer batteryBoxMismatchFault) {
        this.batteryBoxMismatchFault = batteryBoxMismatchFault;
    }

    public Integer getBatteryOverCharge() {
        return batteryOverCharge;
    }

    public void setBatteryOverCharge(Integer batteryOverCharge) {
        this.batteryOverCharge = batteryOverCharge;
    }

    public Integer getBatteryPerFormance() {
        return batteryPerFormance;
    }

    public void setBatteryPerFormance(Integer batteryPerFormance) {
        this.batteryPerFormance = batteryPerFormance;
    }

    public Integer getBatteryTempDiffLargeAlarm() {
        return batteryTempDiffLargeAlarm;
    }

    public void setBatteryTempDiffLargeAlarm(Integer batteryTempDiffLargeAlarm) {
        this.batteryTempDiffLargeAlarm = batteryTempDiffLargeAlarm;
    }

    public Integer getBatteryUniformityBadAlarm() {
        return batteryUniformityBadAlarm;
    }

    public void setBatteryUniformityBadAlarm(Integer batteryUniformityBadAlarm) {
        this.batteryUniformityBadAlarm = batteryUniformityBadAlarm;
    }

    public String getBatteryVoltageOfBatteryMonitor() {
        return batteryVoltageOfBatteryMonitor;
    }

    public void setBatteryVoltageOfBatteryMonitor(String batteryVoltageOfBatteryMonitor) {
        this.batteryVoltageOfBatteryMonitor = batteryVoltageOfBatteryMonitor;
    }

    public Integer getBatterysVoltageHigherAlarm() {
        return batterysVoltageHigherAlarm;
    }

    public void setBatterysVoltageHigherAlarm(Integer batterysVoltageHigherAlarm) {
        this.batterysVoltageHigherAlarm = batterysVoltageHigherAlarm;
    }

    public Integer getBatterysVoltageLowerAlarm() {
        return batterysVoltageLowerAlarm;
    }

    public void setBatterysVoltageLowerAlarm(Integer batterysVoltageLowerAlarm) {
        this.batterysVoltageLowerAlarm = batterysVoltageLowerAlarm;
    }

    public Integer getBrakingSysFault() {
        return brakingSysFault;
    }

    public void setBrakingSysFault(Integer brakingSysFault) {
        this.brakingSysFault = brakingSysFault;
    }

    public Integer getCanCommunicationFault() {
        return canCommunicationFault;
    }

    public void setCanCommunicationFault(Integer canCommunicationFault) {
        this.canCommunicationFault = canCommunicationFault;
    }

    public Integer getCanCommunicationFault0fStorage() {
        return canCommunicationFault0fStorage;
    }

    public void setCanCommunicationFault0fStorage(Integer canCommunicationFault0fStorage) {
        this.canCommunicationFault0fStorage = canCommunicationFault0fStorage;
    }

    public Integer getChargeStatusFault() {
        return chargeStatusFault;
    }

    public void setChargeStatusFault(Integer chargeStatusFault) {
        this.chargeStatusFault = chargeStatusFault;
    }

    public Integer getChargingGunConnectFault() {
        return chargingGunConnectFault;
    }

    public void setChargingGunConnectFault(Integer chargingGunConnectFault) {
        this.chargingGunConnectFault = chargingGunConnectFault;
    }

    public Integer getCorneringLamp() {
        return corneringLamp;
    }

    public void setCorneringLamp(Integer corneringLamp) {
        this.corneringLamp = corneringLamp;
    }

    public Integer getDcStatusAlarm() {
        return dcStatusAlarm;
    }

    public void setDcStatusAlarm(Integer dcStatusAlarm) {
        this.dcStatusAlarm = dcStatusAlarm;
    }

    public Integer getDischargeCurrentBiggerAlarm() {
        return dischargeCurrentBiggerAlarm;
    }

    public void setDischargeCurrentBiggerAlarm(Integer dischargeCurrentBiggerAlarm) {
        this.dischargeCurrentBiggerAlarm = dischargeCurrentBiggerAlarm;
    }

    public Integer getElectricControOverTemp() {
        return electricControOverTemp;
    }

    public void setElectricControOverTemp(Integer electricControOverTemp) {
        this.electricControOverTemp = electricControOverTemp;
    }

    public Integer getElectricOverSpeed() {
        return electricOverSpeed;
    }

    public void setElectricOverSpeed(Integer electricOverSpeed) {
        this.electricOverSpeed = electricOverSpeed;
    }

    public Integer getElectricOverTemp() {
        return electricOverTemp;
    }

    public void setElectricOverTemp(Integer electricOverTemp) {
        this.electricOverTemp = electricOverTemp;
    }

    public Integer getFrontFogLamp() {
        return frontFogLamp;
    }

    public void setFrontFogLamp(Integer frontFogLamp) {
        this.frontFogLamp = frontFogLamp;
    }

    public Integer getInputDcOverCurrent() {
        return inputDcOverCurrent;
    }

    public void setInputDcOverCurrent(Integer inputDcOverCurrent) {
        this.inputDcOverCurrent = inputDcOverCurrent;
    }

    public Integer getInsulationFault() {
        return insulationFault;
    }

    public void setInsulationFault(Integer insulationFault) {
        this.insulationFault = insulationFault;
    }

    public Integer getMonomerTempHigherAlarm() {
        return monomerTempHigherAlarm;
    }

    public void setMonomerTempHigherAlarm(Integer monomerTempHigherAlarm) {
        this.monomerTempHigherAlarm = monomerTempHigherAlarm;
    }

    public Integer getMonomerVoltageHigherAlarm() {
        return monomerVoltageHigherAlarm;
    }

    public void setMonomerVoltageHigherAlarm(Integer monomerVoltageHigherAlarm) {
        this.monomerVoltageHigherAlarm = monomerVoltageHigherAlarm;
    }

    public Integer getMonomerVoltageLowerAlarm() {
        return monomerVoltageLowerAlarm;
    }

    public void setMonomerVoltageLowerAlarm(Integer monomerVoltageLowerAlarm) {
        this.monomerVoltageLowerAlarm = monomerVoltageLowerAlarm;
    }

    public Integer getObcCommunicationFault() {
        return obcCommunicationFault;
    }

    public void setObcCommunicationFault(Integer obcCommunicationFault) {
        this.obcCommunicationFault = obcCommunicationFault;
    }

    public Integer getOtherFaultCount() {
        return otherFaultCount;
    }

    public void setOtherFaultCount(Integer otherFaultCount) {
        this.otherFaultCount = otherFaultCount;
    }

    public Integer getOverTempFault() {
        return overTempFault;
    }

    public void setOverTempFault(Integer overTempFault) {
        this.overTempFault = overTempFault;
    }

    public Integer getPowerSystemReady() {
        return powerSystemReady;
    }

    public void setPowerSystemReady(Integer powerSystemReady) {
        this.powerSystemReady = powerSystemReady;
    }

    public Integer getRearFogLamp() {
        return rearFogLamp;
    }

    public void setRearFogLamp(Integer rearFogLamp) {
        this.rearFogLamp = rearFogLamp;
    }

    public Integer getRunMode() {
        return runMode;
    }

    public void setRunMode(Integer runMode) {
        this.runMode = runMode;
    }

    public Integer getSocHigherAlarm() {
        return socHigherAlarm;
    }

    public void setSocHigherAlarm(Integer socHigherAlarm) {
        this.socHigherAlarm = socHigherAlarm;
    }

    public Integer getSocJumpFault() {
        return socJumpFault;
    }

    public void setSocJumpFault(Integer socJumpFault) {
        this.socJumpFault = socJumpFault;
    }

    public Integer getSocLowerAlarm() {
        return socLowerAlarm;
    }

    public void setSocLowerAlarm(Integer socLowerAlarm) {
        this.socLowerAlarm = socLowerAlarm;
    }

    public Integer getVehicleChargeFault() {
        return vehicleChargeFault;
    }

    public void setVehicleChargeFault(Integer vehicleChargeFault) {
        this.vehicleChargeFault = vehicleChargeFault;
    }

    public Integer getVehicleFault() {
        return vehicleFault;
    }

    public void setVehicleFault(Integer vehicleFault) {
        this.vehicleFault = vehicleFault;
    }

    public Integer getVehicleHighVoltageInterLockFault() {
        return vehicleHighVoltageInterLockFault;
    }

    public void setVehicleHighVoltageInterLockFault(Integer vehicleHighVoltageInterLockFault) {
        this.vehicleHighVoltageInterLockFault = vehicleHighVoltageInterLockFault;
    }

    public String getElectricMachineryData() {
        return electricMachineryData;
    }

    public void setElectricMachineryData(String electricMachineryData) {
        this.electricMachineryData = electricMachineryData;
    }

    public String getBatteryTemp() {
        return batteryTemp;
    }

    public void setBatteryTemp(String batteryTemp) {
        this.batteryTemp = batteryTemp;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "C3ReportCarFault{" +
                ", id=" + id +
                ", vin=" + vin +
                ", batteryBoxMismatchFault=" + batteryBoxMismatchFault +
                ", batteryOverCharge=" + batteryOverCharge +
                ", batteryPerFormance=" + batteryPerFormance +
                ", batteryTempDiffLargeAlarm=" + batteryTempDiffLargeAlarm +
                ", batteryUniformityBadAlarm=" + batteryUniformityBadAlarm +
                ", batteryVoltageOfBatteryMonitor=" + batteryVoltageOfBatteryMonitor +
                ", batterysVoltageHigherAlarm=" + batterysVoltageHigherAlarm +
                ", batterysVoltageLowerAlarm=" + batterysVoltageLowerAlarm +
                ", brakingSysFault=" + brakingSysFault +
                ", canCmmunicationFault=" + canCommunicationFault +
                ", canCommunicationFault0fStorage=" + canCommunicationFault0fStorage +
                ", chargeStatusFault=" + chargeStatusFault +
                ", chargingGunConnectFault=" + chargingGunConnectFault +
                ", corneringLamp=" + corneringLamp +
                ", dcStatusAlarm=" + dcStatusAlarm +
                ", dischargeCurrentBiggerAlarm=" + dischargeCurrentBiggerAlarm +
                ", electricControOverTemp=" + electricControOverTemp +
                ", electricOverSpeed=" + electricOverSpeed +
                ", electricOverTemp=" + electricOverTemp +
                ", frontFogLamp=" + frontFogLamp +
                ", inputDcOverCurrent=" + inputDcOverCurrent +
                ", insulationFault=" + insulationFault +
                ", monomerTempHigherAlarm=" + monomerTempHigherAlarm +
                ", monomerVoltageHigherAlarm=" + monomerVoltageHigherAlarm +
                ", monomerVoltageLowerAlarm=" + monomerVoltageLowerAlarm +
                ", obcCommunicationFault=" + obcCommunicationFault +
                ", otherFaultCount=" + otherFaultCount +
                ", overTempFault=" + overTempFault +
                ", powerSystemReady=" + powerSystemReady +
                ", rearFogLamp=" + rearFogLamp +
                ", runMode=" + runMode +
                ", socHigherAlarm=" + socHigherAlarm +
                ", socJumpFault=" + socJumpFault +
                ", socLowerAlarm=" + socLowerAlarm +
                ", vehicleChargeFault=" + vehicleChargeFault +
                ", vehicleFault=" + vehicleFault +
                ", vehicleHighVoltageInterLockFault=" + vehicleHighVoltageInterLockFault +
                ", electricMachineryData=" + electricMachineryData +
                ", batteryTemp=" + batteryTemp +
                ", createTime=" + createTime +
                "}";
    }
}
