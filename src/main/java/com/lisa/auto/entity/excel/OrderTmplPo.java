package com.lisa.auto.entity.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.excel.annotation.ExcelCollection;

import java.util.List;

/**
 * 城配-运单实体类Po
 * @author huangzhigang 2019/12/21
 */
public class OrderTmplPo {

    @Excel(name = "外部订单号", needMerge = true, width = 10)
    private String extOrderNum;

    @Excel(name = "运费", needMerge = true, width = 8)
    private String freight;

    @Excel(name = "保价费", needMerge = true, width = 8)
    private String insuredFee;

    @Excel(name = "包装费", needMerge = true, width = 8)
    private String packingCharges;

    @Excel(name = "送货费", needMerge = true, width = 8)
    private String deliveryFee;

    @Excel(name = "上楼费", needMerge = true, width = 8)
    private String upstairsFee;

    @Excel(name = "接货费", needMerge = true, width = 8)
    private String receivingCharge;

    @Excel(name = "其他费用", needMerge = true, width = 8)
    private String otherExpenses;

    @Excel(name = "结算类型", needMerge = true, width = 8)
    private String settlementType;

    @Excel(name = "描述", needMerge = true, width = 8)
    private String describe;

    @Excel(name = "商户", needMerge = true, width = 10)
    private String merchant;

    @Excel(name = "商户编号", needMerge = true, width = 10)
    private String merchantCode;

    @Excel(name = "发货地址", needMerge = true, width = 12)
    private String shippingAddr;

    @Excel(name = "发货地址编号", needMerge = true, width = 12)
    private String shippingAddrNo;

    @Excel(name = "发货人", needMerge = true, width = 12)
    private String shipper;

    @Excel(name = "发货人电话", needMerge = true, width = 8)
    private String shipperTelNum;

    @Excel(name = "收货人", needMerge = true, width = 8)
    private String consignee;

    @Excel(name = "收货人电话", needMerge = true, width = 8)
    private String consigneeTelNum;

    @Excel(name = "卸货地址", needMerge = true, width = 10)
    private String unloadingAddr;

    @Excel(name = "卸货地址编号", needMerge = true, width = 12)
    private String unloadingAddrNo;

    @Excel(name = "取货时间起", needMerge = true, width = 12)
    private String startPickUp;

    @Excel(name = "取货时间止", needMerge = true, width = 12)
    private String endPickUp;

    @Excel(name = "期望送达时间起", needMerge = true, width = 15)
    private String extDeliveryStartTime;

    @Excel(name = "期望送达时间止", needMerge = true, width = 15)
    private String extDeliveryEndTime;

    @ExcelCollection(name = "")
    private List<GoodsTmplPo> goodsTmplList;

    public OrderTmplPo() {
        super();
    }

    /** 外部订单号、运费、保价费、包装费、送货费、上楼费、接货费、其他费用、结算类型、描述、期望送达时间起、期望送达时间止 */
    public OrderTmplPo(String extOrderNum, String freight, String insuredFee, String packingCharges, String deliveryFee,
                       String upstairsFee, String receivingCharge, String otherExpenses, String settlementType, String describe, String merchant,
                       String extDeliveryStartTime, String extDeliveryEndTime, String startPickUp, String endPickUp, String shipper, String shipperTelNum,
                       String shippingAddr, String shippingAddrNo) {
        this.extOrderNum = extOrderNum;
        this.freight = freight;
        this.insuredFee = insuredFee;
        this.packingCharges = packingCharges;
        this.deliveryFee = deliveryFee;
        this.upstairsFee = upstairsFee;
        this.receivingCharge = receivingCharge;
        this.otherExpenses = otherExpenses;
        this.settlementType = settlementType;
        this.describe = describe;
        this.merchant = merchant;
        this.extDeliveryStartTime = extDeliveryStartTime;
        this.extDeliveryEndTime = extDeliveryEndTime;
        this.startPickUp = startPickUp;
        this.endPickUp = endPickUp;
        this.shipper = shipper;
        this.shipperTelNum = shipperTelNum;
        this.shippingAddr = shippingAddr;
        this.shippingAddrNo = shippingAddrNo;
    }

    /** 外部订单号、运费、保价费、包装费、送货费、上楼费、接货费、其他费用、结算类型、描述、商户、商户编号、发货地址、发货地址编号、发货人、发货人电话、期望送达时间起、期望送达时间止 */
    public OrderTmplPo(String extOrderNum, String freight, String insuredFee, String packingCharges, String deliveryFee, String upstairsFee,
                       String receivingCharge, String otherExpenses, String settlementType, String describe, String merchant,
                       String merchantCode, String shippingAddr, String shippingAddrNo, String shipper, String shipperTelNum,
                       String extDeliveryStartTime, String extDeliveryEndTime, String consigneeTelNum, String unloadingAddrNo) {
        this.extOrderNum = extOrderNum;
        this.freight = freight;
        this.insuredFee = insuredFee;
        this.packingCharges = packingCharges;
        this.deliveryFee = deliveryFee;
        this.upstairsFee = upstairsFee;
        this.receivingCharge = receivingCharge;
        this.otherExpenses = otherExpenses;
        this.settlementType = settlementType;
        this.describe = describe;
        this.merchant = merchant;
        this.merchantCode = merchantCode;
        this.shippingAddr = shippingAddr;
        this.shippingAddrNo = shippingAddrNo;
        this.shipper = shipper;
        this.shipperTelNum = shipperTelNum;
        this.extDeliveryStartTime = extDeliveryStartTime;
        this.extDeliveryEndTime = extDeliveryEndTime;
        this.consigneeTelNum = consigneeTelNum;
        this.unloadingAddrNo = unloadingAddrNo;
    }

    /** 外部订单号、运费、保价费、包装费、送货费、上楼费、接货费、其他费用、结算类型、描述、商户、商户编号、发货地址、发货地址编号、发货人、发货人电话、期望送达时间起、期望送达时间止 */
    public OrderTmplPo(String extOrderNum, String freight, String insuredFee, String packingCharges, String deliveryFee, String upstairsFee,
                       String receivingCharge, String otherExpenses, String settlementType, String describe, String merchant,
                       String merchantCode, String shippingAddr, String shippingAddrNo, String shipper, String shipperTelNum,
                       String extDeliveryStartTime, String extDeliveryEndTime) {
        this.extOrderNum = extOrderNum;
        this.freight = freight;
        this.insuredFee = insuredFee;
        this.packingCharges = packingCharges;
        this.deliveryFee = deliveryFee;
        this.upstairsFee = upstairsFee;
        this.receivingCharge = receivingCharge;
        this.otherExpenses = otherExpenses;
        this.settlementType = settlementType;
        this.describe = describe;
        this.merchant = merchant;
        this.merchantCode = merchantCode;
        this.shippingAddr = shippingAddr;
        this.shippingAddrNo = shippingAddrNo;
        this.shipper = shipper;
        this.shipperTelNum = shipperTelNum;
        this.extDeliveryStartTime = extDeliveryStartTime;
        this.extDeliveryEndTime = extDeliveryEndTime;
    }

    public String getExtOrderNum() {
        return extOrderNum;
    }

    public void setExtOrderNum(String extOrderNum) {
        this.extOrderNum = extOrderNum;
    }

    public String getFreight() {
        return freight;
    }

    public void setFreight(String freight) {
        this.freight = freight;
    }

    public String getInsuredFee() {
        return insuredFee;
    }

    public void setInsuredFee(String insuredFee) {
        this.insuredFee = insuredFee;
    }

    public String getPackingCharges() {
        return packingCharges;
    }

    public void setPackingCharges(String packingCharges) {
        this.packingCharges = packingCharges;
    }

    public String getDeliveryFee() {
        return deliveryFee;
    }

    public void setDeliveryFee(String deliveryFee) {
        this.deliveryFee = deliveryFee;
    }

    public String getUpstairsFee() {
        return upstairsFee;
    }

    public void setUpstairsFee(String upstairsFee) {
        this.upstairsFee = upstairsFee;
    }

    public String getReceivingCharge() {
        return receivingCharge;
    }

    public void setReceivingCharge(String receivingCharge) {
        this.receivingCharge = receivingCharge;
    }

    public String getOtherExpenses() {
        return otherExpenses;
    }

    public void setOtherExpenses(String otherExpenses) {
        this.otherExpenses = otherExpenses;
    }

    public String getSettlementType() {
        return settlementType;
    }

    public void setSettlementType(String settlementType) {
        this.settlementType = settlementType;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    public String getMerchant() {
        return merchant;
    }

    public void setMerchant(String merchant) {
        this.merchant = merchant;
    }

    public String getMerchantCode() {
        return merchantCode;
    }

    public void setMerchantCode(String merchantCode) {
        this.merchantCode = merchantCode;
    }

    public String getShippingAddr() {
        return shippingAddr;
    }

    public void setShippingAddr(String shippingAddr) {
        this.shippingAddr = shippingAddr;
    }

    public String getShippingAddrNo() {
        return shippingAddrNo;
    }

    public void setShippingAddrNo(String shippingAddrNo) {
        this.shippingAddrNo = shippingAddrNo;
    }

    public String getShipper() {
        return shipper;
    }

    public void setShipper(String shipper) {
        this.shipper = shipper;
    }

    public String getShipperTelNum() {
        return shipperTelNum;
    }

    public void setShipperTelNum(String shipperTelNum) {
        this.shipperTelNum = shipperTelNum;
    }

    public String getConsignee() {
        return consignee;
    }

    public void setConsignee(String consignee) {
        this.consignee = consignee;
    }

    public String getConsigneeTelNum() {
        return consigneeTelNum;
    }

    public void setConsigneeTelNum(String consigneeTelNum) {
        this.consigneeTelNum = consigneeTelNum;
    }

    public String getUnloadingAddr() {
        return unloadingAddr;
    }

    public void setUnloadingAddr(String unloadingAddr) {
        this.unloadingAddr = unloadingAddr;
    }

    public String getUnloadingAddrNo() {
        return unloadingAddrNo;
    }

    public void setUnloadingAddrNo(String unloadingAddrNo) {
        this.unloadingAddrNo = unloadingAddrNo;
    }

    public String getStartPickUp() {
        return startPickUp;
    }

    public void setStartPickUp(String startPickUp) {
        this.startPickUp = startPickUp;
    }

    public String getEndPickUp() {
        return endPickUp;
    }

    public void setEndPickUp(String endPickUp) {
        this.endPickUp = endPickUp;
    }

    public String getExtDeliveryStartTime() {
        return extDeliveryStartTime;
    }

    public void setExtDeliveryStartTime(String extDeliveryStartTime) {
        this.extDeliveryStartTime = extDeliveryStartTime;
    }

    public String getExtDeliveryEndTime() {
        return extDeliveryEndTime;
    }

    public void setExtDeliveryEndTime(String extDeliveryEndTime) {
        this.extDeliveryEndTime = extDeliveryEndTime;
    }

    public List<GoodsTmplPo> getGoodsTmplList() {
        return goodsTmplList;
    }

    public void setGoodsTmplList(List<GoodsTmplPo> goodsTmplList) {
        this.goodsTmplList = goodsTmplList;
    }

    @Override
    public String toString() {
        return "OrderTmplPo{" +
                "extOrderNum='" + extOrderNum + '\'' +
                ", freight='" + freight + '\'' +
                ", insuredFee='" + insuredFee + '\'' +
                ", packingCharges='" + packingCharges + '\'' +
                ", deliveryFee='" + deliveryFee + '\'' +
                ", upstairsFee='" + upstairsFee + '\'' +
                ", receivingCharge='" + receivingCharge + '\'' +
                ", otherExpenses='" + otherExpenses + '\'' +
                ", settlementType='" + settlementType + '\'' +
                ", describe='" + describe + '\'' +
                ", merchant='" + merchant + '\'' +
                ", merchantCode='" + merchantCode + '\'' +
                ", shippingAddr='" + shippingAddr + '\'' +
                ", shippingAddrNo='" + shippingAddrNo + '\'' +
                ", shipper='" + shipper + '\'' +
                ", shipperTelNum='" + shipperTelNum + '\'' +
                ", consignee='" + consignee + '\'' +
                ", consigneeTelNum='" + consigneeTelNum + '\'' +
                ", unloadingAddr='" + unloadingAddr + '\'' +
                ", unloadingAddrNo='" + unloadingAddrNo + '\'' +
                ", startPickUp='" + startPickUp + '\'' +
                ", endPickUp='" + endPickUp + '\'' +
                ", extDeliveryStartTime='" + extDeliveryStartTime + '\'' +
                ", extDeliveryEndTime='" + extDeliveryEndTime + '\'' +
                ", goodsTmplList=" + goodsTmplList +
                '}';
    }
}
