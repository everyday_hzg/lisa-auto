package com.lisa.auto.entity.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @description:
 * @author: huangzhigang
 * @date: 2020-05-07 11:40
 **/
public class CarCondExportPo {

    /** 车队公司 */
    @Excel(name = "车队公司")
    private String fleet;

    /** 车架号 */
    @Excel(name = "车架号")
    private String vin;

    /** 经度 */
    @Excel(name = "经度")
    private BigDecimal longitude;

    /** 纬度 */
    @Excel(name = "纬度")
    private BigDecimal latitude;

    /** 速度km/h */
    @Excel(name = "速度[km/h]")
    private BigDecimal vehicleSpeed;

    /**  数据采集时间ms */
    @Excel(name = "上报时间")
    private Date acquisitionTime;

    /** 车牌号 */
    @Excel(name = "车牌号")
    private String plateLicenseNo;

    public CarCondExportPo() {
        super();
    }

    public CarCondExportPo(String fleet, String vin, BigDecimal latitude, BigDecimal longitude,
                           BigDecimal vehicleSpeed, Date acquisitionTime, String plateLicenseNo) {
        this.fleet = fleet;
        this.vin = vin;
        this.latitude = latitude;
        this.longitude = longitude;
        this.vehicleSpeed = vehicleSpeed;
        this.acquisitionTime = acquisitionTime;
        this.plateLicenseNo = plateLicenseNo;
    }

    public String getFleet() {
        return fleet;
    }

    public void setFleet(String fleet) {
        this.fleet = fleet;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public BigDecimal getLatitude() {
        return latitude;
    }

    public void setLatitude(BigDecimal latitude) {
        this.latitude = latitude;
    }

    public BigDecimal getLongitude() {
        return longitude;
    }

    public void setLongitude(BigDecimal longitude) {
        this.longitude = longitude;
    }

    public BigDecimal getVehicleSpeed() {
        return vehicleSpeed;
    }

    public void setVehicleSpeed(BigDecimal vehicleSpeed) {
        this.vehicleSpeed = vehicleSpeed;
    }

    public Date getAcquisitionTime() {
        return acquisitionTime;
    }

    public void setAcquisitionTime(Date acquisitionTime) {
        this.acquisitionTime = acquisitionTime;
    }
}
