package com.lisa.auto.entity.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;

/**
 * 城配-包裹信息
 * @author huangzhigang 2019/12/21
 */
public class GoodsTmplPo {

    @Excel(name = "包裹名称", width = 10)
    private String pclName;

    @Excel(name = "包裹类型", width = 10)
    private String pclType;

    @Excel(name = "包裹编号", width = 15)
    private String pclNum;

    @Excel(name = "包裹长（m）", width = 12)
    private String pclLength;

    @Excel(name = "包裹宽（m）", width = 12)
    private String pclWide;

    @Excel(name = "包裹高（m）", width = 12)
    private String pclHigh;

    @Excel(name = "容积（m3）", width = 12)
    private String volume;

    @Excel(name = "重量（kg）", width = 12)
    private String weight;

    @Excel(name = "价格", width = 5)
    private String price;

    @Excel(name = "包裹描述", width = 10)
    private String pclDescribe;

    public GoodsTmplPo() {
        super();
    }

    public GoodsTmplPo(String pclName, String pclType, String pclNum, String pclLength, String pclWide,
                       String pclHigh, String volume, String weight, String price, String pclDescribe) {
        this.pclName = pclName;
        this.pclType = pclType;
        this.pclNum = pclNum;
        this.pclLength = pclLength;
        this.pclWide = pclWide;
        this.pclHigh = pclHigh;
        this.volume = volume;
        this.weight = weight;
        this.price = price;
        this.pclDescribe = pclDescribe;
    }

    public String getPclName() {
        return pclName;
    }

    public void setPclName(String pclName) {
        this.pclName = pclName;
    }

    public String getPclType() {
        return pclType;
    }

    public void setPclType(String pclType) {
        this.pclType = pclType;
    }

    public String getPclNum() {
        return pclNum;
    }

    public void setPclNum(String pclNum) {
        this.pclNum = pclNum;
    }

    public String getPclLength() {
        return pclLength;
    }

    public void setPclLength(String pclLength) {
        this.pclLength = pclLength;
    }

    public String getPclWide() {
        return pclWide;
    }

    public void setPclWide(String pclWide) {
        this.pclWide = pclWide;
    }

    public String getPclHigh() {
        return pclHigh;
    }

    public void setPclHigh(String pclHigh) {
        this.pclHigh = pclHigh;
    }

    public String getVolume() {
        return volume;
    }

    public void setVolume(String volume) {
        this.volume = volume;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPclDescribe() {
        return pclDescribe;
    }

    public void setPclDescribe(String pclDescribe) {
        this.pclDescribe = pclDescribe;
    }

    @Override
    public String toString() {
        return "GoodsTmplPo{" +
                "pclName='" + pclName + '\'' +
                ", pclType='" + pclType + '\'' +
                ", pclNum='" + pclNum + '\'' +
                ", pclLength='" + pclLength + '\'' +
                ", pclWide='" + pclWide + '\'' +
                ", pclHigh='" + pclHigh + '\'' +
                ", volume='" + volume + '\'' +
                ", weight='" + weight + '\'' +
                ", price='" + price + '\'' +
                ", pclDescribe='" + pclDescribe + '\'' +
                '}';
    }
}
