package com.lisa.auto.entity.excel;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;

/**
 * @description:
 * @author: huangzhigang
 * @date: 2020-05-07 14:30
 **/
@TableName("v3_car_cond_info")
public class CarCondPo extends Model<CarCondPo> implements Serializable {

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @TableField("create_time")
    private String createTime;

    public CarCondPo() {
        super();
    }

    @Override
    protected Serializable pkVal() {
        return null;
    }

    public CarCondPo(String createTime) {
        this.createTime = createTime;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }
}
