package com.lisa.auto.entity.ceramics.po;

import cn.afterturn.easypoi.excel.annotation.Excel;

/**
 * 陶瓷全部商品信息 实体Po
 * @author huangzhigang 2020/1/13
 */
public class CeramicsGoodsPo {

    @Excel(name = "商品编号")
    private String goodNum;

    @Excel(name = "商品全名")
    private String goodName;

    @Excel(name = "单位")
    private String company;

    @Excel(name = "数量")
    private String quantity;

    @Excel(name = "地址")
    private String addr;

    @Excel(name = "详细地址")
    private String detailedAddr;

    public CeramicsGoodsPo() {
        super();
    }

    public CeramicsGoodsPo(String goodNum, String goodName, String company, String quantity, String addr, String detailedAddr) {
        this.goodNum = goodNum;
        this.goodName = goodName;
        this.company = company;
        this.quantity = quantity;
        this.addr = addr;
        this.detailedAddr = detailedAddr;
    }

    public String getGoodNum() {
        return goodNum;
    }

    public void setGoodNum(String goodNum) {
        this.goodNum = goodNum;
    }

    public String getGoodName() {
        return goodName;
    }

    public void setGoodName(String goodName) {
        this.goodName = goodName;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }

    public String getDetailedAddr() {
        return detailedAddr;
    }

    public void setDetailedAddr(String detailedAddr) {
        this.detailedAddr = detailedAddr;
    }
}
