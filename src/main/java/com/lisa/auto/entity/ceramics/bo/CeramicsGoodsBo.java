package com.lisa.auto.entity.ceramics.bo;

import com.lisa.auto.entity.ceramics.po.CeramicsGoodsPo;

/**
 * 陶瓷全部商品信息 实体Bo
 * @author huangzhigang 2020/1/13
 */
public class CeramicsGoodsBo extends CeramicsGoodsPo {
}
