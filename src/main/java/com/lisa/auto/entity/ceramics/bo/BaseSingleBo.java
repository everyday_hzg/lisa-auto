package com.lisa.auto.entity.ceramics.bo;

import com.lisa.auto.entity.ceramics.po.BaseSinglePo;

/**
 * 陶瓷原始单/基础单实体 BO类
 * @author huangzhigang 2020/1/13
 */
public class BaseSingleBo extends BaseSinglePo {
}
