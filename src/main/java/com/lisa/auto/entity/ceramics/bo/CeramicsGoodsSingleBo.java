package com.lisa.auto.entity.ceramics.bo;

import com.lisa.auto.entity.ceramics.po.CeramicsGoodsSinglePo;

/**
 * 陶瓷原始单/基础单商品实体 BO类
 * @author huangzhigang 2020/1/13
 */
public class CeramicsGoodsSingleBo extends CeramicsGoodsSinglePo {
}
