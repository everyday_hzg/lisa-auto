package com.lisa.auto.entity.ceramics.po;

import java.io.Serializable;
import java.util.List;

/**
 * 陶瓷原始单/基础单实体 PO类
 * @author huangzhigang 2020/1/13
 */
public class BaseSinglePo implements Serializable {

    /** 单位编号 */
    private String compNum;

    /** 客户地址 */
    private String cusAddr;

    /** 商品信息 */
    private List<CeramicsGoodsSinglePo> ceramicsGoodsPos;

    public BaseSinglePo() {
        super();
    }

    public BaseSinglePo(String compNum, String cusAddr, List<CeramicsGoodsSinglePo> ceramicsGoodsPos) {
        this.compNum = compNum;
        this.cusAddr = cusAddr;
        this.ceramicsGoodsPos = ceramicsGoodsPos;
    }

    public String getCompNum() {
        return compNum;
    }

    public void setCompNum(String compNum) {
        this.compNum = compNum;
    }

    public String getCusAddr() {
        return cusAddr;
    }

    public void setCusAddr(String cusAddr) {
        this.cusAddr = cusAddr;
    }

    public List<CeramicsGoodsSinglePo> getCeramicsGoodsPos() {
        return ceramicsGoodsPos;
    }

    public void setCeramicsGoodsPos(List<CeramicsGoodsSinglePo> ceramicsGoodsPos) {
        this.ceramicsGoodsPos = ceramicsGoodsPos;
    }
}
