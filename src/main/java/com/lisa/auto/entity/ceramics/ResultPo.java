package com.lisa.auto.entity.ceramics;

/**
 * @description:
 * @author: huangzhigang
 * @date: 2020-04-01 16:20
 **/
public class ResultPo {

    private int code;

    private String data;

    private String msg;

    private String resultNo;

    private String resultVal;

    public ResultPo() {
        super();
    }

    public ResultPo(int code, String data, String msg) {
        this.code = code;
        this.data = data;
        this.msg = msg;
    }

    public ResultPo(int code, String data, String msg, String resultNo, String resultVal) {
        this.code = code;
        this.data = data;
        this.msg = msg;
        this.resultNo = resultNo;
        this.resultVal = resultVal;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getResultNo() {
        return resultNo;
    }

    public void setResultNo(String resultNo) {
        this.resultNo = resultNo;
    }

    public String getResultVal() {
        return resultVal;
    }

    public void setResultVal(String resultVal) {
        this.resultVal = resultVal;
    }
}
