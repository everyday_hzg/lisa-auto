package com.lisa.auto.entity.ceramics.po;

import cn.afterturn.easypoi.excel.annotation.Excel;

import java.io.Serializable;

/**
 * 陶瓷原始单/基础单商品实体 PO类
 * @author huangzhigang 2020/1/13
 */
public class CeramicsGoodsSinglePo implements Serializable {

    /** 条码 */
    @Excel(name = "条码")
    private String barCode;

    /** 商品名称 */
    @Excel(name = "商品名称")
    private String goodName;

    /** 商品数量 */
    @Excel(name = "商品数量")
    private String goodQuantity;

    /** 备注 */
    @Excel(name = "备注")
    private String remarks;

    public CeramicsGoodsSinglePo() {
        super();
    }

    public CeramicsGoodsSinglePo(String barCode, String goodName, String goodQuantity, String remarks) {
        this.barCode = barCode;
        this.goodName = goodName;
        this.goodQuantity = goodQuantity;
        this.remarks = remarks;
    }

    public String getBarCode() {
        return barCode;
    }

    public void setBarCode(String barCode) {
        this.barCode = barCode;
    }

    public String getGoodName() {
        return goodName;
    }

    public void setGoodName(String goodName) {
        this.goodName = goodName;
    }

    public String getGoodQuantity() {
        return goodQuantity;
    }

    public void setGoodQuantity(String goodQuantity) {
        this.goodQuantity = goodQuantity;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }
}
