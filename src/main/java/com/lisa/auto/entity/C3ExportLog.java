package com.lisa.auto.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 接口日志
 * </p>
 *
 * @author suohao
 * @since 2019-12-23
 */
@TableName("c3_export_log")
public class C3ExportLog extends Model<C3ExportLog> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
	@TableId(value="id", type= IdType.AUTO)
	private Long id;
    /**
     * 类型
     */
	@TableField("export_type")
	private String exportType;
    /**
     * 导出key
     */
	@TableField("export_key")
	private String exportKey;
    /**
     * 目标系统(10:ILS)
     */
	@TableField("target_sys")
	private String targetSys;
    /**
     * 接口地址
     */
	@TableField("interface_url")
	private String interfaceUrl;
    /**
     * 导出状态(1:成功,0:失败)
     */
	@TableField("export_status")
	private String exportStatus;
    /**
     * 请求内容
     */
	@TableField("data_content")
	private String dataContent;
    /**
     * 响应内容
     */
	@TableField("export_response")
	private String exportResponse;
    /**
     * 创建时间
     */
	@TableField("gmt_create")
	private Date gmtCreate;
    /**
     * 备注
     */
	private String remarks;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getExportType() {
		return exportType;
	}

	public void setExportType(String exportType) {
		this.exportType = exportType;
	}

	public String getExportKey() {
		return exportKey;
	}

	public void setExportKey(String exportKey) {
		this.exportKey = exportKey;
	}

	public String getTargetSys() {
		return targetSys;
	}

	public void setTargetSys(String targetSys) {
		this.targetSys = targetSys;
	}

	public String getInterfaceUrl() {
		return interfaceUrl;
	}

	public void setInterfaceUrl(String interfaceUrl) {
		this.interfaceUrl = interfaceUrl;
	}

	public String getExportStatus() {
		return exportStatus;
	}

	public void setExportStatus(String exportStatus) {
		this.exportStatus = exportStatus;
	}

	public String getDataContent() {
		return dataContent;
	}

	public void setDataContent(String dataContent) {
		this.dataContent = dataContent;
	}

	public String getExportResponse() {
		return exportResponse;
	}

	public void setExportResponse(String exportResponse) {
		this.exportResponse = exportResponse;
	}

	public Date getGmtCreate() {
		return gmtCreate;
	}

	public void setGmtCreate(Date gmtCreate) {
		this.gmtCreate = gmtCreate;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "C3ExportLog{" +
			", id=" + id +
			", exportType=" + exportType +
			", exportKey=" + exportKey +
			", targetSys=" + targetSys +
			", interfaceUrl=" + interfaceUrl +
			", exportStatus=" + exportStatus +
			", dataContent=" + dataContent +
			", exportResponse=" + exportResponse +
			", gmtCreate=" + gmtCreate +
			", remarks=" + remarks +
			"}";
	}
}
