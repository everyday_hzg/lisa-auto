package com.lisa.auto.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 车辆信息
 * </p>
 *
 * @author suohao
 * @since 2019-12-20
 */
@TableName("c3_vehicle")
public class Vehicle extends Model<Vehicle> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * c3系统编号
     */
    private String no;
    /**
     * 所属地区id
     */
    @TableField("area_id")
    private Long areaId;
    /**
     * 车架号
     */
    private String vin;
    /**
     * 车牌号
     */
    @TableField("plate_license_no")
    private String plateLicenseNo;
    /**
     * 品牌名称
     */
    @TableField("brand_name")
    private String brandName;
    /**
     * 品牌编号
     */
    @TableField("brand_no")
    private String brandNo;
    /**
     * 服务商编号
     */
    @TableField("service_no")
    private String serviceNo;
    /**
     * 服务商名称
     */
    @TableField("service_name")
    private String serviceName;
    /**
     * 车系编号
     */
    @TableField("series_no")
    private String seriesNo;
    /**
     * 车系名称
     */
    @TableField("series_name")
    private String seriesName;
    /**
     * 车型编号
     */
    @TableField("model_no")
    private String modelNo;
    /**
     * 车型名称
     */
    @TableField("model_name")
    private String modelName;
    /**
     * 制造商编号
     */
    @TableField("maker_no")
    private String makerNo;
    /**
     * 制造商名称
     */
    @TableField("maker_name")
    private String makerName;
    /**
     * 行驶证号
     */
    @TableField("driving_license_no")
    private String drivingLicenseNo;
    /**
     * 行驶证到期日
     */
    @TableField("driving_license_expired_date")
    private Date drivingLicenseExpiredDate;
    /**
     * 颜色
     */
    private String color;
    /**
     * 发动机编号
     */
    @TableField("engine_no")
    private String engineNo;
    /**
     * 网关no
     */
    @TableField("gate_way_no")
    private String gateWayNo;
    /**
     * 网关服务
     */
    @TableField("gate_way_name")
    private String gateWayName;
    /**
     * 注册日期
     */
    @TableField("register_date")
    private Date registerDate;
    /**
     * 使用年限
     */
    @TableField("service_life")
    private Long serviceLife;
    /**
     * 设备ID
     */
    private String sim;
    /**
     * 备注
     */
    private String description;
    /**
     * Tbox型号
     */
    @TableField("tbox_model_no")
    private String tboxModelNo;
    /**
     * Tbox品牌
     */
    @TableField("tbox_model_name")
    private String tboxModelName;
    /**
     * 动力类型(BLADE_ELECTRIC_VEHICLE:电动;HYBRID_ELECTRICAL_VEHICLE:混合动力;GAS_VEHICLE:燃油)
     */
    @TableField("power_type")
    private String powerType;
    /**
     * 年代
     */
    private String year;
    /**
     * tbox_no
     */
    @TableField("tbox_no")
    private String tboxNo;
    /**
     * tbox_id
     */
    @TableField("tbox_id")
    private String tboxId;
    /**
     * 订阅状态
     */
    @TableField("subscribe_status")
    private String subscribeStatus;
    /**
     * 删除标识，默认0，若删除，置为1
     */
    @TableField("is_delete")
    private Integer isDelete;
    /**
     * 创建时间
     */
    @TableField("gmt_create")
    private Date gmtCreate;
    /**
     * 状态
     */
    @TableField("status")
    private String status;
    /**
     * 所属企业
     */
    @TableField("fleet")
    private String fleet;
    /**
     * 所属省
     */
    @TableField(exist = false)
    private String province;
    /**
     * 所属市
     */
    @TableField(exist = false)
    private String city;
    /**
     * 所属省
     */
    @TableField(exist = false)
    private String county;

    @TableField(exist = false)
    private String vehicleStatus;


    public String getFleet() {
        return fleet;
    }

    public void setFleet(String fleet) {
        this.fleet = fleet;
    }

    public Long getAreaId() {
        return areaId;
    }

    public void setAreaId(Long areaId) {
        this.areaId = areaId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getPlateLicenseNo() {
        return plateLicenseNo;
    }

    public void setPlateLicenseNo(String plateLicenseNo) {
        this.plateLicenseNo = plateLicenseNo;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getBrandNo() {
        return brandNo;
    }

    public void setBrandNo(String brandNo) {
        this.brandNo = brandNo;
    }

    public String getServiceNo() {
        return serviceNo;
    }

    public void setServiceNo(String serviceNo) {
        this.serviceNo = serviceNo;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getSeriesNo() {
        return seriesNo;
    }

    public void setSeriesNo(String seriesNo) {
        this.seriesNo = seriesNo;
    }

    public String getSeriesName() {
        return seriesName;
    }

    public void setSeriesName(String seriesName) {
        this.seriesName = seriesName;
    }

    public String getModelNo() {
        return modelNo;
    }

    public void setModelNo(String modelNo) {
        this.modelNo = modelNo;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getMakerNo() {
        return makerNo;
    }

    public void setMakerNo(String makerNo) {
        this.makerNo = makerNo;
    }

    public String getMakerName() {
        return makerName;
    }

    public void setMakerName(String makerName) {
        this.makerName = makerName;
    }

    public String getDrivingLicenseNo() {
        return drivingLicenseNo;
    }

    public void setDrivingLicenseNo(String drivingLicenseNo) {
        this.drivingLicenseNo = drivingLicenseNo;
    }

    public Date getDrivingLicenseExpiredDate() {
        return drivingLicenseExpiredDate;
    }

    public void setDrivingLicenseExpiredDate(Date drivingLicenseExpiredDate) {
        this.drivingLicenseExpiredDate = drivingLicenseExpiredDate;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getEngineNo() {
        return engineNo;
    }

    public void setEngineNo(String engineNo) {
        this.engineNo = engineNo;
    }

    public String getGateWayNo() {
        return gateWayNo;
    }

    public void setGateWayNo(String gateWayNo) {
        this.gateWayNo = gateWayNo;
    }

    public String getGateWayName() {
        return gateWayName;
    }

    public void setGateWayName(String gateWayName) {
        this.gateWayName = gateWayName;
    }

    public Date getRegisterDate() {
        return registerDate;
    }

    public void setRegisterDate(Date registerDate) {
        this.registerDate = registerDate;
    }

    public Long getServiceLife() {
        return serviceLife;
    }

    public void setServiceLife(Long serviceLife) {
        this.serviceLife = serviceLife;
    }

    public String getSim() {
        return sim;
    }

    public void setSim(String sim) {
        this.sim = sim;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTboxModelNo() {
        return tboxModelNo;
    }

    public void setTboxModelNo(String tboxModelNo) {
        this.tboxModelNo = tboxModelNo;
    }

    public String getTboxModelName() {
        return tboxModelName;
    }

    public void setTboxModelName(String tboxModelName) {
        this.tboxModelName = tboxModelName;
    }

    public String getPowerType() {
        return powerType;
    }

    public void setPowerType(String powerType) {
        this.powerType = powerType;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getTboxNo() {
        return tboxNo;
    }

    public void setTboxNo(String tboxNo) {
        this.tboxNo = tboxNo;
    }

    public String getTboxId() {
        return tboxId;
    }

    public void setTboxId(String tboxId) {
        this.tboxId = tboxId;
    }

    public String getSubscribeStatus() {
        return subscribeStatus;
    }

    public void setSubscribeStatus(String subscribeStatus) {
        this.subscribeStatus = subscribeStatus;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getVehicleStatus() {
        return vehicleStatus;
    }

    public void setVehicleStatus(String vehicleStatus) {
        this.vehicleStatus = vehicleStatus;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "C3Vehicle{" +
                ", id=" + id +
                ", no=" + no +
                ", vin=" + vin +
                ", plateLicenseNo=" + plateLicenseNo +
                ", brandName=" + brandName +
                ", brandNo=" + brandNo +
                ", serviceNo=" + serviceNo +
                ", serviceName=" + serviceName +
                ", seriesNo=" + seriesNo +
                ", seriesName=" + seriesName +
                ", modelNo=" + modelNo +
                ", modelName=" + modelName +
                ", makerNo=" + makerNo +
                ", makerName=" + makerName +
                ", drivingLicenseNo=" + drivingLicenseNo +
                ", drivingLicenseExpiredDate=" + drivingLicenseExpiredDate +
                ", color=" + color +
                ", engineNo=" + engineNo +
                ", gateWayNo=" + gateWayNo +
                ", gateWayName=" + gateWayName +
                ", registerDate=" + registerDate +
                ", serviceLife=" + serviceLife +
                ", sim=" + sim +
                ", description=" + description +
                ", tboxModelNo=" + tboxModelNo +
                ", tboxModelName=" + tboxModelName +
                ", powerType=" + powerType +
                ", year=" + year +
                ", tboxNo=" + tboxNo +
                ", tboxId=" + tboxId +
                ", subscribeStatus=" + subscribeStatus +
                ", isDelete=" + isDelete +
                ", gmtCreate=" + gmtCreate +
                ", status=" + status +
                "}";
    }
}
