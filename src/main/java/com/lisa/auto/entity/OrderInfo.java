package com.lisa.auto.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;

import java.util.Date;

import java.io.Serializable;

/**
 * <p>
 * 订单信息
 * </p>
 *
 * @author huangzhigang
 * @since 2019-12-02
 */
public class OrderInfo implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * 数据ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 收件人
     */
    @TableField("addressee")
    private String addressee;

    /**
     * 联系方式
     */
    @TableField("telephone")
    private String telephone;

    /**
     * 收件地址
     */
    @TableField("shipping_address")
    private String shippingAddress;

    /**
     * 送达时间（要求）
     */
    @TableField("estimated_time")
    private String estimatedTime;

    /**
     * 商品名称
     */
    @TableField("shop_name")
    private String shopName;

    /**
     * 装载类型（1：托盘，2：笼，3：箱，4：胶箱，5：捆，6：件），默认为0
     */
    @TableField("slacs_type")
    private Integer slacsType;

    /**
     * 商品数量
     */
    @TableField("shop_num")
    private Integer shopNum;

    /**
     * 送货司机
     */
    @TableField("delivery_driver")
    private String deliveryDriver;

    /**
     * 送货司机联系电话
     */
    @TableField("driver_phone")
    private String driverPhone;

    /**
     * 备注
     */
    @TableField("remark")
    private String remark;

    /**
     * 创建人
     */
    @TableField("create_user")
    private String createUser;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;

    /**
     * 更新人
     */
    @TableField("update_user")
    private String updateUser;

    /**
     * 更新时间
     */
    @TableField("update_time")
    private Date updateTime;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAddressee() {
        return addressee;
    }

    public void setAddressee(String addressee) {
        this.addressee = addressee;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(String shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public String getEstimatedTime() {
        return estimatedTime;
    }

    public void setEstimatedTime(String estimatedTime) {
        this.estimatedTime = estimatedTime;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public Integer getSlacsType() {
        return slacsType;
    }

    public void setSlacsType(Integer slacsType) {
        this.slacsType = slacsType;
    }

    public Integer getShopNum() {
        return shopNum;
    }

    public void setShopNum(Integer shopNum) {
        this.shopNum = shopNum;
    }

    public String getDeliveryDriver() {
        return deliveryDriver;
    }

    public void setDeliveryDriver(String deliveryDriver) {
        this.deliveryDriver = deliveryDriver;
    }

    public String getDriverPhone() {
        return driverPhone;
    }

    public void setDriverPhone(String driverPhone) {
        this.driverPhone = driverPhone;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "OrderInfo{" +
        "id=" + id +
        ", addressee=" + addressee +
        ", telephone=" + telephone +
        ", shippingAddress=" + shippingAddress +
        ", estimatedTime=" + estimatedTime +
        ", shopName=" + shopName +
        ", slacsType=" + slacsType +
        ", shopNum=" + shopNum +
        ", deliveryDriver=" + deliveryDriver +
        ", driverPhone=" + driverPhone +
        ", createUser=" + createUser +
        ", createTime=" + createTime +
        ", updateUser=" + updateUser +
        ", updateTime=" + updateTime +
        "}";
    }
}
