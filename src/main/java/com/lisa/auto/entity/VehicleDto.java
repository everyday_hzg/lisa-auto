package com.lisa.auto.entity;

/**
 * @author : suohao
 * Date: 2020/1/6
 */
public class VehicleDto {
    /**
     * 车辆信息
     */
    private Vehicle vehicle;

    /**
     * 车辆所属地区
     */
    private VehicleArea vehicleArea;

    public Vehicle getVehicle() {
        return vehicle;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    public VehicleArea getVehicleArea() {
        return vehicleArea;
    }

    public void setVehicleArea(VehicleArea vehicleArea) {
        this.vehicleArea = vehicleArea;
    }
}
