package com.lisa.auto.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 车辆故障配置
 * </p>
 *
 * @author suohao
 * @since 2020-02-21
 */
@TableName("c3_fault_configuration")
public class C3FaultConfiguration extends Model<C3FaultConfiguration> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
	@TableId(value="id", type= IdType.AUTO)
	private Long id;
    /**
     * 外部唯一标识
     */
	@TableField("event_id")
	private String eventId;
    /**
     * 车辆设备
     */
	@TableField("vehicle_equipment")
	private String vehicleEquipment;
    /**
     * 故障类型
     */
	@TableField("fault_type")
	private String faultType;
    /**
     * 设备详细故障项
     */
	@TableField("equipment_fault_item")
	private String equipmentFaultItem;
    /**
     * 故障等级数值
     */
	@TableField("fault_level")
	private Integer faultLevel;
    /**
     * 备注
     */
	private String remark;
    /**
     * 创建时间
     */
	@TableField("gmt_create")
	private Date gmtCreate;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEventId() {
		return eventId;
	}

	public void setEventId(String eventId) {
		this.eventId = eventId;
	}

	public String getVehicleEquipment() {
		return vehicleEquipment;
	}

	public void setVehicleEquipment(String vehicleEquipment) {
		this.vehicleEquipment = vehicleEquipment;
	}

	public String getFaultType() {
		return faultType;
	}

	public void setFaultType(String faultType) {
		this.faultType = faultType;
	}

	public String getEquipmentFaultItem() {
		return equipmentFaultItem;
	}

	public void setEquipmentFaultItem(String equipmentFaultItem) {
		this.equipmentFaultItem = equipmentFaultItem;
	}

	public Integer getFaultLevel() {
		return faultLevel;
	}

	public void setFaultLevel(Integer faultLevel) {
		this.faultLevel = faultLevel;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Date getGmtCreate() {
		return gmtCreate;
	}

	public void setGmtCreate(Date gmtCreate) {
		this.gmtCreate = gmtCreate;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "C3FaultConfiguration{" +
			", id=" + id +
			", eventId=" + eventId +
			", vehicleEquipment=" + vehicleEquipment +
			", faultType=" + faultType +
			", equipmentFaultItem=" + equipmentFaultItem +
			", faultLevel=" + faultLevel +
			", remark=" + remark +
			", gmtCreate=" + gmtCreate +
			"}";
	}
}
