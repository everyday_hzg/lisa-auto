package com.lisa.auto.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

/**
 * @description:
 * @author: huangzhigang
 * @date: 2020-04-01 17:19
 **/
@TableName("v1_comd")
public class ComdPo {

    @TableId(value="id", type= IdType.AUTO)
    private Long id;

    @TableField("comd_name")
    private String comdName;

    @TableField("unit")
    private String unit;

    @TableField("length")
    private String length;

    @TableField("width")
    private String width;

    @TableField("height")
    private String height;

    public ComdPo() {
        super();
    }

    public ComdPo(Long id, String comdName, String unit, String length, String width, String height) {
        this.id = id;
        this.comdName = comdName;
        this.unit = unit;
        this.length = length;
        this.width = width;
        this.height = height;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getComdName() {
        return comdName;
    }

    public void setComdName(String comdName) {
        this.comdName = comdName;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getLength() {
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }
}
