package com.lisa.auto.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 风险故障记录表
 * </p>
 *
 * @author suohao
 * @since 2020-02-21
 */
@TableName("c3_risk_fault")
public class C3RiskFault extends Model<C3RiskFault> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
	@TableId(value="id", type= IdType.AUTO)
	private Long id;
    /**
     * 事件标识
     */
	@TableField("event_id")
	private String eventId;
    /**
     * 车架号
     */
	private String vin;
    /**
     * 车辆设备
     */
	@TableField("vehicle_equipment")
	private String vehicleEquipment;
    /**
     * 故障类型
     */
	@TableField("fault_type")
	private String faultType;
    /**
     * 设备详细故障项
     */
	@TableField("equipment_fault_item")
	private String equipmentFaultItem;
    /**
     * 故障等级数值
     */
	@TableField("fault_level")
	private Integer faultLevel;
    /**
     * 备注
     */
	private int count;
    /**
     * 故障/风险开始时间
     */
	@TableField("start_time")
	private Date startTime;
    /**
     * 故障/风险结束时间
     */
	@TableField("end_time")
	private Date endTime;
	/**
     * 故障/风险结束时间
     */
	@TableField("type")
	private String type;
    /**
     * 创建时间
     */
	@TableField("gmt_create")
	private Date gmtCreate;

	public C3RiskFault() {
	}

	public C3RiskFault(String vin) {
		this.vin = vin;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEventId() {
		return eventId;
	}

	public void setEventId(String eventId) {
		this.eventId = eventId;
	}

	public String getVin() {
		return vin;
	}

	public void setVin(String vin) {
		this.vin = vin;
	}

	public String getVehicleEquipment() {
		return vehicleEquipment;
	}

	public void setVehicleEquipment(String vehicleEquipment) {
		this.vehicleEquipment = vehicleEquipment;
	}

	public String getFaultType() {
		return faultType;
	}

	public void setFaultType(String faultType) {
		this.faultType = faultType;
	}

	public String getEquipmentFaultItem() {
		return equipmentFaultItem;
	}

	public void setEquipmentFaultItem(String equipmentFaultItem) {
		this.equipmentFaultItem = equipmentFaultItem;
	}

	public Integer getFaultLevel() {
		return faultLevel;
	}

	public void setFaultLevel(Integer faultLevel) {
		this.faultLevel = faultLevel;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public Date getGmtCreate() {
		return gmtCreate;
	}

	public void setGmtCreate(Date gmtCreate) {
		this.gmtCreate = gmtCreate;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "C3RiskFault{" +
			", id=" + id +
			", eventId=" + eventId +
			", vin=" + vin +
			", vehicleEquipment=" + vehicleEquipment +
			", faultType=" + faultType +
			", equipmentFaultItem=" + equipmentFaultItem +
			", faultLevel=" + faultLevel +
			", count=" + count +
			", startTime=" + startTime +
			", endTime=" + endTime +
			", gmtCreate=" + gmtCreate +
			"}";
	}
}
