package com.lisa.auto.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @description:
 * @author: huangzhigang
 * @date: 2020-03-30 14:37
 **/
@TableName("c3_car_cond_statistics")
public class C3CarCondStatistics extends Model<C3CarCondStatistics> {

    @TableField("id")
    private Long id;

    /**
     * 所属企业
     */
    @TableField("fleet_id")
    private Long fleetId;

    /**
     * 所属企业
     */
    @TableField("fleet")
    private String fleet;

    @TableField("vin")
    private String vin;

    /** 车辆总里程 */
    @TableField("total_distance")
    private BigDecimal totalDistance;

    /** 当天行驶里程 */
    @TableField("distance")
    private BigDecimal distance;

    /**
     * 故障/风险开始时间
     */
    @TableField("start_time")
    private Date startTime;
    /**
     * 故障/风险结束时间
     */
    @TableField("end_time")
    private Date endTime;
    /**
     * 数据类型(1:正常/运行，2：风险，3：故障，4：驾驶风险)
     */
    @TableField("data_type")
    private String dataType;
    /**
     * 创建时间
     */
    @TableField("gmt_create")
    private Date gmtCreate;

    public C3CarCondStatistics() {
    }

    public C3CarCondStatistics(Long fleetId, String fleet, String vin, BigDecimal totalDistance, Date startTime, Date endTime, String dataType, Date gmtCreate) {
        this.fleetId = fleetId;
        this.fleet = fleet;
        this.vin = vin;
        this.totalDistance = totalDistance;
        this.startTime = startTime;
        this.endTime = endTime;
        this.dataType = dataType;
        this.gmtCreate = gmtCreate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getFleetId() {
        return fleetId;
    }

    public void setFleetId(Long fleetId) {
        this.fleetId = fleetId;
    }

    public String getFleet() {
        return fleet;
    }

    public void setFleet(String fleet) {
        this.fleet = fleet;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public BigDecimal getTotalDistance() {
        return totalDistance;
    }

    public void setTotalDistance(BigDecimal totalDistance) {
        this.totalDistance = totalDistance;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public BigDecimal getDistance() {
        return distance;
    }

    public void setDistance(BigDecimal distance) {
        this.distance = distance;
    }

    @Override
    protected Serializable pkVal() {
        return null;
    }
}
