package com.lisa.auto.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 车辆历史轨迹信息
 * </p>
 *
 * @author suohao
 * @since 2019-12-20
 */
@TableName("c3_history_trail")
public class C3HistoryTrail extends Model<C3HistoryTrail> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
	@TableId(value="id", type= IdType.AUTO)
	private Long id;
    /**
     * 纬度，火星坐标
     */
	private BigDecimal latitude;
    /**
     * 经度，火星坐标
     */
	private BigDecimal longitude;
    /**
     * 速度km/h
     */
	@TableField("vehicle_speed")
	private BigDecimal vehicleSpeed;
    /**
     * vin
     */
	private String vin;
    /**
     * GPS上报时间
     */
	@TableField("gps_time")
	private Date gpsTime;
    /**
     * 数据采集的时间
     */
	@TableField("acquisition_time")
	private Date acquisitionTime;
    /**
     * 创建时间
     */
	@TableField("gmt_create")
	private Date gmtCreate;
    /**
     * 更新时间
     */
	@TableField("gmt_modified")
	private Date gmtModified;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BigDecimal getLatitude() {
		return latitude;
	}

	public void setLatitude(BigDecimal latitude) {
		this.latitude = latitude;
	}

	public BigDecimal getLongitude() {
		return longitude;
	}

	public void setLongitude(BigDecimal longitude) {
		this.longitude = longitude;
	}

	public BigDecimal getVehicleSpeed() {
		return vehicleSpeed;
	}

	public void setVehicleSpeed(BigDecimal vehicleSpeed) {
		this.vehicleSpeed = vehicleSpeed;
	}

	public String getVin() {
		return vin;
	}

	public void setVin(String vin) {
		this.vin = vin;
	}

	public Date getGpsTime() {
		return gpsTime;
	}

	public void setGpsTime(Date gpsTime) {
		this.gpsTime = gpsTime;
	}

	public Date getAcquisitionTime() {
		return acquisitionTime;
	}

	public void setAcquisitionTime(Date acquisitionTime) {
		this.acquisitionTime = acquisitionTime;
	}

	public Date getGmtCreate() {
		return gmtCreate;
	}

	public void setGmtCreate(Date gmtCreate) {
		this.gmtCreate = gmtCreate;
	}

	public Date getGmtModified() {
		return gmtModified;
	}

	public void setGmtModified(Date gmtModified) {
		this.gmtModified = gmtModified;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "C3HistoryTrail{" +
			", id=" + id +
			", latitude=" + latitude +
			", longitude=" + longitude +
			", vehicleSpeed=" + vehicleSpeed +
			", vin=" + vin +
			", gpsTime=" + gpsTime +
			", acquisitionTime=" + acquisitionTime +
			", gmtCreate=" + gmtCreate +
			", gmtModified=" + gmtModified +
			"}";
	}
}
