package com.lisa.auto.scheduled;

import com.lisa.auto.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @description: 车况数据汇总
 * @author: huangzhigang
 * @date: 2020-03-28 16:05
 **/
@Component
public class ReportCarScheduled1 {

    private static final Logger logger = LoggerFactory.getLogger(ReportCarScheduled1.class);

    @Autowired
    private C3ReportCarInfo1Service c3ReportCarInfo1Service;

    @Scheduled(cron="0 0 2 * * ?")   //每30分钟执行一次
    public void execute() {
        try {
            c3ReportCarInfo1Service.delHistoryData();
        } catch (Exception e) {
            logger.info(e.getMessage(), e);
        }
    }

}
