package com.lisa.auto.scheduled;

import com.lisa.auto.service.excel.CarCondExportService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @description:
 * @author: huangzhigang
 * @date: 2020-05-07 14:01
 **/
@Component
public class CarCondExportScheduled {

    private static final Logger logger = LoggerFactory.getLogger(CarCondExportScheduled.class);

    @Autowired
    private CarCondExportService carCondExportService;

    @Scheduled(cron="0 0 1 * * ?")
    public void handle() {
        try {
            carCondExportService.export();
        } catch (Exception e) {
            logger.info(e.getMessage(), e);
        }
    }
}
