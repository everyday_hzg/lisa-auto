package com.lisa.auto.scheduled;

import com.lisa.auto.service.C3HistoryTrailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

@Component
public class HistoryTrailScheduled {

    private static final Logger LOGGER = LoggerFactory.getLogger(HistoryTrailScheduled.class);


    @Autowired
    private C3HistoryTrailService historyTrailService;

   // @Scheduled(cron = "0 0 1 * * ?")
    public void HistoryTrailToRedisScheduled(){
        SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
        long startTime = System.currentTimeMillis();
        LOGGER.info("HistoryTrailToRedisScheduled startTime: {}", startTime);
        try {
            historyTrailService.historyTrailToRedis();
            long endTime = System.currentTimeMillis();
            LOGGER.info("HistoryTrailToRedisScheduled 时间: {}", endTime-startTime);
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error("HistoryTrailToRedisScheduled 定时任务出错 error: {}", e);
        }
    }
}
