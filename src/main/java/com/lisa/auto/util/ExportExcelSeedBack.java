package com.lisa.auto.util;

import java.io.IOException;
import java.io.OutputStream;

import org.apache.poi.ss.usermodel.Workbook;

/**
 * @author huangzhigang 2019/12/27
 */
public class ExportExcelSeedBack {

    /*
     * 导出数据
     * */
    public static void export(OutputStream out, Workbook workbook) throws Exception {
        try {
            if (workbook != null) {
                try {
                    workbook.write(out);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            out.close();
        }
    }
}
