package com.lisa.auto.util;

import com.alibaba.fastjson.JSONObject;
import com.lisa.auto.tools.seveneleven.SevenElevenComs;
import com.lisa.auto.util.basic.BaseException;
import io.swagger.models.auth.In;
import org.apache.commons.lang3.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;
import java.time.temporal.TemporalAdjusters;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 基于Java8的时间工具类
 *
 * @author huangzhigang 2019/12/24
 */
public class DateUtil {

    /**
     * 例如:20181228
     */
    public static final String DATE_YYYY_MM_DD = "yyyyMMdd";

    /**
     * 例如:2018-12-28
     */
    public static final String DATE = "yyyy/MM/dd";

    /**
     * 例如:2018-12-28
     */
    public static final String DATE_1 = "yyyy-MM-dd";

    /**
     * 例如:2018-12-28 10:00:00
     */
    public static final String DATE_TIME = "yyyy-MM-dd HH:mm:ss";

    /**
     * 例如:20181228100000
     */
    public static final String DATE_TIME_1 = "yyyyMMddHHmmss";
    /**
     * 例如:2018-12-28 10:00:00
     */
    public static final String DATE_TIME_2 = "yyyy/MM/dd HH:mm:ss";


    /**
     * 例如:10:00:00
     */
    public static final String TIME = "HHmmss";
    /**
     * 例如:10:00
     */
    public static final String TIME_WITHOUT_SECOND = "HH:mm";

    /**
     * 例如:2018-12-28 10:00
     */
    public static final String DATE_TIME_WITHOUT_SECONDS = "yyyy-MM-dd HH:mm";


    /**
     * 获取年
     *
     * @return 年
     */
    public static int getYear() {
        LocalTime localTime = LocalTime.now();
        return localTime.get(ChronoField.YEAR);
    }

    /**
     * 获取月份
     *
     * @return 月份
     */
    public static int getMonth() {
        LocalTime localTime = LocalTime.now();
        return localTime.get(ChronoField.MONTH_OF_YEAR);
    }

    /**
     * 获取某月的第几天
     *
     * @return 几号
     */
    public static int getMonthOfDay() {
        LocalTime localTime = LocalTime.now();
        return localTime.get(ChronoField.DAY_OF_MONTH);
    }

    /**
     * 格式化日期为字符串
     *
     * @param pattern 格式
     * @return 日期字符串
     */
    public static String format(String pattern) {
        Instant instant = new Date().toInstant();
        LocalDateTime localDateTime = LocalDateTime.ofInstant(instant, ZoneId.systemDefault());
        return localDateTime.format(DateTimeFormatter.ofPattern(pattern));
    }

    /**
     * 格式化日期为字符串
     *
     * @param pattern 格式
     * @return 日期字符串
     */
    public static String format(Date date, String pattern) {
        Instant instant = date.toInstant();
        LocalDateTime localDateTime = LocalDateTime.ofInstant(instant, ZoneId.systemDefault());
        return localDateTime.format(DateTimeFormatter.ofPattern(pattern));
    }

    /**
     * 解析字符串日期为Date
     *
     * @param dateStr 日期字符串
     * @param pattern 格式
     * @return Date
     */
    public static Date parse(String dateStr, String pattern) {
        LocalDateTime localDateTime = LocalDateTime.parse(dateStr, DateTimeFormatter.ofPattern(pattern));
        Instant instant = localDateTime.atZone(ZoneId.systemDefault()).toInstant();
        return Date.from(instant);
    }

    /**
     * 为Date增加分钟,减传负数
     *
     * @param date        日期
     * @param plusMinutes 要增加的分钟数
     * @return 新的日期
     */
    public static Date addMinutes(Date date, Long plusMinutes) {
        LocalDateTime dateTime = LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
        LocalDateTime newDateTime = dateTime.plusMinutes(plusMinutes);
        return Date.from(newDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }

    /**
     * 增加时间
     *
     * @param date date
     * @param hour 要增加的小时数
     * @return new date
     */
    public static Date addHour(Date date, Long hour) {
        LocalDateTime dateTime = LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
        LocalDateTime localDateTime = dateTime.plusHours(hour);
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }

    /**
     * @return 返回当天的起始时间
     */
    public static Date getStartTime() {

        LocalDateTime now = LocalDateTime.now().withHour(0).withMinute(0).withSecond(0);
        return localDateTime2Date(now);
    }


    /**
     * @return 返回当天的结束时间
     */
    public static Date getEndTime() {
        LocalDateTime now = LocalDateTime.now().withHour(23).withMinute(59).withSecond(59).withNano(999);
        return localDateTime2Date(now);
    }

    /**
     * 减月份
     *
     * @param monthsToSubtract 月份
     * @return Date
     */
    public static Date minusMonths(long monthsToSubtract) {
        LocalDate localDate = LocalDate.now().minusMonths(monthsToSubtract);

        return localDate2Date(localDate);
    }

    /**
     * LocalDate类型转为Date
     *
     * @param localDate LocalDate object
     * @return Date object
     */
    public static Date localDate2Date(LocalDate localDate) {

        ZonedDateTime zonedDateTime = localDate.atStartOfDay(ZoneId.systemDefault());

        return Date.from(zonedDateTime.toInstant());
    }

    /**
     * LocalDateTime类型转为Date
     *
     * @param localDateTime LocalDateTime object
     * @return Date object
     */
    public static Date localDateTime2Date(LocalDateTime localDateTime) {
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }

    /**
     * 查询当前年的第一天
     *
     * @param pattern 格式，默认格式yyyyMMdd
     * @return 20190101
     */
    public static String getFirstDayOfCurrentYear(String pattern) {
        LocalDateTime localDateTime = LocalDateTime.now().withMonth(1).withDayOfMonth(1);
        if (StringUtils.isEmpty(pattern)) {
            pattern = "yyyyMMdd";
        }

        return format(localDateTime2Date(localDateTime), pattern);
    }

    /**
     * 查询前一年最后一个月第一天
     *
     * @param pattern 格式，默认格式yyyyMMdd
     * @return 20190101
     */
    public static String getLastMonthFirstDayOfPreviousYear(String pattern) {
        LocalDateTime localDateTime = LocalDateTime.now().minusYears(1L).withMonth(12).withDayOfMonth(1);

        if (StringUtils.isEmpty(pattern)) {
            pattern = "yyyyMMdd";
        }

        return format(localDateTime2Date(localDateTime), pattern);
    }

    /**
     * 查询前一年最后一个月第一天
     *
     * @param pattern 格式，默认格式yyyyMMdd
     * @return 20190101
     */
    public static String getLastMonthLastDayOfPreviousYear(String pattern) {
        LocalDateTime localDateTime = LocalDateTime.now().minusYears(1L).with(TemporalAdjusters.lastDayOfYear());

        if (StringUtils.isEmpty(pattern)) {
            pattern = "yyyyMMdd";
        }

        return format(localDateTime2Date(localDateTime), pattern);
    }

    /**
     * 获取当前日期
     *
     * @param pattern 格式，默认格式yyyyMMdd
     * @return 20190101
     */
    public static String getCurrentDay(String pattern) {
        LocalDateTime localDateTime = LocalDateTime.now();

        if (StringUtils.isEmpty(pattern)) {
            pattern = "yyyyMMdd";
        }

        return format(localDateTime2Date(localDateTime), pattern);
    }

    public static String getTomorrowDate(){
        // 获取日历
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        SimpleDateFormat sdf = new SimpleDateFormat(DATE);
        return sdf.format(calendar.getTime());
    }

    public static String getTomorrowDate(int day){
        // 获取日历
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.DAY_OF_MONTH, day);
        SimpleDateFormat sdf = new SimpleDateFormat(DATE);
        return sdf.format(calendar.getTime());
    }

    /**
     * 获取具体当前多少前
     * @param pattern 时间格式化
     * @param days 天数
     * @return
     */
    public static String getLastDate(String pattern, Integer days){
        LocalDateTime time = LocalDateTime.now();
        LocalDateTime changeTime = time.plusDays(days);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
        return changeTime.format(formatter);
    }


    public static void main(String[] args) {

        System.out.println((long)(Math.random() * 1000000));
    }

    /**
     * 查询某年或某月或某日有多少分钟
     * 如果是当前年或当前月份或当前天，则算出的是年初或月初或今日零点到现在的分钟数
     *
     * @param dateStr 时间字符串，默认格式yyyy或者yyyy-MM或者yyyy-MM-dd
     * @return 分钟数
     * 返回值为负，代表起始时间大于当前时间
     */
    public static int getMinutesOfDateStr(String dateStr) {

        int minutes = 0;

        Date startDate;
        Calendar from = Calendar.getInstance();
        Calendar to = Calendar.getInstance();

        try {
            if (dateStr.length() == 4) {
                dateStr += "-01-01";
                startDate = new SimpleDateFormat("yyyy-MM-dd").parse(dateStr);
                from.setTime(startDate);
                to.setTime(startDate);
                to.add(Calendar.YEAR, 1);
            } else if (dateStr.length() == 7) {
                dateStr += "-01";
                startDate = new SimpleDateFormat("yyyy-MM-dd").parse(dateStr);
                from.setTime(startDate);
                to.setTime(startDate);
                to.add(Calendar.MONTH, 1);
            } else if (dateStr.length() > 7) {
                startDate = new SimpleDateFormat("yyyy-MM-dd").parse(dateStr);
                from.setTime(startDate);
                to.setTime(startDate);
                to.add(Calendar.DAY_OF_MONTH, 1);
            }

            if (to.getTimeInMillis() > System.currentTimeMillis()) {
                minutes = (int) ((System.currentTimeMillis() - from.getTimeInMillis()) / (60 * 1000));
            } else {
                minutes = (int) ((to.getTimeInMillis() - from.getTimeInMillis()) / (60 * 1000));
            }


        } catch (ParseException e) {
            e.printStackTrace();
        }
        return minutes;
    }

}
