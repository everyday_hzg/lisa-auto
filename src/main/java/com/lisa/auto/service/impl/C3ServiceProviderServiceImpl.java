package com.lisa.auto.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.lisa.auto.entity.C3ExportLog;
import com.lisa.auto.entity.C3ServiceProvider;
import com.lisa.auto.enums.C3UrlEnum;
import com.lisa.auto.mapper.C3ServiceProviderMapper;
import com.lisa.auto.service.C3ExportLogService;
import com.lisa.auto.service.C3ServiceProviderService;
import com.lisa.auto.util.basic.BaseException;
import com.lisa.auto.util.basic.BaseResponse;
import demo.C3ApiDemo;
import demo.request.ServiceProviderInterfaceCreateRequest;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务商接口 服务实现类
 * </p>
 *
 * @author suohao
 * @since 2019-12-23
 */
@Service
public class C3ServiceProviderServiceImpl extends ServiceImpl<C3ServiceProviderMapper, C3ServiceProvider> implements C3ServiceProviderService {

    private static final Logger LOGGER = LoggerFactory.getLogger(C3ServiceProviderServiceImpl.class);

    @Value("${c3.url}")
    private String c3url;
    @Value("${c3.appKey}")
    private String appKey;
    @Value("${c3.secretKey}")
    private String secretKey;

    @Autowired
    private C3ExportLogService exportLogService;

    @Override
    public void registServiceProvider(C3ServiceProvider serviceProvider) {
        if ("".equals(serviceProvider.getInterfaceUrl()) || serviceProvider.getInterfaceUrl()==null){
            throw new BaseException("url不能为空");
        }
        if ("".equals(serviceProvider.getType()) || serviceProvider.getType()==null){
            throw new BaseException("服务商接口类型不能为空");
        }
        //serviceProvider.setSubscribe(1);
        baseMapper.insert(serviceProvider);
        String[] split = serviceProvider.getType().split(",");
        for (String type : split) {
            new Thread(() -> {
                LOGGER.info("注册服务商推送C3开始---------------");
                try {
                    registServiceProviderToC3(serviceProvider,type);
                } catch (Exception e) {
                    LOGGER.error("注册服务商推送C3失败---------------");
                }
                LOGGER.info("注册服务商推送C3结束---------------");
            }).start();
        }

    }

    /**
     * 注册服务商推C3
     * @param
     */
    private void registServiceProviderToC3(C3ServiceProvider c3ServiceProvider,String type){
        C3ExportLog exportLog = new C3ExportLog();
        try {
            String url= c3url+ C3UrlEnum.SERVICEPROVIDERURL.getUrl();
            ServiceProviderInterfaceCreateRequest request = new ServiceProviderInterfaceCreateRequest();
            request.setInterfaceUrl(c3ServiceProvider.getInterfaceUrl());
            request.setSubscribe(c3ServiceProvider.getSubscribe());
            request.setType(type);
            request.setAppKey(appKey);
            String requstJson = JSON.toJSONString(request);
            exportLog.setInterfaceUrl(url);
            exportLog.setExportKey(c3ServiceProvider.getInterfaceUrl());
            exportLog.setExportType("REGIST");
            exportLog.setTargetSys("C3");
            exportLog.setDataContent(requstJson);
            String result = C3ApiDemo.post(url, requstJson, secretKey);
            if (StringUtils.isNotBlank(result)){
                BaseResponse<C3ServiceProvider> baseResponse = JSONObject.parseObject(result, BaseResponse.class);
                if (!baseResponse.isReturnSuccess()){
                    throw new BaseException("注册失败:"+baseResponse.getReturnErrMsg());
                }
                exportLog.setExportStatus("1");
                exportLog.setExportResponse(result);
            }
        }catch (Exception e){
            exportLog.setExportStatus("0");
            exportLog.setExportResponse(e.getMessage());
        }
        exportLogService.insert(exportLog);
    }
}
