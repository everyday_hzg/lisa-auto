package com.lisa.auto.service.impl;

import com.lisa.auto.entity.C3RiskConfiguration;
import com.lisa.auto.mapper.C3RiskConfigurationMapper;
import com.lisa.auto.service.C3RiskConfigurationService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 车辆风险配置 服务实现类
 * </p>
 *
 * @author suohao
 * @since 2020-02-21
 */
@Service
public class C3RiskConfigurationServiceImpl extends ServiceImpl<C3RiskConfigurationMapper, C3RiskConfiguration> implements C3RiskConfigurationService {

}
