package com.lisa.auto.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.lisa.auto.entity.C3FaultConfiguration;
import com.lisa.auto.entity.C3ReportEventInfo;
import com.lisa.auto.entity.C3RiskConfiguration;
import com.lisa.auto.entity.C3RiskFault;
import com.lisa.auto.mapper.C3ReportEventInfoMapper;
import com.lisa.auto.service.C3FaultConfigurationService;
import com.lisa.auto.service.C3ReportEventInfoService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.lisa.auto.service.C3RiskConfigurationService;
import com.lisa.auto.service.C3RiskFaultService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * 上报事件信息 服务实现类
 * </p>
 *
 * @author suohao
 * @since 2019-12-25
 */
@Service
public class C3ReportEventInfoServiceImpl extends ServiceImpl<C3ReportEventInfoMapper, C3ReportEventInfo> implements C3ReportEventInfoService {

    @Autowired
    private C3RiskConfigurationService riskConfigurationService;
    @Autowired
    private C3FaultConfigurationService faultConfigurationService;
    @Autowired
    private C3RiskFaultService riskFaultService;
    @Autowired
    private C3ReportEventInfoService reportEventInfoService;

    /**
     * 1.1.如果上报的事件为  急加速 急转弯  急刹车  查寻当天的次数判断驾驶行为是否是危险
     * 1.2.如果达到配置表的次数，就在记录表中记录，并更新次数
     * 2.1.上报的事件  先去配置表匹配，根据alarmLevel判断是风险还是故障
     * 2.2 根据上报的间隔时间，插入或更新记录表中记录
     * @param map
     */
    @Override
    public void reportEventInfo(Map<String, Object> map) {
        C3ReportEventInfo c3ReportEventInfo = JSON.parseObject(JSON.toJSONString(map), C3ReportEventInfo.class);
        String event = c3ReportEventInfo.getEvent();
        String alarmLevel = c3ReportEventInfo.getAlarmLevel();
        if (!"".equals(event) && event!=null){
            EntityWrapper<C3RiskConfiguration> ew = new EntityWrapper<>();
            //如果上报的事件为  急加速 急转弯  急刹车  查寻当天的次数判断驾驶行为是否是危险
            if ("EVENT007".equals(event) || "EVENT008".equals(event) ||"ALARM20105".equals(event)){
                //查询风险配置表 是否有配置
                ew.eq("event_id",event);
                List<C3RiskConfiguration> c3RiskConfigurations = riskConfigurationService.selectList(ew);
                int count =baseMapper.getEventCount(event,c3ReportEventInfo.getVin());
                if (c3RiskConfigurations!=null && c3RiskConfigurations.size()>0){
                    Integer riskLevel = c3RiskConfigurations.get(0).getRiskLevel();
                    //如果急加速 急转弯  急刹车 达到配置表的规定的次数  就往risk_fault记录表插入一条数据
                    if (count>=(riskLevel-1)){
                    //查询risk_fault记录表当天有没有生成记录
                        EntityWrapper<C3RiskFault> riskFaultWrapper = new EntityWrapper<>();
                        riskFaultWrapper.where("DATE_FORMAT( gmt_create, '%Y-%m-%d' ) = DATE_FORMAT( CURDATE( ) , '%Y-%m-%d' )").eq("vin",c3ReportEventInfo.getVin()).eq("event_id",event);
                        List<C3RiskFault> riskFaults = riskFaultService.selectList(riskFaultWrapper);
                        //若存在就更新  否则就新增
                        if (riskFaults.size()>0 && riskFaults!=null){
                            C3RiskFault riskFault = riskFaults.get(0);
                            EntityWrapper<C3RiskFault> ew1 = new EntityWrapper<>();
                            ew1.eq("id",riskFault.getId());
                            riskFault.setCount(riskFault.getCount()+1);
                            baseMapper.insert(c3ReportEventInfo);
                            riskFaultService.update(riskFault,ew1);
                        }else {
                            C3RiskFault riskFault = new C3RiskFault();
                            riskFault.setEventId(event);
                            riskFault.setVin(c3ReportEventInfo.getVin());
                            riskFault.setVehicleEquipment(c3RiskConfigurations.get(0).getVehicleEquipment());
                            riskFault.setFaultType(c3RiskConfigurations.get(0).getRiskType());
                            riskFault.setEquipmentFaultItem(c3RiskConfigurations.get(0).getEquipmentRiskItem());
                            riskFault.setType("20");
                            riskFault.setCount(count+1);
                            baseMapper.insert(c3ReportEventInfo);
                            riskFaultService.insert(riskFault);
                        }
                    }else {
                        baseMapper.insert(c3ReportEventInfo);
                    }
                }
            } else{
                ew.eq("event_id",event).eq("risk_level",alarmLevel);
                List<C3RiskConfiguration> c3RiskConfigurations = riskConfigurationService.selectList(ew);
                //判断此次上报的事件是否为风险事件  查询一分钟内是否上报过相同的事件
                EntityWrapper<C3ReportEventInfo> eventInfoEntityWrapper = new EntityWrapper<>();
                eventInfoEntityWrapper.eq("vin",c3ReportEventInfo.getVin())
                        .eq("event",event)
                        .eq("alarm_level",alarmLevel);
                List<C3ReportEventInfo> reportEventInfos=baseMapper.getEvent(eventInfoEntityWrapper);
                if (c3RiskConfigurations!=null && c3RiskConfigurations.size()>0){
                    C3RiskConfiguration riskConfiguration = c3RiskConfigurations.get(0);
                    //判断最近一次上报的是否为该风险事件
                    EntityWrapper<C3RiskFault> riskFaultEntityWrapper = new EntityWrapper<>();
                    riskFaultEntityWrapper.eq("vin",c3ReportEventInfo.getVin())
                            .eq("event_id",event)
                            .eq("fault_level",alarmLevel)
                            .orderBy("id",false)
                            .last("limit 0,1");
                    C3RiskFault riskFault=riskFaultService.selectOne(riskFaultEntityWrapper);

                    //查询是否上报过，上报过就更新结束时间，否则就新增
                    if (Objects.nonNull(riskFault) && reportEventInfos.size()>0 ){
                        EntityWrapper<C3RiskFault> ew1 = new EntityWrapper<>();
                        ew1.eq("id",riskFault.getId());
                        riskFault.setEndTime(c3ReportEventInfo.getAlarmTime());
                        baseMapper.insert(c3ReportEventInfo);
                        riskFaultService.update(riskFault,ew1);
                    }else {
                        C3RiskFault riskFault1 = new C3RiskFault();
                        riskFault1.setEventId(event);
                        riskFault1.setVin(c3ReportEventInfo.getVin());
                        riskFault1.setVehicleEquipment(riskConfiguration.getVehicleEquipment());
                        riskFault1.setFaultType(riskConfiguration.getRiskType());
                        riskFault1.setFaultLevel(riskConfiguration.getRiskLevel());
                        riskFault1.setStartTime(c3ReportEventInfo.getAlarmTime());
                        riskFault1.setEndTime(c3ReportEventInfo.getAlarmTime());
                        riskFault1.setEquipmentFaultItem(riskConfiguration.getEquipmentRiskItem());
                        riskFault1.setType("20");
                        baseMapper.insert(c3ReportEventInfo);
                        riskFaultService.insert(riskFault1);
                    }
                }
                //判断此次上报的事件是否为故障
                EntityWrapper<C3FaultConfiguration> faultConfigurationEntityWrapper = new EntityWrapper<>();
                faultConfigurationEntityWrapper.eq("event_id",event).eq("fault_level",alarmLevel);
                List<C3FaultConfiguration> faultConfigurations = faultConfigurationService.selectList(faultConfigurationEntityWrapper);
                if (faultConfigurations!=null && faultConfigurations.size()>0){
                    C3FaultConfiguration faultConfiguration = faultConfigurations.get(0);
                    //判断最近一次上报的是否为该风险事件
                    EntityWrapper<C3RiskFault> riskFaultEntityWrapper1 = new EntityWrapper<>();
                    riskFaultEntityWrapper1.eq("vin",c3ReportEventInfo.getVin())
                            .eq("event_id",event)
                            .eq("fault_level",alarmLevel)
                            .orderBy("id",false)
                            .last("limit 0,1");
                    C3RiskFault riskFault=riskFaultService.selectOne(riskFaultEntityWrapper1);
                    //查询是否上报过，上报过就更新结束时间，否则就新增
                    if (Objects.nonNull(riskFault)  && reportEventInfos.size()>0){
                        EntityWrapper<C3RiskFault> ew1 = new EntityWrapper<>();
                        ew1.eq("id",riskFault.getId());
                        riskFault.setEndTime(c3ReportEventInfo.getAlarmTime());
                        baseMapper.insert(c3ReportEventInfo);
                        riskFaultService.update(riskFault,ew1);
                    }else {
                        C3RiskFault riskFault2 = new C3RiskFault();
                        riskFault2.setEventId(event);
                        riskFault2.setVin(c3ReportEventInfo.getVin());
                        riskFault2.setVehicleEquipment(faultConfiguration.getVehicleEquipment());
                        riskFault2.setFaultType(faultConfiguration.getFaultType());
                        riskFault2.setFaultLevel(faultConfiguration.getFaultLevel());
                        riskFault2.setStartTime(c3ReportEventInfo.getAlarmTime());
                        riskFault2.setEndTime(c3ReportEventInfo.getAlarmTime());
                        riskFault2.setEquipmentFaultItem(faultConfiguration.getEquipmentFaultItem());
                        riskFault2.setType("30");
                        baseMapper.insert(c3ReportEventInfo);
                        riskFaultService.insert(riskFault2);
                    }
                }
            }
        }

    }
}
