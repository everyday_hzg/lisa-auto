package com.lisa.auto.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.lisa.auto.entity.C3CarCondStatistics;
import com.lisa.auto.entity.C3ReportCarInfo;
import com.lisa.auto.mapper.C3CarCondStatisticsMapper;
import com.lisa.auto.service.C3CarCondStatisticsService;
import org.springframework.stereotype.Service;

/**
 * @description:
 * @author: huangzhigang
 * @date: 2020-03-30 14:41
 **/
@Service
public class C3CarCondStatisticsServiceImpl extends ServiceImpl<C3CarCondStatisticsMapper, C3CarCondStatistics> implements C3CarCondStatisticsService {
    @Override
    public double getTotalMiles(EntityWrapper<C3ReportCarInfo> entityWrapper) {
        return baseMapper.getTotalMiles(entityWrapper);
    }
}
