package com.lisa.auto.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.lisa.auto.entity.SevenElevenShop;
import com.lisa.auto.mapper.SevenElevenShopMapper;
import com.lisa.auto.service.ISevenElevenShopService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 711店铺信息 服务实现类
 * </p>
 *
 * @author huangzhigang
 * @since 2019-12-02
 */
@Service
public class SevenElevenShopServiceImpl extends ServiceImpl<SevenElevenShopMapper, SevenElevenShop> implements ISevenElevenShopService {

    @Override
    public void addData(SevenElevenShop sevenElevenShop) {
        baseMapper.insert(sevenElevenShop);
    }
}
