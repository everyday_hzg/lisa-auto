package com.lisa.auto.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.lisa.auto.entity.VehicleArea;
import com.lisa.auto.mapper.VehicleAreaMapper;
import com.lisa.auto.service.VehicleAreaService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 车辆信息 服务实现类
 * </p>
 *
 * @author huangzhigang
 * @since 2019-12-02
 */
@Service
public class VehicleAreaServiceImpl extends ServiceImpl<VehicleAreaMapper, VehicleArea> implements VehicleAreaService {

    private static final Logger LOGGER = LoggerFactory.getLogger(VehicleAreaServiceImpl.class);

    @Override
    public Long insertVehicleArea(VehicleArea vehicleArea) {
        //1.先判断数据库是否存在此地址
        List<VehicleArea> vehicleAreas = baseMapper.queryVehicleArea(vehicleArea);
        //若存在就返回id
        if (vehicleAreas != null && vehicleAreas.size() > 0) {
            VehicleArea vehicleArea1 = vehicleAreas.get(0);
            return vehicleArea1.getId();
        } else {
            //不存在就插入 并返回id
            baseMapper.insert(vehicleArea);
            return vehicleArea.getId();
        }
    }

    @Override
    public List<VehicleArea> getVehicleAreaList(Map<String, Object> condition) {
        EntityWrapper<VehicleArea> ew = new EntityWrapper<>();
        if (condition.get("province") != null && !"".equals(condition.get("province"))) {
            ew.like("province", condition.get("province").toString());
        }
        if (condition.get("city") != null && !"".equals(condition.get("city"))) {
            ew.like("city", condition.get("city").toString());
        }
        if (condition.get("county") != null && !"".equals(condition.get("county"))) {
            ew.like("county", condition.get("county").toString());
        }
        List<VehicleArea> areaList = baseMapper.getVehicleAreaList(ew);
        return areaList;
    }

}
