package com.lisa.auto.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.lisa.auto.entity.GoodsInfo;
import com.lisa.auto.mapper.GoodsInfoMapper;
import com.lisa.auto.service.IGoodsInfoService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品信息 服务实现类
 * </p>
 *
 * @author huangzhigang
 * @since 2019-12-02
 */
@Service
public class GoodsInfoServiceImpl extends ServiceImpl<GoodsInfoMapper, GoodsInfo> implements IGoodsInfoService {

    @Override
    public void addData(GoodsInfo goodsInfo) {
        baseMapper.insert(goodsInfo);
    }
}
