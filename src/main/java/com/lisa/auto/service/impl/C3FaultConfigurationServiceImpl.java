package com.lisa.auto.service.impl;

import com.lisa.auto.entity.C3FaultConfiguration;
import com.lisa.auto.mapper.C3FaultConfigurationMapper;
import com.lisa.auto.service.C3FaultConfigurationService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 车辆故障配置 服务实现类
 * </p>
 *
 * @author suohao
 * @since 2020-02-21
 */
@Service
public class C3FaultConfigurationServiceImpl extends ServiceImpl<C3FaultConfigurationMapper, C3FaultConfiguration> implements C3FaultConfigurationService {

}
