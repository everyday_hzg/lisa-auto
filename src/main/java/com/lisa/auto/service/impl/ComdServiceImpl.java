package com.lisa.auto.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.lisa.auto.entity.ComdPo;
import com.lisa.auto.mapper.ComdMapper;
import com.lisa.auto.service.ComdService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @description:
 * @author: huangzhigang
 * @date: 2020-04-01 17:22
 **/
@Service
public class ComdServiceImpl extends ServiceImpl<ComdMapper, ComdPo> implements ComdService {

    @Override
    public List<ComdPo> selComdList() {
        EntityWrapper<ComdPo> ew = new EntityWrapper<>();
        return baseMapper.selectList(ew);
    }
}
