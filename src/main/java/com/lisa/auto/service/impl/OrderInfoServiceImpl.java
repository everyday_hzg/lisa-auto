package com.lisa.auto.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.lisa.auto.entity.OrderInfo;
import com.lisa.auto.mapper.OrderInfoMapper;
import com.lisa.auto.service.IOrderInfoService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 订单信息 服务实现类
 * </p>
 *
 * @author huangzhigang
 * @since 2019-12-02
 */
@Service
public class OrderInfoServiceImpl extends ServiceImpl<OrderInfoMapper, OrderInfo> implements IOrderInfoService {

    @Override
    public void addData(OrderInfo orderInfo) {
        baseMapper.insert(orderInfo);
    }

}
