package com.lisa.auto.service.impl;

import com.lisa.auto.entity.C3ExportLog;
import com.lisa.auto.mapper.C3ExportLogMapper;
import com.lisa.auto.service.C3ExportLogService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 接口日志 服务实现类
 * </p>
 *
 * @author suohao
 * @since 2019-12-23
 */
@Service
public class C3ExportLogServiceImpl extends ServiceImpl<C3ExportLogMapper, C3ExportLog> implements C3ExportLogService {

}
