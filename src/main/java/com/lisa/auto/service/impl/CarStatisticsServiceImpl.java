package com.lisa.auto.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.lisa.auto.entity.statistics.VehiclesPo;
import com.lisa.auto.mapper.c3.CarStatisticsMapper;
import com.lisa.auto.service.CarStatisticsService;
import com.lisa.auto.util.DateUtil;
import com.lisa.json.JSONUtil;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.xml.crypto.Data;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author huangzhigang 2020/2/26
 */
@Service
public class CarStatisticsServiceImpl extends ServiceImpl<CarStatisticsMapper, VehiclesPo> implements CarStatisticsService {

    static final Logger LOGGER = LoggerFactory.getLogger(CarStatisticsServiceImpl.class);

    /**
     * 检索查询车辆信息
     * @param condition 检索条件
     * @return List<VehiclesPo>
     */
    @Override
    public List<VehiclesPo> searchVehiclesInfo(Map<String, String> condition) {
        try {
            List<VehiclesPo> result = new ArrayList<>();
            List<VehiclesPo> resultList = baseMapper.searchVehiclesInfo(condition);
            if (!CollectionUtils.isEmpty(resultList)){
                result.addAll(resultList);
            }
            List<VehiclesPo> unusualList = baseMapper.selVehiclesUnusuals(condition);
            if (!CollectionUtils.isEmpty(unusualList)){
                LOGGER.info(JSONUtil.toJsonStr(unusualList));
                Map<String, List<VehiclesPo>> collect = unusualList.stream().collect(Collectors.groupingBy(VehiclesPo::getVin));
                if (!CollectionUtils.isEmpty(collect.keySet())){
                    for (String vin : collect.keySet()){
                        List<VehiclesPo> vehiclesList = collect.get(vin);
                        if (CollectionUtils.isEmpty(vehiclesList)){
                            continue;
                        }
                        Date minTime = null, maxTime = null;
                        /*按照开始时间分组*/
                        List<VehiclesPo> filterList0 = vehiclesList.stream().filter(t -> t.getStartTime()!=null).collect(Collectors.toList());
                        /*按照结束时间分组*/
                        List<VehiclesPo> filterList1 = vehiclesList.stream().filter(t -> t.getEndTime()!=null).collect(Collectors.toList());
                        if (!CollectionUtils.isEmpty(filterList0)){
                            VehiclesPo minVehicles = filterList0.stream().min(Comparator.comparing(VehiclesPo::getStartTime)).get();
                            minTime = minVehicles.getStartTime();
                        }
                        if (!CollectionUtils.isEmpty(filterList1)){
                            VehiclesPo maxVehicles = filterList1.stream().max(Comparator.comparing(VehiclesPo::getStartTime)).get();
                            maxTime = maxVehicles.getEndTime();
                        }
                        if (minTime != null){
                            LOGGER.info("最小时间："+ DateUtil.format(minTime, DateUtil.DATE_TIME));
                        }
                        if (maxTime != null){
                            LOGGER.info("最大时间："+ DateUtil.format(maxTime, DateUtil.DATE_TIME));
                        }
                        for (VehiclesPo vehicles : vehiclesList){
                            Date startTime = vehicles.getStartTime();
                            Date endTime = vehicles.getEndTime();
                            if (startTime!=null && startTime.getTime()<minTime.getTime() && endTime!=null && endTime.getTime()>maxTime.getTime() ){
                                minTime = startTime;
                                maxTime = endTime;
                            }
                            if (startTime!=null && startTime.getTime()<minTime.getTime()){
                                minTime = startTime;
                            }
                            if (endTime!=null && endTime.getTime()<maxTime.getTime() ){
                                maxTime = endTime;
                            }
                        }
                        Map<String, String> paramMap = new HashMap<>();
                        paramMap.put("fleet", condition.get("fleet"));
                        paramMap.put("vin", vin);
                        if (minTime != null){
                            paramMap.put("type", "2");
                            paramMap.put("dateTime", DateUtil.format(minTime, DateUtil.DATE_TIME));
                            List<VehiclesPo> dataList = baseMapper.selNormalVehiclesInfo(paramMap);
                            result.addAll(dataList);
                        }
                        if (maxTime != null){
                            paramMap.put("type", "1");
                            paramMap.put("dateTime", DateUtil.format(maxTime, DateUtil.DATE_TIME));
                            List<VehiclesPo> dataList = baseMapper.selNormalVehiclesInfo(paramMap);
                            result.addAll(dataList);
                        }
                    }
                }
            }
            String state = condition.get("state");
            if (StringUtils.isBlank(state)){
                return result;
            }
            if (StringUtils.isNotBlank(state)){
                return result.stream().filter(t -> state.equals(String.valueOf(t.getState()))).collect(Collectors.toList());
            }
        }catch (Exception e){
            LOGGER.info(e.getMessage(), e);
        }
        return new ArrayList<>();
    }

    @Override
    public Map<String,Object> getTabDetails(Map<String, String> condition) {
        if (!StringUtils.isNotBlank(condition.get("state"))){
            return null;
        }
        if ( "10".equals(condition.get("state"))){
            Map<String,Object> resultMap = baseMapper.getNomalTabDetails(condition);
            return resultMap;
        }
        if ("30".equals(condition.get("state"))|| "20".equals(condition.get("state"))){
            Map<String,Object> resultMap = baseMapper.getTabDetails(condition);
            return resultMap;
        }
        return null;
    }

}
