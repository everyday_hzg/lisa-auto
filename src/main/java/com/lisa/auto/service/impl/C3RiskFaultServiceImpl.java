package com.lisa.auto.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.lisa.auto.entity.C3RiskFault;
import com.lisa.auto.mapper.C3RiskFaultMapper;
import com.lisa.auto.service.C3RiskFaultService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 风险故障记录表 服务实现类
 * </p>
 *
 * @author suohao
 * @since 2020-02-21
 */
@Service
public class C3RiskFaultServiceImpl extends ServiceImpl<C3RiskFaultMapper, C3RiskFault> implements C3RiskFaultService {

    @Override
    public C3RiskFault selectRiskFault(EntityWrapper<C3RiskFault> riskFaultEntityWrapper) {
        return baseMapper.selectRiskFault(riskFaultEntityWrapper);
    }

    @Override
    public String   selectTypeByVin(String vin) {
        return baseMapper.selectTypeByVin(vin);
    }

    /**
     * 根据车架号查询风险及故障数据
     * @param vin 车架号
     * @return C3RiskFault
     */
    public C3RiskFault selectRiskFault(String vin){
        C3RiskFault c3RiskFault = new C3RiskFault(vin);
        EntityWrapper<C3RiskFault> ew = new EntityWrapper<>();
        ew.eq("vin",vin);
        List<C3RiskFault> c3RiskFaults = baseMapper.selectList(ew);
        if (CollectionUtils.isEmpty(c3RiskFaults) || c3RiskFaults.size()<1){
            return null;
        }
        return c3RiskFaults.get(0);
    }

}
