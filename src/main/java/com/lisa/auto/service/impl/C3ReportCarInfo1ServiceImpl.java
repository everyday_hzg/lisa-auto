package com.lisa.auto.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.lisa.auto.entity.*;
import com.lisa.auto.mapper.C3ReportCarInfo1Mapper;
import com.lisa.auto.service.*;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 上报车况信息 服务实现类
 * </p>
 *
 * @author suohao
 * @since 2019-12-25
 */
@Service
public class C3ReportCarInfo1ServiceImpl extends ServiceImpl<C3ReportCarInfo1Mapper, C3ReportCar1Info> implements C3ReportCarInfo1Service {

    @Override
    public void add(C3ReportCar1Info c3ReportCar1Info) {
        baseMapper.insert(c3ReportCar1Info);
    }

    /**
     * 删除历史数据
     */
    @Override
    public void delHistoryData(){
        baseMapper.delHistoryData();
    }

}
