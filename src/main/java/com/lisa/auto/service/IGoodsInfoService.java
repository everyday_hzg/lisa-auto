package com.lisa.auto.service;

import com.baomidou.mybatisplus.service.IService;
import com.lisa.auto.entity.GoodsInfo;

/**
 * <p>
 * 商品信息 服务类
 * </p>
 *
 * @author huangzhigang
 * @since 2019-12-02
 */
public interface IGoodsInfoService extends IService<GoodsInfo> {

    /**
     * 添加商品信息
     * @param goodsInfo 商品信息
     */
    void addData(GoodsInfo goodsInfo);

}
