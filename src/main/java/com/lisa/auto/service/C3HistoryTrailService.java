package com.lisa.auto.service;

import com.lisa.auto.entity.C3HistoryTrail;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;

/**
 * <p>
 * 车辆历史轨迹信息 服务类
 * </p>
 *
 * @author suohao
 * @since 2019-12-20
 */
public interface C3HistoryTrailService extends IService<C3HistoryTrail> {

    List<C3HistoryTrail> getHistoryTrailList(String vin);

    List<C3HistoryTrail> getHistoryTrailListC3(String vin,Long startTime,Long endTime);

    void historyTrailToRedis();
}
