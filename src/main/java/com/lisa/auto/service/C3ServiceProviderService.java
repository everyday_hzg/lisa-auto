package com.lisa.auto.service;

import com.lisa.auto.entity.C3ServiceProvider;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 服务商接口 服务类
 * </p>
 *
 * @author suohao
 * @since 2019-12-23
 */
public interface C3ServiceProviderService extends IService<C3ServiceProvider> {

    void registServiceProvider(C3ServiceProvider serviceProvider);
}
