package com.lisa.auto.service;

import com.baomidou.mybatisplus.service.IService;
import com.lisa.auto.entity.AreaCountDto;
import com.lisa.auto.entity.VehicleArea;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 车况信息 服务类
 * </p>
 *
 * @author suohao
 * @since 2019-12-20
 */
public interface VehicleAreaService extends IService<VehicleArea> {

    Long insertVehicleArea(VehicleArea vehicleArea);

    List<VehicleArea> getVehicleAreaList(Map<String, Object> condition);

}
