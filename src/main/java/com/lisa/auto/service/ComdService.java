package com.lisa.auto.service;

import com.baomidou.mybatisplus.service.IService;
import com.lisa.auto.entity.ComdPo;

import java.util.List;

/**
 * @description:
 * @author: huangzhigang
 * @date: 2020-04-01 17:18
 **/
public interface ComdService extends IService<ComdPo> {

    /**
     * 查询所有数据
     * @return List<ComdPo>
     */
    List<ComdPo> selComdList();
}
