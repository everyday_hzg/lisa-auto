package com.lisa.auto.service;

import com.lisa.auto.entity.C3RiskConfiguration;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 车辆风险配置 服务类
 * </p>
 *
 * @author suohao
 * @since 2020-02-21
 */
public interface C3RiskConfigurationService extends IService<C3RiskConfiguration> {

}
