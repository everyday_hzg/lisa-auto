package com.lisa.auto.service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.IService;
import com.lisa.auto.entity.C3CarCondStatistics;
import com.lisa.auto.entity.C3ReportCarInfo;

/**
 * @description:
 * @author: huangzhigang
 * @date: 2020-03-30 14:40
 **/
public interface C3CarCondStatisticsService extends IService<C3CarCondStatistics> {
    double getTotalMiles(EntityWrapper<C3ReportCarInfo> entityWrapper);
}
