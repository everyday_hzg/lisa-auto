package com.lisa.auto.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.lisa.auto.entity.Vehicle;
import com.lisa.auto.entity.VehicleDto;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import com.lisa.auto.entity.VehicleModelConfigureDto;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 车辆信息 服务类
 * </p>
 *
 * @author huangzhigang
 * @since 2019-12-02
 */
public interface IVehicleService extends IService<Vehicle> {

    /**
     * 添加车辆信息
     *
     * @param vehicleDto 车辆信息
     */
    void createVehicles(VehicleDto vehicleDto);

    Page<Vehicle> getVehiclesList(Page<Vehicle> page);

    void updateVehicles(Vehicle vehicle);

    void deleteVehicles(Vehicle vehicle);

    void updateStatusVehicles(String vin, String status);

    /**
     * 禁用车辆(支持批量)
     *
     * @param ids
     */
    void forbiddenByIds(String ids);

    /**
     * 启用车辆（支持批量）
     *
     * @param ids
     */
    void enableByIds(String ids);

    /**
     * 删除车辆(支持批量)
     *
     * @param ids
     */
    void deleteByIds(String ids);

    void importVehicleExcel(MultipartFile file, HttpServletResponse response);
    /**
     * 获取车辆详细数据(新版)
     * @param page
     * @return
     */
    Page<VehicleModelConfigureDto> getVehiclesListNew(Page<VehicleModelConfigureDto> page);

    List<String> getVehiclesFleet(String fleetName);

    Object vehiclesToC3(Vehicle vehicle);

    List<String> getPlateList(Map<String,String> map);
}
