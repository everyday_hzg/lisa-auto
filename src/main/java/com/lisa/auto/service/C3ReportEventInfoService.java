package com.lisa.auto.service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.lisa.auto.entity.C3ReportEventInfo;
import com.baomidou.mybatisplus.service.IService;

import java.util.Map;

/**
 * <p>
 * 上报事件信息 服务类
 * </p>
 *
 * @author suohao
 * @since 2019-12-25
 */
public interface C3ReportEventInfoService extends IService<C3ReportEventInfo> {

    void reportEventInfo(Map<String, Object> map);
}
