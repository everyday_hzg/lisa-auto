package com.lisa.auto.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.lisa.auto.entity.*;
import com.lisa.auto.entity.statistics.VehiclesStatisticsPo;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 上报车况信息 服务类
 * </p>
 *
 * @author suohao
 * @since 2019-12-25
 */
public interface C3ReportCarFaultService extends IService<C3ReportCarFault> {
    /**
     * 车辆故障上报
     * @param map
     */
    void reportCarFault(Map<String, Object> map);

    /**
     * 车辆故障上报
     * @param faultList
     */
    void reportCarFault(List<C3ReportCarFault> faultList);

}
