package com.lisa.auto.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.lisa.auto.entity.*;
import com.baomidou.mybatisplus.service.IService;
import com.lisa.auto.entity.statistics.VehiclesStatisticsPo;

import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 上报车况信息 服务类
 * </p>
 *
 * @author suohao
 * @since 2019-12-25
 */
public interface C3ReportCarInfoService extends IService<C3ReportCarInfo> {

    /**
     * 车况上报
     * @param map
     */
    void reportCarInfo(Map<String, Object> map);

    List<C3ReportCarInfo> selCarCond(String vin);

    /**
     * 获取车辆当前车况信息
     *
     * @param condition
     * @return
     */
    List<C3ReportCarInfoVo> getCarCurrentInfoList(Map<String, Object> condition);

    /**
     * 获取车辆当前经纬度信息
     *
     * @param condition
     * @return
     */
    List<C3ReportCarInfoVo> getCarCurrentLocationList(Map<String, Object> condition);

    /**
     * 获取某市当天车辆车况详情列表
     *
     * @param condition
     * @return
     */
    List<C3ReportCarInfoDto> getCityCarInfoList(Map<String, Object> condition);

    /**
     * 获取车况统计信息
     *
     * @param condition
     * @return
     */
    Map<String, Object> getCarInfoTotal(Map<String, Object> condition);

    /**
     * 获取省/市/区县当前车辆数量
     *
     * @param areaCountDto
     * @return
     */
    List<AreaCountDto> getAreaVehicleCount(AreaCountDto areaCountDto);

    /**
     * 获取车辆运行路线轨迹
     *
     * @param condition
     * @return
     */
    List<C3ReportCarInfoDto> getCarInfoRoute(Map<String, Object> condition);

    List<MilesReportDto> getTotalFleetMiles(Map<String, Object> condition);

    List<Map<String, Object>> getRiskFaultRate(Map<String, Object> condition) throws ParseException;

    Page<Map<String, Object>> getRiskRateForVin(Page<Map<String, Object>> page);

    /**
     * 查询车队公司统计数据
     * @author huangzhigang 2020-02-25 10:45:11
     * @param condition 查询条件集
     * @return List<VehiclesStatisticsPo>
     */
    List<VehiclesStatisticsPo> selVehiclesStatistics(Map<String, String> condition);

    /**
     * 车队公司及车辆汇总数据
     * @param vin 车架号
     * @return List<VehiclesStatisticsPo>
     */
    VehiclesStatisticsPo selVehiclesStatisticsList(String vin);

    boolean reportCarHandle(List<C3ReportCarInfo> data);

}
