package com.lisa.auto.service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.lisa.auto.entity.C3RiskFault;
import com.baomidou.mybatisplus.service.IService;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 风险故障记录表 服务类
 * </p>
 *
 * @author suohao
 * @since 2020-02-21
 */
public interface C3RiskFaultService extends IService<C3RiskFault> {

    C3RiskFault selectRiskFault( EntityWrapper<C3RiskFault> riskFaultEntityWrapper);

    String   selectTypeByVin(String vin);

    /**
     * 根据车架号查询风险及故障数据
     * @param vin 车架号
     * @return C3RiskFault
     */
    C3RiskFault selectRiskFault(String vin);
}
