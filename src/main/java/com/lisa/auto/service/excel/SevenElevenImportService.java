package com.lisa.auto.service.excel;

import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

/**
 * 711转换工具Service
 * @author huangzhigang 2019/12/2
 */
public interface SevenElevenImportService {

    void transVehicleExcel(MultipartFile orderFile, MultipartFile vehicleFile, HttpServletResponse response);

	/**
     * excel 导入
     * @param file
     * @param response
     */
    void importExcel(MultipartFile file, HttpServletResponse response);

}
