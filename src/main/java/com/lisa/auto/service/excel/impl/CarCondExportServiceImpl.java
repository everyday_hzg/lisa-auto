package com.lisa.auto.service.excel.impl;

import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import cn.afterturn.easypoi.excel.entity.enmus.ExcelType;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.lisa.auto.entity.C3ReportCarInfo;
import com.lisa.auto.entity.C3ReportCarInfoVo;
import com.lisa.auto.entity.Vehicle;
import com.lisa.auto.entity.excel.CarCondExportPo;
import com.lisa.auto.entity.excel.CarCondPo;
import com.lisa.auto.mapper.CarCondExportMapper;
import com.lisa.auto.service.C3ReportCarInfoService;
import com.lisa.auto.service.IVehicleService;
import com.lisa.auto.service.excel.CarCondExportService;
import com.lisa.auto.util.DateUtil;
import com.lisa.auto.util.ExcelStyleUtil;
import com.lisa.auto.util.ExcelUtils;
import com.lisa.redis.JedisClusterUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.servlet.http.HttpServletResponse;
import java.util.*;

/**
 * @description:
 * @author: huangzhigang
 * @date: 2020-05-06 16:54
 **/
@Service
public class CarCondExportServiceImpl extends ServiceImpl<CarCondExportMapper, CarCondPo> implements CarCondExportService {

    private static final String CAR_COND_KEY_ = "CAR_COND_KEY";

    static final Logger logger = LoggerFactory.getLogger(CarCondExportServiceImpl.class);

    static String TITLE_ = "title";

    static String ENTITY_ = "entity";

    static String DATA_ = "data";

    @Autowired
    private IVehicleService vehicleService;

    @Autowired
    private C3ReportCarInfoService c3ReportCarInfoService;

    @Override
    public void export() {
        List<Vehicle> vehicleList = vehicleService.selectList(new EntityWrapper<>());
        if (CollectionUtils.isEmpty(vehicleList)) {
            return;
        }
        try {
            List<Map<String, Object>> result = new ArrayList<>();
            for (Vehicle vehicle : vehicleList) {
                /*车架号*/
                String vin = vehicle.getVin();
                /*车牌号*/
                String plateLicenseNo = vehicle.getPlateLicenseNo();
                if (StringUtils.isBlank(vehicle.getVin())) {
                    continue;
                }
                List<C3ReportCarInfo> carInfoVoList = c3ReportCarInfoService.selCarCond(vin);
                if (CollectionUtils.isEmpty(carInfoVoList)) {
                    continue;
                }
                List<CarCondExportPo> dataList = new ArrayList<>();
                for (C3ReportCarInfo c3ReportCarInfoVo : carInfoVoList) {
                    CarCondExportPo carCondExport = new CarCondExportPo(vehicle.getFleet(), vin, c3ReportCarInfoVo.getLatitude(), c3ReportCarInfoVo.getLongitude(),
                            c3ReportCarInfoVo.getVehicleSpeed(), c3ReportCarInfoVo.getAcquisitionTime(), vehicle.getPlateLicenseNo());
                    dataList.add(carCondExport);
                }
                Map<String, Object> sheetMap = new HashMap<>(3);
                ExportParams exportParams = new ExportParams();
                exportParams.setSheetName(plateLicenseNo);
                sheetMap.put(TITLE_, exportParams);
                sheetMap.put(ENTITY_, CarCondExportPo.class);
                exportParams.setStyle(ExcelStyleUtil.class);
                sheetMap.put(DATA_, dataList);
                result.add(sheetMap);
            }
            JedisClusterUtil.hSet(CAR_COND_KEY_, DateUtil.format(DateUtil.DATE_YYYY_MM_DD), JSONArray.toJSONString(result));
        } catch (Exception e) {
            logger.info(e.getMessage(), e);
        }
    }

    @Override
    public List<CarCondPo> selList() {
        EntityWrapper<CarCondPo> ew = new EntityWrapper();
        return baseMapper.selectList(ew);
    }

    @Override
    public void download(String dateTime, HttpServletResponse response) {
        try {
            String json = JedisClusterUtil.hGet(CAR_COND_KEY_, dateTime);
            if (StringUtils.isNotBlank(json)){
                List<Map<String, Object>> result = new ArrayList<>();
                List<Object> list = JSON.parseArray(json);
                for (Object object : list){
                    Map <String,Object> ret = (Map<String, Object>) object;
                    result.add(ret);
                }
                Workbook workbook = ExcelExportUtil.exportExcel(result, ExcelType.HSSF);
                ExcelUtils.download(workbook, dateTime, response);
            }
        }catch (Exception e){
            logger.info(e.getMessage(), e);
        }
    }


}
