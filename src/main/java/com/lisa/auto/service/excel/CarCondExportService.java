package com.lisa.auto.service.excel;

import com.lisa.auto.entity.excel.CarCondPo;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @description:
 * @author: huangzhigang
 * @date: 2020-05-06 16:54
 **/
public interface CarCondExportService {

    /**
     * 车况数据导出
     */
    void export();

    List<CarCondPo> selList();

    void download(String dateTime, HttpServletResponse response);

}
