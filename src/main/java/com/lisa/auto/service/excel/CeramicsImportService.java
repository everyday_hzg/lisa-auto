package com.lisa.auto.service.excel;

import javax.servlet.http.HttpServletResponse;

import com.lisa.auto.entity.ceramics.ResultPo;
import com.lisa.auto.entity.excel.OrderTmplPo;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.List;

/**
 * 陶瓷转换工具Service
 * @author huangzhigang 20200113
 */
public interface CeramicsImportService {

	/**
	 * 陶瓷转换工具
	 * @param fileList 订单/原始单
	 * @param response
	 */
	List<ResultPo> importExcel(List<MultipartFile> fileList, HttpServletResponse response);

	/**
	 * 解析城配车次Excel
	 * @param baseFile 订单/原始单
	 * @param loadFile 车次列表
	 * @param response 
	 */
	void importLoadExcel(MultipartFile baseFile, MultipartFile loadFile, HttpServletResponse response);

	
}
