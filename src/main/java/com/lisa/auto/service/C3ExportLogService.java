package com.lisa.auto.service;

import com.lisa.auto.entity.C3ExportLog;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 接口日志 服务类
 * </p>
 *
 * @author suohao
 * @since 2019-12-23
 */
public interface C3ExportLogService extends IService<C3ExportLog> {

}
