package com.lisa.auto.service;

import com.baomidou.mybatisplus.service.IService;
import com.lisa.auto.entity.SevenElevenShop;

/**
 * <p>
 * 711店铺信息 服务类
 * </p>
 *
 * @author huangzhigang
 * @since 2019-12-02
 */
public interface ISevenElevenShopService extends IService<SevenElevenShop> {

    /**
     * 添加711店铺信息
     * @param sevenElevenShop 711店铺信息
     */
    void addData(SevenElevenShop sevenElevenShop);

}
