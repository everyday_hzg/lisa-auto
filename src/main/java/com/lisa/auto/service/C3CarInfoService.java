package com.lisa.auto.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.lisa.auto.entity.C3CarInfo;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 车况信息 服务类
 * </p>
 *
 * @author suohao
 * @since 2019-12-20
 */
public interface C3CarInfoService extends IService<C3CarInfo> {

    //C3CarInfo getCarInfo(String vin);
    void getCarInfoToC3(String vin);

    Page<Map<String, Object>> getVehicleList(Page<Map<String, Object>> page);

    List<Map<String, Object>> getTotalDistance(String vin, String startTime, String endTime);

    List<Map<String, Object>> getVehicleSpeed(String vin, String startTime, String endTime);
}
