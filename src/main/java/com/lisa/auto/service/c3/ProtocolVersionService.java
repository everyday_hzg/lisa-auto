package com.lisa.auto.service.c3;

import com.baomidou.mybatisplus.service.IService;
import com.lisa.auto.entity.c3.model.ProtocolVersion;

import java.util.List;

/**
 * <p>
 * 协议版本信息 服务类
 * </p>
 *
 * @author hxh
 * @since 2019-12-24
 */
public interface ProtocolVersionService extends IService<ProtocolVersion> {

    /**
     * 外部接口获取设备协议列表数据并存储到数据库
     * @return
     */
    List<ProtocolVersion> queryListToSave();

    void addProtocolVersion(ProtocolVersion protocolVersion);
}
