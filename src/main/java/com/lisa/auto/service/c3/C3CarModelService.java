package com.lisa.auto.service.c3;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.lisa.auto.entity.c3.model.CarModel;
import com.lisa.auto.entity.c3.model.CarSeries;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 车型管理 服务类
 * </p>
 *
 * @author suohao
 * @since 2019-12-23
 */
public interface C3CarModelService extends IService<CarModel> {

    void addModel(CarModel carModel);

    /**
     * 新增车型（批量）
     * @param carSeries
     */
    void modelUpdate(CarSeries carSeries);

    /**
     * 获取车型列表
     * @param condition
     * @return
     */
    List<CarModel> getModelsList(Map<String, Object> condition);

    /**
     * 获取车型列表（分页）
     * @param page
     * @return
     */
    Page<CarModel> getModelsListPage(Page<CarModel> page);

    void updateCarModel(CarModel carModel);

    List<CarModel> queryModelsToSave(String modelNo);

    List<CarModel> getAllCarModel();

    /**
     * 禁用车型(支持批量)
     *
     * @param ids
     */
    void forbiddenByIds(String ids);

    /**
     * 启用车型（支持批量）
     * @param ids
     */
    void enableByIds(String ids);

    /**
     * 删除车型(支持批量)
     *
     * @param ids
     */
    void deleteByIds(String ids);
}
