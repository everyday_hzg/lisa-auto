package com.lisa.auto.service.c3;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.lisa.auto.entity.c3.model.Device;

import java.util.Map;

/**
 * <p>
 * 设备信息表 服务类
 * </p>
 *
 * @author hxh
 * @since 2019-12-24
 */
public interface DeviceService extends IService<Device> {

    /**
     * 取得设备分页数据
     * @param page
     * @return
     */
    Page<Device> getDevicesList(Page<Device> page);

    /**
     * 新增设备
     *
     * @param device
     */
    void addDevice(Device device);

    /**
     * 新建设备推C3
     *
     * @param device
     */
    void createDeviceToC3(Device device);

    /**
     * 取得设备详情
     */
    Device getDeviceDetailFromC3(String no);

    /**
     * 更新设备推C3
     *
     * @param device
     */
    void updateDeviceToC3(Device device);

    /**
     * 更新设备
     *
     * @param device
     */
    void updateDevice(Device device);

    /**
     * 删除设备推C3
     *
     * @param nos 设备编号，批量删除，以逗号(,)隔开
     */
    void deleteDeviceToC3(String nos);

    /**
     * 删除设备
     *
     * @param nos 设备编号，批量删除，以逗号(,)隔开
     */
    void deleteDevice(String nos);

    /**
     * 绑定设备
     *
     * @param map noList 需要绑定的设备编号
     *            vin 车架号
     */
    void bindingDevice(Map<String, Object> map);

}
