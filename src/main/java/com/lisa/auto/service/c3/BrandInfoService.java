package com.lisa.auto.service.c3;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.lisa.auto.entity.c3.model.BrandInfo;
import com.lisa.auto.entity.c3.model.Maker;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 品牌管理信息 服务类
 * </p>
 *
 * @author hxh
 * @since 2019-12-20
 */
public interface BrandInfoService extends IService<BrandInfo> {

    /**
     * 外部接口获取品牌列表数据并存储到数据库
     *
     * @param makerNo 制造商编码
     */
    List<BrandInfo> queryBrandsToSave(String makerNo);

    /**
     * 获取品牌列表
     *
     * @param condition
     * @return
     */
    List<BrandInfo> getBrandsList(Map<String, Object> condition);

    /**
     * 获取品牌列表（分页）
     *
     * @param page
     * @return
     */
    Page<BrandInfo> getBrandsListPage(Page<BrandInfo> page);

    /**
     * 添加品牌信息
     *
     * @param brandInfo 品牌信息
     */
    void addBrand(BrandInfo brandInfo);

    /**
     * 批量编辑同一个制造商下的品牌信息
     * @param maker
     */
    void brandInfoUpdate(Maker maker);

    /**
     * 删除品牌信息
     *
     * @param id 品牌id
     */
    void deleteByIds(String id);

    /**
     * 禁用品牌信息
     *
     * @param id 品牌id
     */
    void forbiddenByIds(String id);

    /**
     * 启用品牌
     * @param ids
     */
    void enableByIds(String ids);

}
