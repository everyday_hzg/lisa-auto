package com.lisa.auto.service.c3;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.lisa.auto.entity.c3.model.Maker;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 制造商信息 服务类
 * </p>
 *
 * @author hxh
 * @since 2019-12-20
 */
public interface MakerService extends IService<Maker> {

    /**
     * 外部接口获取制造商列表数据并存储到数据库
     */
    List<Maker> queryMakersToSave();

    /**
     * 获取制造商列表
     *
     * @param condition
     * @return
     */
    List<Maker> getMakersList(Map<String, Object> condition);

    /**
     * 获取制造商列表（分页）
     *
     * @param page
     * @return
     */
    Page<Maker> getMakersListPage(Page<Maker> page);

    /**
     * 添加制造商信息
     *
     * @param maker 制造商信息
     */
    void addMaker(Maker maker);

    /**
     * 删除制造商信息
     *
     * @param id 制造商id
     */
    void deleteById(Long id);

    /**
     * 禁用制造商信息
     *
     * @param id 制造商id
     */
    void forbiddenByIds(String id);

    /**
     * 启用制造商
     * @param ids
     */
    void enableByIds(String ids);

    /**
     * 获取制造商品牌分页列表数据
     * @param page
     * @return
     */
    Page<Maker> getMakerAndBrandList(Page<Maker> page);
}
