package com.lisa.auto.service.c3.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.lisa.auto.entity.c3.model.ProtocolVersion;
import com.lisa.auto.entity.c3.response.DeviceProtocolVersion;
import com.lisa.auto.enums.C3UrlEnum;
import com.lisa.auto.mapper.c3.ProtocolVersionMapper;
import com.lisa.auto.service.c3.ProtocolVersionService;
import com.lisa.auto.util.basic.BaseException;
import demo.C3ApiDemo;
import demo.request.DemoSignRequest;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 协议版本信息 服务实现类
 * </p>
 *
 * @author hxh
 * @since 2019-12-24
 */
@Service
public class ProtocolVersionServiceImpl extends ServiceImpl<ProtocolVersionMapper, ProtocolVersion> implements ProtocolVersionService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProtocolVersionServiceImpl.class);

    @Value("${c3.url}")
    private String c3url;
    @Value("${c3.appKey}")
    private String appKey;
    @Value("${c3.secretKey}")
    private String secretKey;

    @Override
    public List<ProtocolVersion> queryListToSave() {
        try {
            String url = c3url + C3UrlEnum.PROTOCOLVERSIONSURL.getUrl();
            DemoSignRequest request = new DemoSignRequest();
            request.setAppKey(appKey);
            String requstJson = JSON.toJSONString(request);
            String result = C3ApiDemo.get(url, requstJson, secretKey);
            if (StringUtils.isNotBlank(result)) {
                DeviceProtocolVersion response = JSONObject.parseObject(result, DeviceProtocolVersion.class);
                if (!response.isReturnSuccess()) {
                    throw new BaseException("获取设备协议失败:" + response.getReturnErrMsg());
                }
                List<ProtocolVersion> versionList = response.getVersionList();
//                if (versionList != null && !versionList.isEmpty()) {
//                    baseMapper.delete(new EntityWrapper<>());
//                    insertBatch(versionList);
//                }
                return versionList;
            }
        } catch (Exception e) {
            LOGGER.error("ProtocolVersionServiceImpl.queryListToSave ERROR {}", e.getMessage());
            throw new BaseException(e.getMessage());
        }
        return null;
    }

    @Override
    public void addProtocolVersion(ProtocolVersion protocolVersion) {
        if (StringUtils.isEmpty(protocolVersion.getType())) {
            throw new BaseException("协议版本类型为空");
        }
        if (StringUtils.isEmpty(protocolVersion.getVersion())) {
            throw new BaseException("协议版本号为空");
        }
        //查询该车架号是否已存在，存在就更新，不存在就新增
        EntityWrapper<ProtocolVersion> ew = new EntityWrapper<>();
        ew.eq("type", protocolVersion.getType());
        List<ProtocolVersion> versions = baseMapper.selectList(ew);
        if (versions != null && !versions.isEmpty()) {
            baseMapper.update(protocolVersion, ew);
        } else {
            baseMapper.insert(protocolVersion);
        }
    }

}
