package com.lisa.auto.service.c3;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.lisa.auto.entity.c3.model.CarSeries;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 车系管理 服务类
 * </p>
 *
 * @author hxh
 * @since 2019-12-20
 */
public interface CarSeriesService extends IService<CarSeries> {

    /**
     * 外部接口获取车系列表数据并存储到数据库
     *
     * @param brandNo 品牌编码
     */
    List<CarSeries> queryCarSeriesToSave(String brandNo);

    /**
     * 获取车系列表
     *
     * @param condition
     * @return
     */
    List<CarSeries> getCarSeriesList(Map<String, Object> condition);

    /**
     * 获取车系列表（分页）
     *
     * @param page
     * @return
     */
    Page<CarSeries> getCarSeriesListPage(Page<CarSeries> page);

    /**
     * 添加品牌信息
     *
     * @param varSeries 车系信息
     */
    void addCarSeries(CarSeries varSeries);

    /**
     * 删除品牌信息
     *
     * @param id 品牌id
     */
    void deleteById(Long id);

    /**
     * 获取车型车系分页列表
     * @param page
     * @return
     */
    Page<CarSeries> getSeriesAndModelList(Page<CarSeries> page);

    /**
     * 禁用制造商信息
     *
     * @param id 制造商id
     */
    void forbiddenByIds(String id);

    /**
     * 启用制造商
     * @param ids
     */
    void enableByIds(String ids);
}
