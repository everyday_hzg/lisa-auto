package com.lisa.auto.service.c3.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.lisa.auto.entity.C3ExportLog;
import com.lisa.auto.entity.c3.model.CarModel;
import com.lisa.auto.entity.c3.model.CarModelConfigure;
import com.lisa.auto.entity.c3.response.BaseResponse;
import com.lisa.auto.enums.C3UrlEnum;
import com.lisa.auto.mapper.c3.C3CarModelConfigureMapper;
import com.lisa.auto.service.C3ExportLogService;
import com.lisa.auto.service.c3.C3CarModelConfigureService;
import com.lisa.auto.service.c3.C3CarModelService;
import com.lisa.auto.util.basic.BaseException;
import demo.C3ApiDemo;
import demo.request.ModelListRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 车型管理 服务实现类
 * </p>
 *
 * @author hxh
 * @since 2020-01-07
 */
@Service
public class C3CarModelConfigureServiceImpl extends ServiceImpl<C3CarModelConfigureMapper, CarModelConfigure> implements C3CarModelConfigureService {

    @Value("${c3.url}")
    private String c3url;
    @Value("${c3.appKey}")
    private String appKey;
    @Value("${c3.secretKey}")
    private String secretKey;

    @Autowired
    private C3ExportLogService exportLogService;
    @Autowired
    private C3CarModelService carModelService;

    @Override
    public List<CarModelConfigure> queryConfiguresToSave(String seriesNo) {
        C3ExportLog exportLog = new C3ExportLog();
        List<CarModelConfigure> carModelConfigures = new ArrayList<>();
        try {
            String url = c3url + C3UrlEnum.MODELSLISTURL.getUrl();
            ModelListRequest request = new ModelListRequest();
            //request.setVin(vin);
            request.setSeriesNo(seriesNo);
            request.setAppKey(appKey);
            String requstJson = JSON.toJSONString(request);
            exportLog.setInterfaceUrl(url);
            exportLog.setExportKey(seriesNo);
            exportLog.setExportType("GET_MODEL");
            exportLog.setTargetSys("C3");
            exportLog.setDataContent(requstJson);
            String result = C3ApiDemo.get(url, requstJson, secretKey);
            if (StringUtils.isNotBlank(result)) {
                BaseResponse<CarModel> baseResponse = JSONObject.parseObject(result, BaseResponse.class);
                //Object parse = JSONObject.parse(result);
                JSONObject jsonObject = JSONObject.parseObject(result);
                if (!"true".equals(jsonObject.get("returnSuccess").toString())) {
                    throw new BaseException("获取车辆信息失败:" + baseResponse.getReturnErrMsg());
                }
                carModelConfigures = JSON.parseArray(JSON.toJSONString(jsonObject.get("list")), CarModelConfigure.class);
                //List<CarModel> list = baseResponse.getList();
                exportLog.setExportStatus("1");
                exportLog.setExportResponse(result);
                for (CarModelConfigure carModelConfigure : carModelConfigures) {
                    carModelConfigure.setModelNo(carModelConfigure.getNo());
                    carModelConfigure.setModelName(carModelConfigure.getConfiguration());
                    addModelConfigure(carModelConfigure);
                }
                // CarModel carModel = list.get(0);
                //baseMapper.update(baseResponse.getData(),ew);
            }
        } catch (Exception e) {
            exportLog.setExportStatus("0");
            exportLog.setExportResponse(e.getMessage());
        }
        exportLogService.insert(exportLog);
        return carModelConfigures;
    }

    @Override
    public void addModelConfigure(CarModelConfigure carModelConfigure) {
        if (StringUtils.isEmpty(carModelConfigure.getNo())) {
            throw new BaseException("车型编码不能为空");
        }
        if (StringUtils.isEmpty(carModelConfigure.getSeriesNo())) {
            throw new BaseException("车系编码不能为空");
        }
        if (StringUtils.isEmpty(carModelConfigure.getSeriesName())) {
            throw new BaseException("车系名称不能为空");
        }
        //查询该车架号是否已存在，存在就更新，不存在就新增
        EntityWrapper<CarModelConfigure> ew = new EntityWrapper<>();
        ew.eq("no", carModelConfigure.getNo());
        List<CarModelConfigure> carModelConfigures = baseMapper.selectList(ew);
        if (carModelConfigures != null && !carModelConfigures.isEmpty()) {
            baseMapper.update(carModelConfigure, ew);
        } else {
            baseMapper.insert(carModelConfigure);
        }
    }

    @Override
    public void configureUpdate(CarModel carModel) {
        if (carModel == null || StringUtils.isEmpty(carModel.getNo())) throw new BaseException("车型编码为空");
        EntityWrapper<CarModel> seriesEW = new EntityWrapper<>();
        seriesEW.eq("no", carModel.getNo());
        List<CarModel> modelList = carModelService.selectList(seriesEW);
        if (modelList == null || modelList.size() < 1) throw new BaseException("未找到对应车型");
        List<CarModelConfigure> configures = carModel.getConfigures();
        if (configures != null && !configures.isEmpty()) {
            for (CarModelConfigure carModelConfigure : configures) {
                carModelConfigure.setModelNo(carModel.getNo());
                carModelConfigure.setModelName(carModel.getName());
                carModelConfigure.setSeriesNo(carModel.getSeriesNo());
                carModelConfigure.setSeriesName(carModel.getSeriesName());

                //判断该车型配置是否存在，不存在则新增，存在就更新
                EntityWrapper<CarModelConfigure> configureEW = new EntityWrapper<>();
                configureEW.eq("no", carModelConfigure.getNo());
                configureEW.eq("model_no", carModel.getNo());
                List<CarModelConfigure> configureList = baseMapper.selectList(configureEW);
                if (configureList == null || configureList.size() < 1) {
                    baseMapper.insert(carModelConfigure);
                } else {
                    baseMapper.update(carModelConfigure, configureEW);
                }
            }
        }
    }

    @Override
    public List<CarModelConfigure> getConfiguresList(Map<String, Object> condition) {
        EntityWrapper<CarModelConfigure> ew = buildWrapper(condition);
        List<CarModelConfigure> modelsList = baseMapper.selectList(ew);
        return modelsList;
    }

    @Override
    public Page<CarModelConfigure> getConfiguresListPage(Page<CarModelConfigure> page) {
        if (page == null) {
            throw new BaseException("参数不能为空");
        }
        Map<String, Object> condition = page.getCondition();
        EntityWrapper<CarModelConfigure> ew = buildWrapper(condition);
        List<CarModelConfigure> modelsList = baseMapper.getConfiguresListPage(ew, page);
        page.setRecords(modelsList);
        return page;
    }

    @Override
    public List<CarModelConfigure> getAllCarModel() {
        List<CarModelConfigure> modelsList = baseMapper.getAllCarModel();
        return modelsList;
    }

    private EntityWrapper<CarModelConfigure> buildWrapper(Map<String, Object> condition) {
        EntityWrapper<CarModelConfigure> ew = new EntityWrapper();
        if (condition.get("no") != null && !"".equals(condition.get("no"))) {
            ew.like("no", condition.get("no").toString());
        }
        if (condition.get("configuration") != null && !"".equals(condition.get("configuration"))) {
            ew.like("configuration", condition.get("configuration").toString());
        }
        if (condition.get("modelNo") != null && !"".equals(condition.get("modelNo"))) {
            ew.like("model_no", condition.get("modelNo").toString());
        }
        if (condition.get("modelName") != null && !"".equals(condition.get("modelName"))) {
            ew.like("model_name", condition.get("modelName").toString());
        }
        if (condition.get("seriesNo") != null && !"".equals(condition.get("seriesNo"))) {
            ew.like("series_no", condition.get("seriesNo").toString());
        }
        if (condition.get("seriesName") != null && !"".equals(condition.get("seriesName"))) {
            ew.like("series_name", condition.get("seriesName").toString());
        }
        if (condition.get("status") != null && !"".equals(condition.get("status"))) {
            ew.eq("status", condition.get("status").toString());
        }
        if (condition.get("startDate") != null && !"".equals(condition.get("startDate"))) {
            ew.ge("gmt_create", condition.get("startDate"));
        }
        if (condition.get("endDate") != null && !"".equals(condition.get("endDate"))) {
            ew.le("gmt_create", condition.get("endDate"));
        }
        return ew;
    }

    @Override
    public void forbiddenByIds(String ids) {
        if (StringUtils.isEmpty(ids)) throw new BaseException("参数为空");
        String[] split = ids.split(",");
        EntityWrapper<CarModelConfigure> ew = new EntityWrapper<>();
        ew.in("id", split);
        List<CarModelConfigure> carModelConfiguress = baseMapper.selectList(ew);
        if (carModelConfiguress == null || carModelConfiguress.size() < 1) throw new BaseException("未查询到对应车型配置信息");
        for (CarModelConfigure carModelConfigure : carModelConfiguress) {
            if ("50".equals(carModelConfigure.getStatus())) throw new BaseException("存在已删除车型配置");
            if ("40".equals(carModelConfigure.getStatus())) throw new BaseException("存在已禁用车型配置");
        }
        CarModelConfigure info = new CarModelConfigure();
        info.setStatus("40");
        info.setGmtModified(new Date());
        baseMapper.update(info, ew);
    }

    @Override
    public void enableByIds(String ids) {
        if (StringUtils.isEmpty(ids)) throw new BaseException("参数为空");
        String[] split = ids.split(",");
        EntityWrapper<CarModelConfigure> ew = new EntityWrapper<>();
        ew.in("id", split);
        List<CarModelConfigure> carModelConfigures = baseMapper.selectList(ew);
        if (carModelConfigures == null || carModelConfigures.size() < 1) throw new BaseException("未查询到对应车型配置信息");
        for (CarModelConfigure carModelConfigure : carModelConfigures) {
            if ("50".equals(carModelConfigure.getStatus())) throw new BaseException("存在已删除车型配置");
            if ("10".equals(carModelConfigure.getStatus())) throw new BaseException("存在已启用车型配置");
        }
        CarModelConfigure info = new CarModelConfigure();
        info.setStatus("10");
        info.setGmtModified(new Date());
        baseMapper.update(info, ew);
    }

    @Override
    public void deleteByIds(String ids) {
        if (StringUtils.isEmpty(ids)) throw new BaseException("参数为空");
        String[] split = ids.split(",");
        EntityWrapper<CarModelConfigure> ew = new EntityWrapper<>();
        ew.in("id", split);
        List<CarModelConfigure> carModelConfigures = baseMapper.selectList(ew);
        if (carModelConfigures == null || carModelConfigures.size() < 1) throw new BaseException("未查询到对应车型配置信息");
        for (CarModelConfigure carModelConfigure : carModelConfigures) {
            if ("50".equals(carModelConfigure.getStatus())) throw new BaseException("存在已删除车型配置");
        }
        CarModelConfigure info = new CarModelConfigure();
        info.setStatus("50");
        info.setGmtModified(new Date());
        baseMapper.update(info, ew);
    }

    @Override
    public void updateCarModelConfigure(CarModelConfigure carModelConfigure) {
        EntityWrapper<CarModelConfigure> ew = new EntityWrapper();
        ew.eq("no", carModelConfigure.getNo());
        carModelConfigure.setIsDelete(1);
        baseMapper.update(carModelConfigure, ew);
    }

}
