package com.lisa.auto.service.c3;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.lisa.auto.entity.c3.model.CarModel;
import com.lisa.auto.entity.c3.model.CarModelConfigure;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 车型配置管理 服务类
 * </p>
 *
 * @author hxh
 * @since 2020-01-07
 */
public interface C3CarModelConfigureService extends IService<CarModelConfigure> {

    /**
     * 外部接口获取车型配置信息并存储到数据库
     * @param seriesNo
     * @return
     */
    List<CarModelConfigure> queryConfiguresToSave(String seriesNo);

    void addModelConfigure(CarModelConfigure carModelConfigure);

    /**
     * 新增车型配置（批量）
     * @param carModel
     */
    void configureUpdate(CarModel carModel);

    /**
     * 获取车型列表
     * @param condition
     * @return
     */
    List<CarModelConfigure> getConfiguresList(Map<String, Object> condition);

    /**
     * 获取车型列表（分页）
     * @param page
     * @return
     */
    Page<CarModelConfigure> getConfiguresListPage(Page<CarModelConfigure> page);

    /**
     * 禁用车型(支持批量)
     *
     * @param ids
     */
    void forbiddenByIds(String ids);

    /**
     * 启用车型（支持批量）
     * @param ids
     */
    void enableByIds(String ids);

    /**
     * 删除车型(支持批量)
     *
     * @param ids
     */
    void deleteByIds(String ids);

    /**
     * 更新单挑配置信息
     * @param carModelConfigure
     */
    void updateCarModelConfigure(CarModelConfigure carModelConfigure);


     List<CarModelConfigure> getAllCarModel();
}
