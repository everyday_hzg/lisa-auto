package com.lisa.auto.service;

import com.baomidou.mybatisplus.service.IService;
import com.lisa.auto.entity.OrderInfo;

/**
 * <p>
 * 订单信息 服务类
 * </p>
 *
 * @author huangzhigang
 * @since 2019-12-02
 */
public interface IOrderInfoService extends IService<OrderInfo> {

    /**
     * 添加商品信息
     * @param orderInfo 订单信息
     */
    void addData(OrderInfo orderInfo);

}
