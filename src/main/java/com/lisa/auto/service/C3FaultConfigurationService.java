package com.lisa.auto.service;

import com.lisa.auto.entity.C3FaultConfiguration;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 车辆故障配置 服务类
 * </p>
 *
 * @author suohao
 * @since 2020-02-21
 */
public interface C3FaultConfigurationService extends IService<C3FaultConfiguration> {

}
