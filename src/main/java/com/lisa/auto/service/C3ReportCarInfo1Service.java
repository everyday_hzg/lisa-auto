package com.lisa.auto.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.lisa.auto.entity.*;
import com.lisa.auto.entity.statistics.VehiclesStatisticsPo;

import java.text.ParseException;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 上报车况信息 服务类
 * </p>
 *
 * @author suohao
 * @since 2019-12-25
 */
public interface C3ReportCarInfo1Service extends IService<C3ReportCar1Info> {

    /**
     * 车况上报
     * @param c3ReportCar1Info
     */
    void add(C3ReportCar1Info c3ReportCar1Info);

    /**
     * 删除历史数据
     */
    void delHistoryData();
}
