package com.lisa.auto.service;

import com.lisa.auto.entity.statistics.VehiclesPo;

import java.util.List;
import java.util.Map;

/**
 * @author huangzhigang 2020/2/26
 */
public interface CarStatisticsService {

    /**
     * 检索查询车辆信息
     * @param condition 检索条件
     * @return List<VehiclesPo>
     */
    List<VehiclesPo> searchVehiclesInfo(Map<String, String> condition);

    Map<String,Object> getTabDetails(Map<String, String> condition);
}
