package com.lisa.auto.controller.report;

import com.lisa.auto.entity.statistics.VehiclesPo;
import com.lisa.auto.service.CarStatisticsService;
import com.lisa.auto.util.basic.BaseException;
import com.lisa.util.RestfulResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * @author huangzhigang 2020/2/26
 */
@RestController
@RequestMapping("/report")
@Api(value = "Tab车队公司/车辆", description = "车队公司/车辆详情", tags = {"车队公司/车辆详情"})
public class CarStatisticsController {

    private static final Logger LOGGER = LoggerFactory.getLogger(CarStatisticsController.class);

    @Autowired
    private CarStatisticsService carStatisticsService;

    /**
     * 车队公司/车辆维度，查询tab页列表数据
     * @param condition
     * @return
     */
    @PostMapping(value = "/tab")
    @ApiOperation(value = "Tab车队公司/车辆", notes = "Tab车队公司/车辆", produces = MediaType.APPLICATION_JSON_VALUE, response = RestfulResponse.class)
    public RestfulResponse<List<VehiclesPo>> getCarList(@RequestBody Map<String, String> condition) {
        RestfulResponse<List<VehiclesPo>> result = new RestfulResponse<>(0, "查询成功", null);
        try {
            if (StringUtils.isNotBlank(condition.get("dateTime")) && "2".equals(condition.get("timeType"))){
                condition.put("dateTime", condition.get("dateTime") + "-00");
            }
            if (StringUtils.isNotBlank(condition.get("dateTime")) && "3".equals(condition.get("timeType"))){
                condition.put("dateTime", condition.get("dateTime") + "00-00");
            }
            result.setData(carStatisticsService.searchVehiclesInfo(condition));
        } catch (BaseException e) {
            LOGGER.error("CarStatisticsController.getCarList ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("CarStatisticsController.getCarList ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, "查询失败", null);
        }
        return result;
    }
    /**
     * 车队公司/车辆维度，查询tab页详情数据
     * @param condition
     * @return
     */
    @PostMapping(value = "/getTabDetails")
    @ApiOperation(value = "查询tab页详情数据", notes = "查询tab页详情数据", produces = MediaType.APPLICATION_JSON_VALUE, response = RestfulResponse.class)
    public RestfulResponse<Map<String,Object>> getTabDetails(@RequestBody Map<String, String> condition) {
        RestfulResponse<Map<String,Object>> result = new RestfulResponse<>(0, "查询成功", null);
        try {
            result.setData(carStatisticsService.getTabDetails(condition));
        } catch (BaseException e) {
            LOGGER.error("CarStatisticsController.getTabDetails ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("CarStatisticsController.getTabDetails ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, "查询失败", null);
        }
        return result;
    }

}
