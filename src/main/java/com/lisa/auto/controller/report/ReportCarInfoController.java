package com.lisa.auto.controller.report;


import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.plugins.Page;
import com.lisa.auto.entity.*;
import com.lisa.auto.entity.statistics.VehiclesStatisticsPo;
import com.lisa.auto.service.C3ReportCarInfoService;
import com.lisa.auto.service.C3ReportEventInfoService;
import com.lisa.auto.util.basic.BaseException;
import com.lisa.util.RestfulResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 车况信息 前端控制器
 * </p>
 *
 * @author suohao
 * @since 2019-12-20
 */
@RestController
@RequestMapping("/report")
@Api(value = "上报车况信息管理", description = "上报车况信息管理接口", tags = {"上报车况信息管理接口"})
public class ReportCarInfoController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ReportCarInfoController.class);

    @Autowired
    private C3ReportEventInfoService reportEventInfoService;

    @Autowired
    private C3ReportCarInfoService reportCarInfoService;


    @PostMapping(value = "/reportCarInfo")
    @ApiOperation(value = "数据上报", notes = "数据上报", produces = MediaType.APPLICATION_JSON_VALUE, response = RestfulResponse.class)
    public Map<String, Object> reportCarInfo(@RequestBody Map<String, Object> map) {
        LOGGER.info("ReportCarInfoController.reportCarInfo  dtoJSON:{}", JSONObject.toJSONString(map));
        Map<String, Object> result = new HashMap<>();
        result.put("returnSuccess", true);
        result.put("returnErrCode", 200);
        result.put("returnErrMsg", null);
        long start = System.currentTimeMillis();
        try {
            if (!map.isEmpty()) {
                if (map.containsKey("event")) {
                    reportEventInfoService.reportEventInfo(map);
                }
                if (map.containsKey("list")) {
                    reportCarInfoService.reportCarInfo(map);
                }
            }
            long end =System.currentTimeMillis();

            LOGGER.info("endTime:{}",end-start);
            return result;
        } catch (BaseException e) {
            LOGGER.error("ReportCarInfoController.reportCarInfo ERROR {}", e.getMessage());
            result.put("returnSuccess", false);
            result.put("returnErrCode", 201);
            result.put("returnErrMsg", e.getMessage());
        } catch (Exception e) {
            LOGGER.error("ReportCarInfoController.reportCarInfo ERROR {}", e.getMessage());
            result.put("returnSuccess", false);
            result.put("returnErrCode", 201);
            result.put("returnErrMsg", e.getMessage());
        }
        return result;
    }

    @PostMapping(value = "/getCarCurrentInfoList")
    @ApiOperation(value = "获取车辆当前车况信息", notes = "获取车辆当前车况信息", produces = MediaType.APPLICATION_JSON_VALUE, response = RestfulResponse.class)
    public RestfulResponse<List<C3ReportCarInfoVo>> getCarCurrentInfoList(@RequestBody Map<String, Object> condition) {
        RestfulResponse<List<C3ReportCarInfoVo>> result = new RestfulResponse<>(0, "查询成功", null);
        try {
            result.setData(reportCarInfoService.getCarCurrentInfoList(condition));
        } catch (BaseException e) {
            LOGGER.error("ReportCarInfoController.getCarCurrentInfoList ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("ReportCarInfoController.getCarCurrentInfoList ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, "查询失败", null);
        }
        return result;
    }

    @PostMapping(value = "/getCarCurrentLocationList")
    @ApiOperation(value = "获取车辆当前经纬度信息", notes = "获取车辆当前经纬度信息", produces = MediaType.APPLICATION_JSON_VALUE, response = RestfulResponse.class)
    public RestfulResponse<List<C3ReportCarInfoVo>> getCarCurrentLocationList(@RequestBody Map<String, Object> condition) {
        RestfulResponse<List<C3ReportCarInfoVo>> result = new RestfulResponse<>(0, "查询成功", null);
        try {
            result.setData(reportCarInfoService.getCarCurrentLocationList(condition));
        } catch (BaseException e) {
            LOGGER.error("ReportCarInfoController.getCarCurrentLocationList ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("ReportCarInfoController.getCarCurrentLocationList ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, "查询失败", null);
        }
        return result;
    }

    @PostMapping(value = "/getCityCarInfoList")
    @ApiOperation(value = "获取某市当天车况详情列表", notes = "获取某市当天车况详情列表", produces = MediaType.APPLICATION_JSON_VALUE, response = RestfulResponse.class)
    public RestfulResponse<List<C3ReportCarInfoDto>> getCityCarInfoList(@RequestBody Map<String, Object> condition) {
        RestfulResponse<List<C3ReportCarInfoDto>> result = new RestfulResponse<>(0, "查询成功", null);
        try {
            result.setData(reportCarInfoService.getCityCarInfoList(condition));
        } catch (BaseException e) {
            LOGGER.error("ReportCarInfoController.getCityCarInfoList ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("ReportCarInfoController.getCityCarInfoList ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, "查询失败", null);
        }
        return result;
    }

    @PostMapping(value = "/getCarInfoTotal")
    @ApiOperation(value = "获取车况统计信息", notes = "获取车况统计信息", produces = MediaType.APPLICATION_JSON_VALUE, response = RestfulResponse.class)
    public RestfulResponse<Map<String, Object>> getCarInfoTotal(@RequestBody Map<String, Object> condition) {
        RestfulResponse<Map<String, Object>> result = new RestfulResponse<>(0, "查询成功", null);
        try {
            result.setData(reportCarInfoService.getCarInfoTotal(condition));
        } catch (BaseException e) {
            LOGGER.error("ReportCarInfoController.getCarInfoTotal ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("ReportCarInfoController.getCarInfoTotal ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, "查询失败", null);
        }
        return result;
    }

    @PostMapping(value = "/getAreaVehicleCount")
    @ApiOperation(value = "获取省/市/区县当前车辆数量", notes = "获取省/市/区县当前车辆数量", produces = MediaType.APPLICATION_JSON_VALUE, response = RestfulResponse.class)
    public RestfulResponse<List<AreaCountDto>> getAreaVehicleCount(@RequestBody AreaCountDto areaCountDto) {
        RestfulResponse<List<AreaCountDto>> result = new RestfulResponse<>(0, "查询成功", null);
        try {
            result.setData(reportCarInfoService.getAreaVehicleCount(areaCountDto));
        } catch (BaseException e) {
            LOGGER.error("ReportCarInfoController.getAreaVehicleCount ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("ReportCarInfoController.getAreaVehicleCount ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, "查询失败", null);
        }
        return result;
    }

    @PostMapping(value = "/getCarInfoRoute")
    @ApiOperation(value = "获取车辆运行路线轨迹", notes = "获取车辆运行路线轨迹", produces = MediaType.APPLICATION_JSON_VALUE, response = RestfulResponse.class)
    public RestfulResponse<List<C3ReportCarInfoDto>> getCarInfoRoute(@RequestBody Map<String, Object> condition) {
        RestfulResponse<List<C3ReportCarInfoDto>> result = new RestfulResponse<>(0, "查询成功", null);
        try {
            result.setData(reportCarInfoService.getCarInfoRoute(condition));
        } catch (BaseException e) {
            LOGGER.error("ReportCarInfoController.getCarInfoRoute ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("ReportCarInfoController.getCarInfoRoute ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, "查询失败", null);
        }
        return result;
    }

    @PostMapping(value = "/getTotalFleetMiles")
    @ApiOperation(value = "获取车队里程报表", notes = "获取车队里程报表", produces = MediaType.APPLICATION_JSON_VALUE, response = RestfulResponse.class)
    public RestfulResponse<List<MilesReportDto>> getTotalFleetMiles(@RequestBody Map<String, Object> condition) {
        RestfulResponse<List<MilesReportDto>> result = new RestfulResponse<>(0, "查询成功", null);
        try {
            result.setData(reportCarInfoService.getTotalFleetMiles(condition));
        } catch (BaseException e) {
            LOGGER.error("ReportCarInfoController.getTotalFleetMiles ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("ReportCarInfoController.getTotalFleetMiles ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, "查询失败", null);
        }
        return result;
    }
    @PostMapping(value = "/getRiskFaultRate")
    @ApiOperation(value = "获取风险故障率报表", notes = "获取风险故障率报表", produces = MediaType.APPLICATION_JSON_VALUE, response = RestfulResponse.class)
    public RestfulResponse<List<Map<String,Object>>> getRiskFaultRate(@RequestBody Map<String, Object> condition) {
        RestfulResponse<List<Map<String,Object>>> result = new RestfulResponse<>(0, "查询成功", null);
        try {
            result.setData(reportCarInfoService.getRiskFaultRate(condition));
        } catch (BaseException e) {
            LOGGER.error("ReportCarInfoController.getRiskFaultRate ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("ReportCarInfoController.getRiskFaultRate ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, "查询失败", null);
        }
        return result;
    }

    @PostMapping(value = "/getRiskRateForVin")
    @ApiOperation(value = "获取车队车辆风险故障率报表", notes = "获取车队车辆风险故障率报表", produces = MediaType.APPLICATION_JSON_VALUE, response = RestfulResponse.class)
    public RestfulResponse<Page<Map<String, Object>>> getRiskRateForVin(@RequestBody Page<Map<String, Object>> page) {
        RestfulResponse<Page<Map<String, Object>>> result = new RestfulResponse<>(0, "查询成功", null);
        try {
            result.setData(reportCarInfoService.getRiskRateForVin(page));
        } catch (BaseException e) {
            LOGGER.error("ReportCarInfoController.getRiskRateForVin ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("ReportCarInfoController.getRiskRateForVin ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, "查询失败", null);
        }
        return result;
    }
    
    /**
     * 查询车队公司统计数据
     * @author huangzhigang 2020-02-25 10:45:11
     * @param condition 查询条件集
     * @return List<VehiclesStatisticsPo>
     */
    @PostMapping(value = "/selVehiclesStatistics")
    @ApiOperation(value = "获取所有车队汇总数据", notes = "获取所有车队汇总数据", produces = MediaType.APPLICATION_JSON_VALUE, response = RestfulResponse.class)
    public RestfulResponse<List<VehiclesStatisticsPo>> selVehiclesStatistics(@RequestBody Map<String, String> condition) {
        RestfulResponse<List<VehiclesStatisticsPo>> result = new RestfulResponse<>(0, "查询成功", null);
        try {
            result.setData(reportCarInfoService.selVehiclesStatistics(condition));
        } catch (BaseException e) {
            LOGGER.error("ReportCarInfoController.selVehiclesStatistics ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, "查询失败", null);
        } catch (Exception e) {
            LOGGER.error("ReportCarInfoController.selVehiclesStatistics ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, "查询失败", null);
        }
        return result;
    }

}

