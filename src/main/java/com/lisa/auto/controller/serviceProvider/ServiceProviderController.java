package com.lisa.auto.controller.serviceProvider;


import com.lisa.auto.entity.C3ServiceProvider;
import com.lisa.auto.service.C3ServiceProviderService;
import com.lisa.auto.util.basic.BaseException;
import com.lisa.util.RestfulResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 服务商接口 前端控制器
 * </p>
 *
 * @author suohao
 * @since 2019-12-23
 */
@RestController
@RequestMapping("/serviceProvider")
@Api(value = "服务商注册管理", description = "服务商注册管理接口", tags = {"服务商注册管理接口"})
public class ServiceProviderController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ServiceProviderController.class);

    @Autowired
    private C3ServiceProviderService serviceProviderService;

    @PostMapping(value = "/registServiceProvider")
    @ApiOperation(value = "注册服务商", notes = "注册服务商", produces = MediaType.APPLICATION_JSON_VALUE, response = RestfulResponse.class)
    public RestfulResponse registServiceProvider(@RequestBody C3ServiceProvider serviceProvider) {
        RestfulResponse  result = new RestfulResponse<>(0, "注册成功", null);
        try {
            serviceProviderService.registServiceProvider(serviceProvider);
        }   catch (BaseException e) {
            LOGGER.error("ServiceProviderController.registServiceProvider ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        }catch (Exception e) {
            LOGGER.error("ServiceProviderController.registServiceProvider ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, "注册失败", null);
        }
        return result;
    }
}

