package com.lisa.auto.controller.models;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.plugins.Page;
import com.lisa.auto.entity.c3.model.CarModel;
import com.lisa.auto.entity.c3.model.CarSeries;
import com.lisa.auto.service.c3.C3CarModelService;
import com.lisa.auto.util.basic.BaseException;
import com.lisa.util.RestfulResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @author : suohao
 * Date: 2019/12/17
 * 车型
 */
@RestController
@Api(value = "车型管理", description = "车型管理接口", tags = {"车型管理接口"})
@RequestMapping("/model")
public class ModelController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ModelController.class);

    @Autowired
    private C3CarModelService carModelService;

    /**
     * 新增车型
     *
     * @return RestfulResponse
     */
    @ApiOperation(value = "新增车型", notes = "新增车型")
    @RequestMapping(value = "/addModel", method = RequestMethod.POST)
    public RestfulResponse addModel(@RequestBody CarModel carModel) {
        LOGGER.info("ModelController.addModel OTD dtoJSON:{}", JSONObject.toJSONString(carModel));
        RestfulResponse result = new RestfulResponse<>(0, "success", null);
        try {
            carModelService.addModel(carModel);
        } catch (BaseException e) {
            LOGGER.error("ModelController.addModel error:{}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("ModelController.addModel error:{}", e.getMessage());
            result = new RestfulResponse<>(-1, "新增车型失败", null);
        }
        return result;
    }

    /**
     * 编辑车型(支持批量)
     *
     * @return RestfulResponse
     */
    @ApiOperation(value = "编辑车型(支持批量)", notes = "编辑车型(支持批量)")
    @RequestMapping(value = "/modelUpdate", method = RequestMethod.POST)
    public RestfulResponse modelUpdate(@RequestBody CarSeries carSeries) {
        LOGGER.info("ModelController.modelUpdate OTD dtoJSON:{}", JSONObject.toJSONString(carSeries));
        RestfulResponse result = new RestfulResponse<>(0, "success", null);
        try {
            carModelService.modelUpdate(carSeries);
        } catch (BaseException e) {
            LOGGER.error("ModelController.modelUpdate error:{}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("ModelController.modelUpdate error:{}", e.getMessage());
            result = new RestfulResponse<>(-1, "编辑车型失败", null);
        }
        return result;
    }



    @PostMapping(value = "/getModelsListFromC3")
    @ApiOperation(value = "外部接口获取车型数据", notes = "外部接口获取车型数据", response = RestfulResponse.class)
    public RestfulResponse<List<CarModel>> getModelsListFromC3(@RequestParam("modelNo") String modelNo) {
        RestfulResponse<List<CarModel>> result = new RestfulResponse<>(0, "获取成功", null);
        try {
            result.setData(carModelService.queryModelsToSave(modelNo));
        } catch (BaseException e) {
            LOGGER.error("ModelController.getModelsListFromC3 ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("ModelController.getModelsListFromC3 ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, "获取失败", null);
        }
        return result;
    }

    @PostMapping(value = "/getModelsList")
    @ApiOperation(value = "获取车型列表数据", notes = "获取车型列表数据", produces = MediaType.APPLICATION_JSON_VALUE, response = RestfulResponse.class)
    public RestfulResponse<List<CarModel>> getModelsList(@RequestBody Map<String, Object> condition) {
        RestfulResponse<List<CarModel>> result = new RestfulResponse<>(0, "查询成功", null);
        try {
            result.setData(carModelService.getModelsList(condition));
        } catch (BaseException e) {
            LOGGER.error("ModelController.getModelsList ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("ModelController.getModelsList ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        }
        return result;
    }

    @PostMapping(value = "/getModelsListPage")
    @ApiOperation(value = "获取车型列表数据（分页）", notes = "获取车型列表数据（分页）", produces = MediaType.APPLICATION_JSON_VALUE, response = RestfulResponse.class)
    public RestfulResponse<Page<CarModel>> getModelsListPage(@RequestBody Page<CarModel> page) {
        RestfulResponse<Page<CarModel>> result = new RestfulResponse<>(0, "查询成功", null);
        try {
            result.setData(carModelService.getModelsListPage(page));
        } catch (BaseException e) {
            LOGGER.error("ModelController.getModelsListPage ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("ModelController.getModelsListPage ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        }
        return result;
    }

    /**
     * 禁用车型(支持批量)
     *
     * @return RestfulResponse
     */
    @ApiOperation(value = "禁用车型", notes = "禁用车型")
    @RequestMapping(value = "/forbiddenByIds", method = RequestMethod.POST)
    public RestfulResponse forbiddenByIds(@RequestBody Map<String, Object> map) {
        RestfulResponse result = new RestfulResponse<>(0, "success", null);
        try {
            carModelService.forbiddenByIds(String.valueOf(map.get("ids")));
        } catch (BaseException e) {
            LOGGER.error("ModelController.forbiddenByIds error:{}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("ModelController.forbiddenByIds error:{}", e.getMessage());
            result = new RestfulResponse<>(-1, "禁用失败", null);
        }
        return result;
    }

    /**
     * 启用车型(支持批量)
     *
     * @return RestfulResponse
     */
    @ApiOperation(value = "启用车型", notes = "启用车型")
    @RequestMapping(value = "/enableByIds", method = RequestMethod.POST)
    public RestfulResponse enableByIds(@RequestBody Map<String, Object> map) {
        RestfulResponse result = new RestfulResponse<>(0, "success", null);
        try {
            carModelService.enableByIds(String.valueOf(map.get("ids")));
        } catch (BaseException e) {
            LOGGER.error("ModelController.enableByIds error:{}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("ModelController.enableByIds error:{}", e.getMessage());
            result = new RestfulResponse<>(-1, "启用失败", null);
        }
        return result;
    }

    /**
     * 删除车型
     *
     * @return RestfulResponse
     */
    @ApiOperation(value = "删除车型", notes = "删除车型")
    @RequestMapping(value = "/deleteById", method = RequestMethod.POST)
    public RestfulResponse deleteById(@RequestBody Long id) {
        RestfulResponse result = new RestfulResponse<>(0, "success", null);
        try {
            carModelService.deleteById(id);
        } catch (BaseException e) {
            LOGGER.error("ModelController.deleteById error:{}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("ModelController.deleteById error:{}", e.getMessage());
            result = new RestfulResponse<>(-1, "删除失败", null);
        }
        return result;
    }

    /**
     * 删除车型(支持批量)
     *
     * @return RestfulResponse
     */
    @ApiOperation(value = "删除车型", notes = "删除车型")
    @RequestMapping(value = "/deleteByIds", method = RequestMethod.POST)
    public RestfulResponse deleteByIds(@RequestBody Map<String, Object> map) {
        RestfulResponse result = new RestfulResponse<>(0, "success", null);
        try {
            carModelService.deleteByIds(String.valueOf(map.get("ids")));
        } catch (BaseException e) {
            LOGGER.error("ModelController.deleteByIds error:{}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("ModelController.deleteByIds error:{}", e.getMessage());
            result = new RestfulResponse<>(-1, "删除失败", null);
        }
        return result;
    }

    /**
     * 修改车型信息
     *
     * @return RestfulResponse
     */
    @ApiOperation(value = "修改车型信息", notes = "修改车型信息")
    @RequestMapping(value = "/updateById", method = RequestMethod.POST)
    public RestfulResponse updateById(@RequestBody CarModel carModel) {
        LOGGER.info("ModelInfoController.updateById OTD dtoJSON:{}", JSONObject.toJSONString(carModel));
        RestfulResponse result = new RestfulResponse<>(0, "success", null);
        try {
            carModelService.updateCarModel(carModel);
        } catch (BaseException e) {
            LOGGER.error("ModelController.updateById error:{}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("ModelController.updateById error:{}", e.getMessage());
            result = new RestfulResponse<>(-1, "更新车型信息失败", null);
        }
        return result;
    }



}
