package com.lisa.auto.controller.models;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.plugins.Page;
import com.lisa.auto.entity.c3.model.CarModel;
import com.lisa.auto.entity.c3.model.CarModelConfigure;
import com.lisa.auto.service.c3.C3CarModelConfigureService;
import com.lisa.auto.util.basic.BaseException;
import com.lisa.util.RestfulResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @author : hxh
 * Date: 2020/01/07
 * 车型配置
 */
@RestController
@Api(value = "车型配置", description = "车型配置接口", tags = {"车型配置接口"})
@RequestMapping("/modelConfigure")
public class ModelConfigureController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ModelConfigureController.class);

    @Autowired
    private C3CarModelConfigureService c3CarModelConfigureService;

    /**
     * 新增车型配置
     *
     * @return RestfulResponse
     */
    @ApiOperation(value = "新增车型配置", notes = "新增车型配置")
    @RequestMapping(value = "/addModelConfigure", method = RequestMethod.POST)
    public RestfulResponse addModelConfigure(@RequestBody CarModelConfigure carModelConfigure) {
        LOGGER.info("ModelConfigureController.addModelConfigure OTD dtoJSON:{}", JSONObject.toJSONString(carModelConfigure));
        RestfulResponse result = new RestfulResponse<>(0, "success", null);
        try {
            c3CarModelConfigureService.addModelConfigure(carModelConfigure);
        } catch (BaseException e) {
            LOGGER.error("ModelConfigureController.addModelConfigure error:{}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("ModelConfigureController.addModelConfigure error:{}", e.getMessage());
            result = new RestfulResponse<>(-1, "车型配置失败", null);
        }
        return result;
    }

    /**
     * 编辑车型配置(支持批量)
     *
     * @return RestfulResponse
     */
    @ApiOperation(value = "编辑车型配置(支持批量)", notes = "编辑车型配置(支持批量)")
    @RequestMapping(value = "/configureUpdate", method = RequestMethod.POST)
    public RestfulResponse configureUpdate(@RequestBody CarModel carModel) {
        LOGGER.info("ModelConfigureController.configureUpdate OTD dtoJSON:{}", JSONObject.toJSONString(carModel));
        RestfulResponse result = new RestfulResponse<>(0, "success", null);
        try {
            c3CarModelConfigureService.configureUpdate(carModel);
        } catch (BaseException e) {
            LOGGER.error("ModelConfigureController.configureUpdate error:{}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("ModelConfigureController.configureUpdate error:{}", e.getMessage());
            result = new RestfulResponse<>(-1, "编辑车型配置失败", null);
        }
        return result;
    }


    @PostMapping(value = "/getConfiguresListFromC3")
    @ApiOperation(value = "外部接口获取车型数据", notes = "外部接口获取车型数据", response = RestfulResponse.class)
    public RestfulResponse<List<CarModelConfigure>> getConfiguresListFromC3(@RequestParam("seriesNo") String seriesNo) {
        RestfulResponse<List<CarModelConfigure>> result = new RestfulResponse<>(0, "获取成功", null);
        try {
            result.setData(c3CarModelConfigureService.queryConfiguresToSave(seriesNo));
        } catch (BaseException e) {
            LOGGER.error("ModelConfigureController.getConfiguresListFromC3 ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("ModelConfigureController.getConfiguresListFromC3 ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, "获取失败", null);
        }
        return result;
    }

    @PostMapping(value = "/getConfiguresList")
    @ApiOperation(value = "获取车型配置列表数据", notes = "获取车型配置列表数据", produces = MediaType.APPLICATION_JSON_VALUE, response = RestfulResponse.class)
    public RestfulResponse<List<CarModelConfigure>> getConfiguresList(@RequestBody Map<String, Object> condition) {
        RestfulResponse<List<CarModelConfigure>> result = new RestfulResponse<>(0, "查询成功", null);
        try {
            result.setData(c3CarModelConfigureService.getConfiguresList(condition));
        } catch (BaseException e) {
            LOGGER.error("ModelConfigureController.getConfiguresList ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("ModelConfigureController.getConfiguresList ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        }
        return result;
    }

    @PostMapping(value = "/getConfiguresListPage")
    @ApiOperation(value = "获取车型配置列表数据（分页）", notes = "获取车型配置列表数据（分页）", produces = MediaType.APPLICATION_JSON_VALUE, response = RestfulResponse.class)
    public RestfulResponse<Page<CarModelConfigure>> getConfiguresListPage(@RequestBody Page<CarModelConfigure> page) {
        RestfulResponse<Page<CarModelConfigure>> result = new RestfulResponse<>(0, "查询成功", null);
        try {
            result.setData(c3CarModelConfigureService.getConfiguresListPage(page));
        } catch (BaseException e) {
            LOGGER.error("ModelConfigureController.getConfiguresListPage ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("ModelConfigureController.getConfiguresListPage ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        }
        return result;
    }

    /**
     * 禁用车型(支持批量)
     *
     * @return RestfulResponse
     */
    @ApiOperation(value = "禁用车型", notes = "禁用车型")
    @RequestMapping(value = "/forbiddenByIds", method = RequestMethod.POST)
    public RestfulResponse forbiddenByIds(@RequestBody Map<String, Object> map) {
        RestfulResponse result = new RestfulResponse<>(0, "success", null);
        try {
            c3CarModelConfigureService.forbiddenByIds(String.valueOf(map.get("ids")));
        } catch (BaseException e) {
            LOGGER.error("ModelConfigureController.forbiddenByIds error:{}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("ModelConfigureController.forbiddenByIds error:{}", e.getMessage());
            result = new RestfulResponse<>(-1, "禁用失败", null);
        }
        return result;
    }

    /**
     * 启用车型(支持批量)
     *
     * @return RestfulResponse
     */
    @ApiOperation(value = "启用车型", notes = "启用车型")
    @RequestMapping(value = "/enableByIds", method = RequestMethod.POST)
    public RestfulResponse enableByIds(@RequestBody Map<String, Object> map) {
        RestfulResponse result = new RestfulResponse<>(0, "success", null);
        try {
            c3CarModelConfigureService.enableByIds(String.valueOf(map.get("ids")));
        } catch (BaseException e) {
            LOGGER.error("ModelConfigureController.enableByIds error:{}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("ModelConfigureController.enableByIds error:{}", e.getMessage());
            result = new RestfulResponse<>(-1, "启用失败", null);
        }
        return result;
    }

    /**
     * 删除车型
     *
     * @return RestfulResponse
     */
    @ApiOperation(value = "删除车型", notes = "删除车型")
    @RequestMapping(value = "/deleteById", method = RequestMethod.POST)
    public RestfulResponse deleteById(@RequestBody Long id) {
        RestfulResponse result = new RestfulResponse<>(0, "success", null);
        try {
            c3CarModelConfigureService.deleteById(id);
        } catch (BaseException e) {
            LOGGER.error("ModelConfigureController.deleteById error:{}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("ModelConfigureController.deleteById error:{}", e.getMessage());
            result = new RestfulResponse<>(-1, "删除失败", null);
        }
        return result;
    }

    /**
     * 删除车型(支持批量)
     *
     * @return RestfulResponse
     */
    @ApiOperation(value = "删除车型(支持批量)", notes = "删除车型(支持批量)")
    @RequestMapping(value = "/deleteByIds", method = RequestMethod.POST)
    public RestfulResponse deleteByIds(@RequestBody Map<String, Object> map) {
        RestfulResponse result = new RestfulResponse<>(0, "success", null);
        try {
            c3CarModelConfigureService.deleteByIds(String.valueOf(map.get("ids")));
        } catch (BaseException e) {
            LOGGER.error("ModelConfigureController.deleteByIds error:{}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("ModelConfigureController.deleteByIds error:{}", e.getMessage());
            result = new RestfulResponse<>(-1, "删除失败", null);
        }
        return result;
    }
    /**
     * 查询所有配置
     *
     * @return RestfulResponse
     */
    @ApiOperation(value = "查询所有配置", notes = "查询所有配置")
    @RequestMapping(value = "/getAllCarModel", method = RequestMethod.GET)
    public RestfulResponse<List<CarModelConfigure>> getAllCarModel() {
        RestfulResponse<List<CarModelConfigure>> result = new RestfulResponse<>(0, "success", null);
        try {
            result.setData(c3CarModelConfigureService.getAllCarModel());
        } catch (BaseException e) {
            LOGGER.error("ModelConfigureController.getAllCarModel error:{}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("ModelConfigureController.getAllCarModel error:{}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        }
        return result;
    }

    /**
     * 修改车型配置信息
     *
     * @return RestfulResponse
     */
    @ApiOperation(value = "修改车型配置信息", notes = "修改车型配置信息")
    @RequestMapping(value = "/updateById", method = RequestMethod.POST)
    public RestfulResponse updateById(@RequestBody CarModelConfigure carModelConfigure) {
        LOGGER.info("ModelInfoController.updateById OTD dtoJSON:{}", JSONObject.toJSONString(carModelConfigure));
        RestfulResponse result = new RestfulResponse<>(0, "success", null);
        try {
            c3CarModelConfigureService.updateById(carModelConfigure);
        } catch (BaseException e) {
            LOGGER.error("ModelConfigureController.updateById error:{}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("ModelConfigureController.updateById error:{}", e.getMessage());
            result = new RestfulResponse<>(-1, "更新车型信息失败", null);
        }
        return result;
    }

}
