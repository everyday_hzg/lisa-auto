package com.lisa.auto.controller.historyTrail;


import com.lisa.auto.entity.C3HistoryTrail;
import com.lisa.auto.service.C3HistoryTrailService;
import com.lisa.auto.util.basic.BaseException;
import com.lisa.util.RestfulResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 车辆历史轨迹信息 前端控制器
 * </p>
 *
 * @author suohao
 * @since 2019-12-20
 */
@RestController
@RequestMapping("/historyTrail")
@Api(value = "车辆历史轨迹管理", description = "车辆历史轨迹管理", tags = {"车辆历史轨迹管理接口"})
public class HistoryTrailController {


    private static final Logger LOGGER = LoggerFactory.getLogger(HistoryTrailController.class);

    @Autowired
    private C3HistoryTrailService historyTrailService;


    @PostMapping(value = "/getHistoryTrailList1")
    @ApiOperation(value = "获取历史轨迹信息", notes = "获取历史轨迹信息", produces = MediaType.APPLICATION_JSON_VALUE, response = RestfulResponse.class)
    public RestfulResponse<List<C3HistoryTrail>> getHistoryTrailList1() {
        RestfulResponse<List<C3HistoryTrail>>   result = new RestfulResponse<>(0, "查询成功", null);
        try {
            historyTrailService.historyTrailToRedis();
        }   catch (BaseException e) {
            LOGGER.error("HistoryTrailController.getHistoryTrailList ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        }catch (Exception e) {
            LOGGER.error("HistoryTrailController.getHistoryTrailList ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, "查询失败", null);
        }
        return result;
    }


    @PostMapping(value = "/getHistoryTrailListC3")
    @ApiOperation(value = "获取历史轨迹信息", notes = "获取历史轨迹信息", produces = MediaType.APPLICATION_JSON_VALUE, response = RestfulResponse.class)
    public RestfulResponse<List<C3HistoryTrail>> getHistoryTrailListC3(@RequestParam("vin") String vin,Long startTime,Long endTime) {
        RestfulResponse<List<C3HistoryTrail>>   result = new RestfulResponse<>(0, "查询成功", null);
        try {
            result.setData(historyTrailService.getHistoryTrailListC3(vin,startTime,endTime));
        }   catch (BaseException e) {
            LOGGER.error("HistoryTrailController.getHistoryTrailList ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        }catch (Exception e) {
            LOGGER.error("HistoryTrailController.getHistoryTrailList ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, "查询失败", null);
        }
        return result;
    }
}

