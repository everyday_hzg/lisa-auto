package com.lisa.auto.controller.excel;

import com.lisa.auto.entity.C3CarInfo;
import com.lisa.auto.entity.excel.CarCondPo;
import com.lisa.auto.service.excel.CarCondExportService;
import com.lisa.util.RestfulResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @description:
 * @author: huangzhigang
 * @date: 2020-05-07 14:45
 **/
@RestController
@Api(description = "车辆轨迹报告")
@RequestMapping(value = "/export", produces = MediaType.APPLICATION_JSON_VALUE)
public class CarCondExportController {

    @Autowired
    private CarCondExportService carCondExportService;

    /**
     * 查询列表
     *
     * @return RestfulResponse
     */
    @ApiOperation(value = "查询列表", notes = "查询列表")
    @RequestMapping(value = "/selList", method = RequestMethod.POST)
    public List<CarCondPo> selList() {
        return carCondExportService.selList();
    }

    /**
     * 下载
     *
     * @return RestfulResponse
     */
    @ApiOperation(value = "下载", notes = "下载")
    @RequestMapping(value = "/download/{dateTime}", method = RequestMethod.GET)
    public void download(@PathVariable(value = "dateTime")String dateTime, HttpServletResponse response) {
        carCondExportService.download(dateTime, response);
    }

}
