package com.lisa.auto.controller.carInfo;


import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.plugins.Page;
import com.lisa.auto.entity.C3CarInfo;
import com.lisa.auto.service.C3CarInfoService;
import com.lisa.auto.util.basic.BaseException;
import com.lisa.util.RestfulResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 车况信息 前端控制器
 * </p>
 *
 * @author suohao
 * @since 2019-12-20
 */
@RestController
@RequestMapping("/carInfo")
@Api(value = "车况管理", description = "车况管理接口", tags = {"车况管理接口"})
public class CarInfoController {

    private static final Logger LOGGER = LoggerFactory.getLogger(CarInfoController.class);

    @Autowired
    private C3CarInfoService carInfoService;


    @PostMapping(value = "/getCarInfo")
    @ApiOperation(value = "获取实时车况", notes = "获取实时车况", produces = MediaType.APPLICATION_JSON_VALUE, response = RestfulResponse.class)
    public RestfulResponse<C3CarInfo> getCarInfo(@RequestParam("vin")String vin) {
        RestfulResponse<C3CarInfo>  result = new RestfulResponse<>(0, "查询成功", null);
        try {
            carInfoService.getCarInfoToC3(vin);
        }   catch (BaseException e) {
            LOGGER.error("CarInfoController.getCarInfo ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        }catch (Exception e) {
            LOGGER.error("CarInfoController.getCarInfo ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, "查询失败", null);
        }
        return result;
    }

    @PostMapping(value = "/getVehicleList")
    @ApiOperation(value = "获取车型列表数据", notes = "获取车型列表数据", produces = MediaType.APPLICATION_JSON_VALUE, response = RestfulResponse.class)
    public RestfulResponse<Page<Map<String, Object>>> getVehicleList(@RequestBody Page<Map<String, Object>> page) {
        LOGGER.info("CarInfoController.getCarInfoList  dtoJSON:{}", JSONObject.toJSONString(page.getCondition()));
        RestfulResponse<Page<Map<String, Object>>> result = new RestfulResponse<>(0, "查询成功", null);
        try {
            result.setData(carInfoService.getVehicleList(page));
        } catch (BaseException e) {
            LOGGER.error("ModelController.getCarInfoList ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("ModelController.getCarInfoList ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        }
        return result;
    }

    @GetMapping(value = "/getTotalDistance")
    @ApiOperation(value = "获取行驶里程根据时间", notes = "获取行驶里程根据时间", produces = MediaType.APPLICATION_JSON_VALUE, response = RestfulResponse.class)
    public RestfulResponse<List<Map<String,Object>>> getTotalDistance(@RequestParam("vin")String vin,
                                                  @RequestParam("startTime")String startTime,
                                                  @RequestParam("endTime")String endTime) {
        RestfulResponse<List<Map<String,Object>>>  result = new RestfulResponse<>(0, "查询成功", null);
        try {
            result.setData(carInfoService.getTotalDistance(vin,startTime,endTime));
        }   catch (BaseException e) {
            LOGGER.error("CarInfoController.getTotalDistance ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        }catch (Exception e) {
            LOGGER.error("CarInfoController.getTotalDistance ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        }
        return result;
    }

    @GetMapping(value = "/getVehicleSpeed")
    @ApiOperation(value = "获取最高车速", notes = "获取最高车速", produces = MediaType.APPLICATION_JSON_VALUE, response = RestfulResponse.class)
    public RestfulResponse<List<Map<String,Object>>> getVehicleSpeed(@RequestParam("vin")String vin,
                                                                      @RequestParam("startTime")String startTime,
                                                                      @RequestParam("endTime")String endTime) {
        RestfulResponse<List<Map<String,Object>>>  result = new RestfulResponse<>(0, "查询成功", null);
        try {
            result.setData(carInfoService.getVehicleSpeed(vin,startTime,endTime));
        }   catch (BaseException e) {
            LOGGER.error("CarInfoController.getVehicleSpeed ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        }catch (Exception e) {
            LOGGER.error("CarInfoController.getVehicleSpeed ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        }
        return result;
    }

}

