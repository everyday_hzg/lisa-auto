package com.lisa.auto.controller.vehicles;

import com.baomidou.mybatisplus.plugins.Page;
import com.lisa.auto.entity.Vehicle;
import com.lisa.auto.entity.VehicleDto;
import com.lisa.auto.entity.VehicleModelConfigureDto;
import com.lisa.auto.service.IVehicleService;
import com.lisa.auto.util.basic.BaseException;
import com.lisa.json.JSONUtil;
import com.lisa.util.RestfulResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * @author : suohao
 * Date: 2019/12/17
 * 车辆前端控制器
 */
@RestController
@RequestMapping("/vehicles")
@Api(value = "车辆管理", description = "车辆管理接口", tags = {"车辆管理接口"})
public class VehiclesController {

    private static final Logger LOGGER = LoggerFactory.getLogger(VehiclesController.class);

    @Autowired
    private IVehicleService vehicleService;

    @PostMapping(value = "/createVehicles")
    @ApiOperation(value = "新增车辆", notes = "新增车辆", produces = MediaType.APPLICATION_JSON_VALUE, response = RestfulResponse.class)
    public RestfulResponse createVehicles(@RequestBody VehicleDto vehicleDto) {
        LOGGER.info("VehiclesController.createVehicles（新增车辆） params : {} ", JSONUtil.toJsonStr(vehicleDto));
        RestfulResponse result = new RestfulResponse<>(0, "新增成功", null);
        try {
            vehicleService.createVehicles(vehicleDto);
        } catch (BaseException e) {
            LOGGER.error("VehiclesController.createVehicles ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("VehiclesController.createVehicles ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, "新增失败", null);
        }
        return result;
    }

    @PostMapping(value = "/getVehiclesList")
    @ApiOperation(value = "获取车辆详细数据", notes = "获取车辆详细数据", produces = MediaType.APPLICATION_JSON_VALUE, response = RestfulResponse.class)
    public RestfulResponse<Page<Vehicle>> getVehiclesList(@RequestBody Page<Vehicle> page) {
        RestfulResponse<Page<Vehicle>> result = new RestfulResponse<>(0, "查询成功", null);
        try {
            result.setData(vehicleService.getVehiclesList(page));
        } catch (BaseException e) {
            LOGGER.error("VehiclesController.getVehiclesList ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("VehiclesController.getVehiclesList ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, "查询失败", null);
        }
        return result;
    }

    @GetMapping(value = "/getVehiclesFleet")
    @ApiOperation(value = "获取车辆的车队公司", notes = "获取车辆的车队公司", produces = MediaType.APPLICATION_JSON_VALUE, response = RestfulResponse.class)
    public RestfulResponse<List<String>> getVehiclesFleet(@RequestParam String fleet) {
        RestfulResponse<List<String>>  result = new RestfulResponse<>(0, "查询成功", null);
        try {
            result.setData(vehicleService.getVehiclesFleet(fleet));
        } catch (BaseException e) {
            LOGGER.error("VehiclesController.getVehiclesFleet ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("VehiclesController.getVehiclesFleet ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, "查询失败", null);
        }
        return result;
    }

    @PostMapping(value = "/getPlateList")
    @ApiOperation(value = "获取车队公司下的车牌号", notes = "获取车队公司下的车牌号", produces = MediaType.APPLICATION_JSON_VALUE, response = RestfulResponse.class)
    public RestfulResponse<List<String>> getPlateList(@RequestBody Map<String,String> map) {
        RestfulResponse<List<String>>  result = new RestfulResponse<>(0, "查询成功", null);
        try {
            result.setData(vehicleService.getPlateList(map));
        } catch (BaseException e) {
            LOGGER.error("VehiclesController.getVehiclesFleet ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("VehiclesController.getVehiclesFleet ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, "查询失败", null);
        }
        return result;
    }

    @PostMapping(value = "/updateVehicles")
    @ApiOperation(value = "更改车辆信息", notes = "更改车辆信息", produces = MediaType.APPLICATION_JSON_VALUE, response = RestfulResponse.class)
    public RestfulResponse updateVehicles(@RequestBody Vehicle vehicle) {
        LOGGER.info("VehiclesController.updateVehicles（更改车辆信息） params : {} ", JSONUtil.toJsonStr(vehicle));
        RestfulResponse result = new RestfulResponse<>(0, "更改车辆信息成功", null);
        try {
            vehicleService.updateVehicles(vehicle);
        } catch (BaseException e) {
            LOGGER.error("VehiclesController.updateVehicles ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("VehiclesController.updateVehicles ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, "更改车辆信息失败", null);
        }
        return result;
    }

    @PostMapping(value = "/deleteVehicles")
    @ApiOperation(value = "删除车辆", notes = "删除车辆", produces = MediaType.APPLICATION_JSON_VALUE, response = RestfulResponse.class)
    public RestfulResponse deleteVehicles(@RequestBody Vehicle vehicle) {
        LOGGER.info("VehiclesController.deleteVehicles（删除车辆） params : {} ", JSONUtil.toJsonStr(vehicle));
        RestfulResponse result = new RestfulResponse<>(0, "删除车辆", null);
        try {
            vehicleService.deleteVehicles(vehicle);
        } catch (BaseException e) {
            LOGGER.error("VehiclesController.deleteVehicles ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("VehiclesController.deleteVehicles ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, "删除车辆", null);
        }
        return result;
    }

    @PostMapping(value = "/updateStatusVehicles")
    @ApiOperation(value = "启用禁用车辆", notes = "启用禁用车辆", produces = MediaType.APPLICATION_JSON_VALUE, response = RestfulResponse.class)
    public RestfulResponse updateStatusVehicles( String vin,String status) {
        LOGGER.info("VehiclesController.updateStatusVehicles（启用禁用车辆） params :车架号 {}，状态  {} ", vin, status);
        RestfulResponse result = new RestfulResponse<>(0, "启用禁用车辆", null);
        try {
            vehicleService.updateStatusVehicles(vin, status);
        } catch (BaseException e) {
            LOGGER.error("VehiclesController.updateStatusVehicles ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("VehiclesController.updateStatusVehicles ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, "启用禁用车辆", null);
        }
        return result;
    }
    /**
     * 禁用车辆(支持批量)
     *
     * @return RestfulResponse
     */
    @ApiOperation(value = "禁用车辆", notes = "禁用车辆")
    @RequestMapping(value = "/forbiddenByIds", method = RequestMethod.POST)
    public RestfulResponse forbiddenByIds(@RequestBody Map<String, Object> map) {
        RestfulResponse result = new RestfulResponse<>(0, "success", null);
        try {
            vehicleService.forbiddenByIds(String.valueOf(map.get("ids")));
        } catch (BaseException e) {
            LOGGER.error("VehiclesController.forbiddenByIds error:{}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("VehiclesController.forbiddenByIds error:{}", e.getMessage());
            result = new RestfulResponse<>(-1, "禁用失败", null);
        }
        return result;
    }

    /**
     * 启用车辆(支持批量)
     *
     * @return RestfulResponse
     */
    @ApiOperation(value = "启用车辆", notes = "启用车辆")
    @RequestMapping(value = "/enableByIds", method = RequestMethod.POST)
    public RestfulResponse enableByIds(@RequestBody Map<String, Object> map) {
        RestfulResponse result = new RestfulResponse<>(0, "success", null);
        try {
            vehicleService.enableByIds(String.valueOf(map.get("ids")));
        } catch (BaseException e) {
            LOGGER.error("VehiclesController.enableByIds error:{}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("VehiclesController.enableByIds error:{}", e.getMessage());
            result = new RestfulResponse<>(-1, "启用失败", null);
        }
        return result;
    }

    /**
     * 删除车辆(支持批量)
     *
     * @return RestfulResponse
     */
    @ApiOperation(value = "删除车辆", notes = "删除车辆")
    @RequestMapping(value = "/deleteByIds", method = RequestMethod.POST)
    public RestfulResponse deleteByIds(@RequestBody Map<String, Object> map) {
        RestfulResponse result = new RestfulResponse<>(0, "success", null);
        try {
            vehicleService.deleteByIds(String.valueOf(map.get("ids")));
        } catch (BaseException e) {
            LOGGER.error("VehiclesController.deleteByIds error:{}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("VehiclesController.deleteByIds error:{}", e.getMessage());
            result = new RestfulResponse<>(-1, "删除失败", null);
        }
        return result;
    }



    /**
     * 车辆批量导入
     *
     * @return RestfulResponse
     */
    @ApiOperation(value = "车辆批量导入", notes = "车辆批量导入")
    @RequestMapping(value = "/importVehicleExcel", method = RequestMethod.POST)
    public RestfulResponse importVehicleExcel(MultipartFile file, HttpServletResponse response) {
        RestfulResponse result = new RestfulResponse<>(0, "success", null);
        try {
            vehicleService.importVehicleExcel(file, response);
        } catch (BaseException e) {
            LOGGER.error("VehiclesController.importVehicleExcel error:{}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("VehiclesController.importVehicleExcel error:{}", e.getMessage());
            result = new RestfulResponse<>(-1, "导入失败", null);
        }
        return result;
    }
    @PostMapping(value = "/getVehiclesListNew")
    @ApiOperation(value = "获取车辆详细数据(新版)", notes = "获取车辆详细数据(新版)", produces = MediaType.APPLICATION_JSON_VALUE, response = RestfulResponse.class)
    public RestfulResponse<Page<VehicleModelConfigureDto>> getVehiclesListNew(@RequestBody Page<VehicleModelConfigureDto> page) {
        RestfulResponse<Page<VehicleModelConfigureDto>> result = new RestfulResponse<>(0, "查询成功", null);
        try {
            result.setData(vehicleService.getVehiclesListNew(page));
        } catch (BaseException e) {
            LOGGER.error("VehiclesController.getVehiclesListNew ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("VehiclesController.getVehiclesListNew ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, "查询失败", null);
        }
        return result;
    }

    @PostMapping(value = "/vehiclesToC3")
    @ApiOperation(value = "推送车辆到C3", notes = "推送车辆到C3(新版)", produces = MediaType.APPLICATION_JSON_VALUE, response = RestfulResponse.class)
    public RestfulResponse vehiclesToC3(@RequestBody Vehicle vehicle) {
        RestfulResponse result = new RestfulResponse<>(0, "推送成功", null);
        try {
            result.setData(vehicleService.vehiclesToC3(vehicle));
        } catch (BaseException e) {
            LOGGER.error("VehiclesController.getVehiclesListNew ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("VehiclesController.getVehiclesListNew ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, "推送失败", null);
        }
        return result;
    }

}
