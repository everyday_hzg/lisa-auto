package com.lisa.auto.controller.vehicles;


import com.lisa.auto.entity.VehicleArea;
import com.lisa.auto.entity.c3.model.Maker;
import com.lisa.auto.entity.c3.model.ProtocolVersion;
import com.lisa.auto.service.VehicleAreaService;
import com.lisa.auto.util.basic.BaseException;
import com.lisa.util.RestfulResponse;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 车辆所属地区 前端控制器
 * </p>
 *
 * @author hxh
 * @since 2020-02-04
 */
@RestController
@RequestMapping("/vehiclesArea")
public class VehiclesAreaController {

    private final static Logger LOGGER = LoggerFactory.getLogger(VehiclesAreaController.class);

    @Autowired
    private VehicleAreaService vehicleAreaService;

    @PostMapping(value = "/getVehicleAreaList")
    @ApiOperation(value = "获取车辆所属地区列表数据", notes = "获取车辆所属地区列表数据", produces = MediaType.APPLICATION_JSON_VALUE, response = RestfulResponse.class)
    public RestfulResponse<List<VehicleArea>> getVehicleAreaList(@RequestBody Map<String, Object> condition) {
        RestfulResponse<List<VehicleArea>> result = new RestfulResponse<>(0, "查询成功", null);
        try {
            result.setData(vehicleAreaService.getVehicleAreaList(condition));
        } catch (BaseException e) {
            LOGGER.error("VehiclesAreaController.getVehicleAreaList ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("VehiclesAreaController.getVehicleAreaList ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, "查询失败", null);
        }
        return result;
    }

}

