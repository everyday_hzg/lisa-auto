package com.lisa.auto.controller.c3;


import com.baomidou.mybatisplus.plugins.Page;
import com.lisa.auto.entity.c3.model.Device;
import com.lisa.auto.service.c3.DeviceService;
import com.lisa.auto.util.basic.BaseException;
import com.lisa.util.RestfulResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * <p>
 * 设备信息表 前端控制器
 * </p>
 *
 * @author hxh
 * @since 2019-12-24
 */
@RestController
@RequestMapping(value = "/device", produces = MediaType.APPLICATION_JSON_VALUE)
@Api(value = "设备管理", description = "设备管理接口", tags = {"设备管理接口"})
public class DeviceController {

    private static final Logger LOGGER = LoggerFactory.getLogger(DeviceController.class);

    @Autowired
    private DeviceService deviceService;

    @PostMapping(value = "/getDevicesList")
    @ApiOperation(value = "获取制造商列表数据", notes = "获取制造商列表数据", produces = MediaType.APPLICATION_JSON_VALUE, response = RestfulResponse.class)
    public RestfulResponse<Page<Device>> getDevicesList(@RequestBody Page<Device> page) {
        RestfulResponse<Page<Device>> result = new RestfulResponse<>(0, "查询成功", null);
        try {
            result.setData(deviceService.getDevicesList(page));
        } catch (BaseException e) {
            LOGGER.error("DeviceController.getDevicesList ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("DeviceController.getDevicesList ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, "查询失败", null);
        }
        return result;
    }

    @PostMapping(value = "/getDeviceDetail")
    @ApiOperation(value = "取得设备详情", notes = "取得设备详情", response = RestfulResponse.class)
    public RestfulResponse getDeviceDetailFromC3(@RequestParam("no") String no) {
        RestfulResponse result = new RestfulResponse<>(0, "查询成功", null);
        try {
            result.setData(deviceService.getDeviceDetailFromC3(no));
        } catch (BaseException e) {
            LOGGER.error("DeviceController.getDeviceDetailFromC3 ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("DeviceController.getDeviceDetailFromC3 ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, "查询失败", null);
        }
        return result;
    }

    @PostMapping(value = "/createDevice")
    @ApiOperation(value = "新增设备", notes = "新增设备", response = RestfulResponse.class)
    public RestfulResponse createDevice(@RequestBody Device device) {
        RestfulResponse result = new RestfulResponse(0, "新建成功", null);
        try {
//            deviceService.createDeviceToC3(device);
            deviceService.addDevice(device);
        } catch (BaseException e) {
            LOGGER.error("DeviceController.createDevice ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("DeviceController.createDevice ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, "新增失败", null);
        }
        return result;
    }

    @PostMapping(value = "/updateDevice")
    @ApiOperation(value = "更新设备", notes = "更新设备", response = RestfulResponse.class)
    public RestfulResponse updateDevice(@RequestBody Device device) {
        RestfulResponse result = new RestfulResponse(0, "新建成功", null);
        try {
            deviceService.updateDevice(device);
//            deviceService.updateDeviceToC3(device);
        } catch (BaseException e) {
            LOGGER.error("DeviceController.updateDevice ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("DeviceController.updateDevice ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, "更新失败", null);
        }
        return result;
    }

    @PostMapping(value = "/deleteDevice")
    @ApiOperation(value = "删除设备", notes = "删除设备", response = RestfulResponse.class)
    public RestfulResponse deleteDevice(@RequestBody String nos) {
        RestfulResponse result = new RestfulResponse(0, "新建成功", null);
        try {
            deviceService.deleteDevice(nos);
//            deviceService.deleteDeviceToC3(device);
        } catch (BaseException e) {
            LOGGER.error("DeviceController.deleteDevice ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("DeviceController.deleteDevice ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, "删除失败", null);
        }
        return result;
    }

    @PostMapping(value = "/bindingDevice")
    @ApiOperation(value = "绑定设备", notes = "绑定设备", response = RestfulResponse.class)
    public RestfulResponse bindingDevice(@RequestBody Map<String, Object> map) {
        RestfulResponse result = new RestfulResponse(0, "success", null);
        try {
            deviceService.bindingDevice(map);
        } catch (BaseException e) {
            LOGGER.error("DeviceController.bindingDevice ERROR {}", e.getMessage());
            result = new RestfulResponse(-1, e.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("DeviceController.bindingDevice ERROR {}", e.getMessage());
            result = new RestfulResponse(-1, "设备绑定失败", null);
        }
        return result;
    }


}

