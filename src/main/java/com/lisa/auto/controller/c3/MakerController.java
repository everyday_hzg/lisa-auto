package com.lisa.auto.controller.c3;


import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.plugins.Page;
import com.lisa.auto.entity.c3.model.BrandInfo;
import com.lisa.auto.entity.c3.model.Maker;
import com.lisa.auto.service.c3.MakerService;
import com.lisa.auto.util.basic.BaseException;
import com.lisa.util.RestfulResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 制造商信息 前端控制器
 * </p>
 *
 * @author hxh
 * @since 2019-12-20
 */
@RestController
@RequestMapping("/maker")
@Api(value = "制造商管理", description = "制造商管理接口", tags = {"制造商管理接口"})
public class MakerController {

    private final static Logger LOGGER = LoggerFactory.getLogger(MakerController.class);

    @Autowired
    private MakerService makerService;

    /**
     * 外部接口获取制造商列表数据
     *
     * @return RestfulResponse
     */
    @ApiOperation(value = "外部接口获取制造商列表数据", notes = "外部接口获取制造商列表数据")
    @RequestMapping(value = "/queryMakersToSave", method = RequestMethod.GET)
    public RestfulResponse queryMakersToSave() {
        RestfulResponse result = new RestfulResponse<>(0, "success", null);
        try {
            List<Maker> makers = makerService.queryMakersToSave();
            result.setData(makers);
        } catch (BaseException e) {
            LOGGER.error("MakerController.queryMakersToSave error:{}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("MakerController.queryMakersToSave error:{}", e.getMessage());
            result = new RestfulResponse<>(-1, "获取制造商列表数据失败", false);
        }
        return result;
    }

    /**
     * 新增制造商
     *
     * @return RestfulResponse
     */
    @ApiOperation(value = "新增制造商", notes = "新增制造商")
    @RequestMapping(value = "/addMaker", method = RequestMethod.POST)
    public RestfulResponse addMaker(@RequestBody Maker maker) {
        LOGGER.info("MakerController.addMaker OTD dtoJSON:{}", JSONObject.toJSONString(maker));
        RestfulResponse result = new RestfulResponse<>(0, "success", null);
        try {
            makerService.addMaker(maker);
        } catch (BaseException e) {
            LOGGER.error("MakerController.addMaker error:{}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("MakerController.addMaker error:{}", e.getMessage());
            result = new RestfulResponse<>(-1, "新增制造商失败", false);
        }
        return result;
    }

    @PostMapping(value = "/getMakersList")
    @ApiOperation(value = "获取制造商列表数据", notes = "获取制造商列表数据", produces = MediaType.APPLICATION_JSON_VALUE, response = RestfulResponse.class)
    public RestfulResponse<List<Maker>> getMakersList(@RequestBody Map<String, Object> condition) {
        RestfulResponse<List<Maker>> result = new RestfulResponse<>(0, "查询成功", null);
        try {
            result.setData(makerService.getMakersList(condition));
        } catch (BaseException e) {
            LOGGER.error("MakerController.getMakersListPage ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("MakerController.getMakersListPage ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, "查询失败", null);
        }
        return result;
    }

    @PostMapping(value = "/getMakersListPage")
    @ApiOperation(value = "获取制造商列表数据（分页）", notes = "获取制造商列表数据（分页）", produces = MediaType.APPLICATION_JSON_VALUE, response = RestfulResponse.class)
    public RestfulResponse<Page<Maker>> getMakersListPage(@RequestBody Page<Maker> page) {
        RestfulResponse<Page<Maker>> result = new RestfulResponse<>(0, "查询成功", null);
        try {
            result.setData(makerService.getMakersListPage(page));
        } catch (BaseException e) {
            LOGGER.error("MakerController.getMakersListPage ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("MakerController.getMakersListPage ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, "查询失败", null);
        }
        return result;
    }

    /**
     * 删除制造商
     *
     * @return RestfulResponse
     */
    @ApiOperation(value = "删除制造商", notes = "删除制造商")
    @RequestMapping(value = "/deleteById", method = RequestMethod.POST)
    public RestfulResponse deleteById(@RequestBody Long id) {
        RestfulResponse result = new RestfulResponse<>(0, "success", null);
        try {
            makerService.deleteById(id);
        } catch (BaseException e) {
            LOGGER.error("MakerController.deleteById error:{}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("MakerController.deleteById error:{}", e.getMessage());
            result = new RestfulResponse<>(-1, "删除失败", false);
        }
        return result;
    }

    /**
     * 禁用制造商
     *
     * @return RestfulResponse
     */
    @ApiOperation(value = "禁用制造商", notes = "禁用制造商")
    @RequestMapping(value = "/forbiddenByIds", method = RequestMethod.POST)
    public RestfulResponse forbiddenByIds(@RequestBody Map<String,Object> map) {
        RestfulResponse result = new RestfulResponse<>(0, "success", null);
        try {
            makerService.forbiddenByIds(String.valueOf(map.get("ids")));
        } catch (BaseException e) {
            LOGGER.error("MakerController.forbiddenByIds error:{}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("MakerController.forbiddenByIds error:{}", e.getMessage());
            result = new RestfulResponse<>(-1, "禁用失败", null);
        }
        return result;
    }

    /**
     * 启用制造商
     *
     * @return RestfulResponse
     */
    @ApiOperation(value = "启用制造商", notes = "启用制造商")
    @RequestMapping(value = "/enableByIds", method = RequestMethod.POST)
    public RestfulResponse enableByIds(@RequestBody Map<String,Object> map) {
        RestfulResponse result = new RestfulResponse<>(0, "success", null);
        try {
            makerService.enableByIds(String.valueOf(map.get("ids")));
        } catch (BaseException e) {
            LOGGER.error("MakerController.enableByIds error:{}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("MakerController.enableByIds error:{}", e.getMessage());
            result = new RestfulResponse<>(-1, "启用失败", null);
        }
        return result;
    }

    /**
     * 修改制造商信息
     *
     * @return RestfulResponse
     */
    @ApiOperation(value = "修改制造商信息", notes = "修改制造商信息")
    @RequestMapping(value = "/updateById", method = RequestMethod.POST)
    public RestfulResponse updateById(@RequestBody Maker maker) {
        LOGGER.info("MakerController.updateById OTD dtoJSON:{}", JSONObject.toJSONString(maker));
        RestfulResponse result = new RestfulResponse<>(0, "success", null);
        try {
            makerService.updateById(maker);
        } catch (BaseException e) {
            LOGGER.error("MakerController.updateById error:{}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("MakerController.updateById error:{}", e.getMessage());
            result = new RestfulResponse<>(-1, "更新制造商信息失败", false);
        }
        return result;
    }

    @PostMapping(value = "/getMakerAndBrandList")
    @ApiOperation(value = "获取制造商品牌分页列表", notes = "获取制造商品牌分页列表数据", produces = MediaType.APPLICATION_JSON_VALUE, response = RestfulResponse.class)
    public RestfulResponse<Page<Maker>> getMakerAndBrandList(@RequestBody Page<Maker> page) {
        RestfulResponse<Page<Maker>> result = new RestfulResponse<>(0, "查询成功", null);
        try {
            result.setData(makerService.getMakerAndBrandList(page));
        } catch (BaseException e) {
            LOGGER.error("MakerController.getMakerAndBrandList ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("MakerController.getMakerAndBrandList ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, "查询失败", null);
        }
        return result;
    }

}

