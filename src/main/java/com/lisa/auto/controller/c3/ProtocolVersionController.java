package com.lisa.auto.controller.c3;


import com.lisa.auto.entity.c3.model.ProtocolVersion;
import com.lisa.auto.service.c3.ProtocolVersionService;
import com.lisa.auto.util.basic.BaseException;
import com.lisa.util.RestfulResponse;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 协议版本信息 前端控制器
 * </p>
 *
 * @author hxh
 * @since 2019-12-24
 */
@RestController
@RequestMapping("/protocolVersion")
public class ProtocolVersionController {

    private final static Logger LOGGER = LoggerFactory.getLogger(ProtocolVersionController.class);

    @Autowired
    private ProtocolVersionService versionService;

    @RequestMapping(value = "/queryListToSave", method = RequestMethod.GET)
    @ApiOperation(value = "外部接口获取设备协议列表", notes = "外部接口获取设备协议列表")
    public RestfulResponse queryListToSave() {
        RestfulResponse result = new RestfulResponse(0,"success",null);
        try {
            List<ProtocolVersion> versionList =  versionService.queryListToSave();
            result.setData(versionList);
        } catch (BaseException e) {
            LOGGER.error("ProtocolVersionController.queryListToSave error:{}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("ProtocolVersionController.queryListToSave error:{}", e.getMessage());
            result = new RestfulResponse<>(-1, "获取失败", null);
        }
        return result;
    }

}

