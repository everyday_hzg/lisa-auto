package com.lisa.auto.controller.c3;


import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.plugins.Page;
import com.lisa.auto.entity.c3.model.CarSeries;
import com.lisa.auto.service.c3.CarSeriesService;
import com.lisa.auto.util.basic.BaseException;
import com.lisa.util.RestfulResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 车系管理 前端控制器
 * </p>
 *
 * @author hxh
 * @since 2019-12-20
 */
@RestController
@RequestMapping("/carSeries")
@Api(value = "车系管理", description = "车系管理接口", tags = {"车系管理接口"})
public class CarSeriesController {

    private final static Logger LOGGER = LoggerFactory.getLogger(CarSeriesController.class);

    @Autowired
    private CarSeriesService carSeriesService;

    @ApiOperation(value = "新增车系", notes = "新增车系")
    @RequestMapping(value = "/addCarSeries", method = RequestMethod.POST)
    public RestfulResponse addCarSeries(@RequestBody CarSeries carSeries) {
        LOGGER.info("CarSeriesController.addCarSeries OTD dtoJSON:{}", JSONObject.toJSONString(carSeries));
        RestfulResponse result = new RestfulResponse(0, "success", null);
        try {
            carSeriesService.addCarSeries(carSeries);
        } catch (BaseException e) {
            LOGGER.error("CarSeriesController.addCarSeries error:{}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("CarSeriesController.addCarSeries error:{}", e.getMessage());
            result = new RestfulResponse(-1, "新增车系失败", null);
        }
        return result;
    }

    @PostMapping(value = "/getCarSeriesListFromC3")
    @ApiOperation(value = "外部接口获取车系列表数据", notes = "外部接口获取车系列表数据", response = RestfulResponse.class)
    public RestfulResponse<List<CarSeries>> getCarSeriesListFromC3(@RequestParam("brandNo") String brandNo) {
        RestfulResponse<List<CarSeries>> result = new RestfulResponse<>(0, "获取成功", null);
        try {
            result.setData(carSeriesService.queryCarSeriesToSave(brandNo));
        } catch (BaseException e) {
            LOGGER.error("CarSeriesController.getCarSeriesListFromC3 ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("CarSeriesController.getCarSeriesListFromC3 ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, "获取失败", null);
        }
        return result;
    }

    @PostMapping(value = "/getCarSeriesList")
    @ApiOperation(value = "获取车系列表数据", notes = "获取车系列表数据", produces = MediaType.APPLICATION_JSON_VALUE, response = RestfulResponse.class)
    public RestfulResponse<List<CarSeries>> getCarSeriesList(@RequestBody Map<String, Object> condition) {
        RestfulResponse<List<CarSeries>> result = new RestfulResponse<>(0, "查询成功", null);
        try {
            result.setData(carSeriesService.getCarSeriesList(condition));
        } catch (BaseException e) {
            LOGGER.error("CarSeriesController.getCarSeriesList ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("CarSeriesController.getCarSeriesList ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, "查询失败", null);
        }
        return result;
    }

    @PostMapping(value = "/getCarSeriesListPage")
    @ApiOperation(value = "获取车系列表数据（分页）", notes = "获取车系列表数据（分页）", produces = MediaType.APPLICATION_JSON_VALUE, response = RestfulResponse.class)
    public RestfulResponse<Page<CarSeries>> getCarSeriesListPage(@RequestBody Page<CarSeries> page) {
        RestfulResponse<Page<CarSeries>> result = new RestfulResponse<>(0, "查询成功", null);
        try {
            result.setData(carSeriesService.getCarSeriesListPage(page));
        } catch (BaseException e) {
            LOGGER.error("CarSeriesController.getCarSeriesListPage ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("CarSeriesController.getCarSeriesListPage ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, "查询失败", null);
        }
        return result;
    }

    @ApiOperation(value = "删除车系", notes = "删除车系")
    @RequestMapping(value = "/deleteById", method = RequestMethod.POST)
    public RestfulResponse deleteById(@RequestBody Long id) {
        LOGGER.error("CarSeriesController.deleteById OTD dtoJSON:{}", JSONObject.toJSONString(id));
        RestfulResponse result = new RestfulResponse(0, "success", null);
        try {
            carSeriesService.deleteById(id);
        } catch (BaseException e) {
            LOGGER.error("CarSeriesController.deleteById error:{}", e.getMessage());
            result = new RestfulResponse(-1, e.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("CarSeriesController.deleteById error:{}", e.getMessage());
            result = new RestfulResponse(-1, "删除车系失败", null);
        }
        return result;
    }

    @ApiOperation(value = "修改车系信息", notes = "修改车系信息")
    @RequestMapping(value = "/updateById", method = RequestMethod.POST)
    public RestfulResponse updateById(@RequestBody CarSeries carSeries) {
        LOGGER.error("CarSeriesController.deleteById OTD dtoJSON:{}", JSONObject.toJSONString(carSeries));
        RestfulResponse result = new RestfulResponse(0, "success", null);
        try {
            carSeriesService.updateById(carSeries);
        } catch (BaseException e) {
            LOGGER.error("CarSeriesController.updateById error:{}", e.getMessage());
            result = new RestfulResponse(-1, e.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("CarSeriesController.updateById error:{}", e.getMessage());
            result = new RestfulResponse(-1, "更新车系失败", null);
        }
        return result;
    }

    @PostMapping(value = "/getSeriesAndModelList")
    @ApiOperation(value = "获取车系车型列表数据", notes = "获取车系车型列表数据", produces = MediaType.APPLICATION_JSON_VALUE, response = RestfulResponse.class)
    public RestfulResponse<Page<CarSeries>> getSeriesAndModelList(@RequestBody Page<CarSeries> page) {
        RestfulResponse<Page<CarSeries>> result = new RestfulResponse<>(0, "查询成功", null);
        try {
            result.setData(carSeriesService.getSeriesAndModelList(page));
        } catch (BaseException e) {
            LOGGER.error("CarSeriesController.getSeriesAndModelList ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("CarSeriesController.getSeriesAndModelList ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, "查询失败", null);
        }
        return result;
    }

    /**
     * 禁用制造商
     *
     * @return RestfulResponse
     */
    @ApiOperation(value = "禁用制造商", notes = "禁用制造商")
    @RequestMapping(value = "/forbiddenByIds", method = RequestMethod.POST)
    public RestfulResponse forbiddenByIds(@RequestBody Map<String,Object> map) {
        RestfulResponse result = new RestfulResponse<>(0, "success", null);
        try {
            carSeriesService.forbiddenByIds(String.valueOf(map.get("ids")));
        } catch (BaseException e) {
            LOGGER.error("CarSeriesController.forbiddenByIds error:{}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("CarSeriesController.forbiddenByIds error:{}", e.getMessage());
            result = new RestfulResponse<>(-1, "禁用失败", null);
        }
        return result;
    }

    /**
     * 启用制造商
     *
     * @return RestfulResponse
     */
    @ApiOperation(value = "启用制造商", notes = "启用制造商")
    @RequestMapping(value = "/enableByIds", method = RequestMethod.POST)
    public RestfulResponse enableByIds(@RequestBody Map<String,Object> map) {
        RestfulResponse result = new RestfulResponse<>(0, "success", null);
        try {
            carSeriesService.enableByIds(String.valueOf(map.get("ids")));
        } catch (BaseException e) {
            LOGGER.error("CarSeriesController.enableByIds error:{}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("CarSeriesController.enableByIds error:{}", e.getMessage());
            result = new RestfulResponse<>(-1, "启用失败", null);
        }
        return result;
    }

}

