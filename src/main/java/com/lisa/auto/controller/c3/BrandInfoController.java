package com.lisa.auto.controller.c3;


import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.plugins.Page;
import com.lisa.auto.entity.c3.model.BrandInfo;
import com.lisa.auto.entity.c3.model.Maker;
import com.lisa.auto.service.c3.BrandInfoService;
import com.lisa.auto.util.basic.BaseException;
import com.lisa.util.RestfulResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 品牌管理信息 前端控制器
 * </p>
 *
 * @author hxh
 * @since 2019-12-20
 */
@RestController
@RequestMapping(value = "/brandInfo", produces = MediaType.APPLICATION_JSON_VALUE)
@Api(value = "品牌管理", description = "品牌管理接口", tags = {"品牌管理接口"})
public class BrandInfoController {

    private final static Logger LOGGER = LoggerFactory.getLogger(BrandInfoController.class);

    @Autowired
    private BrandInfoService brandInfoService;

    /**
     * 新增品牌
     *
     * @return RestfulResponse
     */
    @ApiOperation(value = "新增品牌", notes = "新增品牌")
    @RequestMapping(value = "/addBrand", method = RequestMethod.POST)
    public RestfulResponse addBrand(@RequestBody BrandInfo brandInfo) {
        LOGGER.info("BrandInfoController.addBrand OTD dtoJSON:{}", JSONObject.toJSONString(brandInfo));
        RestfulResponse result = new RestfulResponse<>(0, "success", null);
        try {
            brandInfoService.addBrand(brandInfo);
        } catch (BaseException e) {
            LOGGER.error("BrandInfoController.addBrand error:{}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("BrandInfoController.addBrand error:{}", e.getMessage());
            result = new RestfulResponse<>(-1, "新增品牌失败", null);
        }
        return result;
    }

    /**
     * 批量编辑同一个制造商下的品牌信息
     *
     * @return RestfulResponse
     */
    @ApiOperation(value = "批量编辑同一个制造商下的品牌信息", notes = "批量编辑同一个制造商下的品牌信息")
    @RequestMapping(value = "/brandInfoUpdate", method = RequestMethod.POST)
    public RestfulResponse brandInfoUpdate(@RequestBody Maker maker) {
        LOGGER.info("BrandInfoController.brandInfoUpdate OTD dtoJSON:{}", JSONObject.toJSONString(maker));
        RestfulResponse result = new RestfulResponse<>(0, "success", null);
        try {
            brandInfoService.brandInfoUpdate(maker);
        } catch (BaseException e) {
            LOGGER.error("BrandInfoController.brandInfoUpdate error:{}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("BrandInfoController.brandInfoUpdate error:{}", e.getMessage());
            result = new RestfulResponse<>(-1, "新增品牌失败", null);
        }
        return result;
    }

    @PostMapping(value = "/getBrandsListFromC3")
    @ApiOperation(value = "外部接口获取品牌列表数据", notes = "外部接口获取品牌列表数据", response = RestfulResponse.class)
    public RestfulResponse<List<BrandInfo>> getBrandsListFromC3(@RequestParam("makerNo") String makerNo) {
        RestfulResponse<List<BrandInfo>> result = new RestfulResponse<>(0, "获取成功", null);
        try {
            result.setData(brandInfoService.queryBrandsToSave(makerNo));
        } catch (BaseException e) {
            LOGGER.error("BrandInfoController.getBrandsListFromC3 ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("BrandInfoController.getBrandsListFromC3 ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, "获取失败", null);
        }
        return result;
    }

    @PostMapping(value = "/getBrandsList")
    @ApiOperation(value = "获取品牌列表数据", notes = "获取品牌列表数据", produces = MediaType.APPLICATION_JSON_VALUE, response = RestfulResponse.class)
    public RestfulResponse<List<BrandInfo>> getBrandsList(@RequestBody Map<String, Object> condition) {
        RestfulResponse<List<BrandInfo>> result = new RestfulResponse<>(0, "查询成功", null);
        try {
            result.setData(brandInfoService.getBrandsList(condition));
        } catch (BaseException e) {
            LOGGER.error("BrandInfoController.getBrandsList ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("BrandInfoController.getBrandsList ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, "查询失败", null);
        }
        return result;
    }

    @PostMapping(value = "/getBrandsListPage")
    @ApiOperation(value = "获取品牌列表数据（分页）", notes = "获取品牌列表数据（分页）", produces = MediaType.APPLICATION_JSON_VALUE, response = RestfulResponse.class)
    public RestfulResponse<Page<BrandInfo>> getBrandsListPage(@RequestBody Page<BrandInfo> page) {
        RestfulResponse<Page<BrandInfo>> result = new RestfulResponse<>(0, "查询成功", null);
        try {
            result.setData(brandInfoService.getBrandsListPage(page));
        } catch (BaseException e) {
            LOGGER.error("BrandInfoController.getBrandsListPage ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("BrandInfoController.getBrandsListPage ERROR {}", e.getMessage());
            result = new RestfulResponse<>(-1, "查询失败", null);
        }
        return result;
    }

    /**
     * 删除品牌
     *
     * @return RestfulResponse
     */
    @ApiOperation(value = "删除品牌", notes = "删除品牌")
    @RequestMapping(value = "/deleteByIds", method = RequestMethod.POST)
    public RestfulResponse deleteByIds(@RequestBody Map<String,Object> map) {
        RestfulResponse result = new RestfulResponse<>(0, "success", null);
        try {
            brandInfoService.deleteByIds(String.valueOf(map.get("ids")));
        } catch (BaseException e) {
            LOGGER.error("BrandInfoController.deleteByIds error:{}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("BrandInfoController.deleteByIds error:{}", e.getMessage());
            result = new RestfulResponse<>(-1, "删除失败", null);
        }
        return result;
    }

    /**
     * 禁用品牌
     *
     * @return RestfulResponse
     */
    @ApiOperation(value = "禁用品牌", notes = "禁用品牌")
    @RequestMapping(value = "/forbiddenByIds", method = RequestMethod.POST)
    public RestfulResponse forbiddenByIds(@RequestBody Map<String,Object> map) {
        RestfulResponse result = new RestfulResponse<>(0, "success", null);
        try {
            brandInfoService.forbiddenByIds(String.valueOf(map.get("ids")));
        } catch (BaseException e) {
            LOGGER.error("BrandInfoController.forbiddenByIds error:{}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("BrandInfoController.forbiddenByIds error:{}", e.getMessage());
            result = new RestfulResponse<>(-1, "禁用失败", null);
        }
        return result;
    }

    /**
     * 启用品牌
     *
     * @return RestfulResponse
     */
    @ApiOperation(value = "启用品牌", notes = "启用品牌")
    @RequestMapping(value = "/enableByIds", method = RequestMethod.POST)
    public RestfulResponse enableByIds(@RequestBody Map<String,Object> map) {
        RestfulResponse result = new RestfulResponse<>(0, "success", null);
        try {
            brandInfoService.enableByIds(String.valueOf(map.get("ids")));
        } catch (BaseException e) {
            LOGGER.error("BrandInfoController.enableByIds error:{}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("BrandInfoController.enableByIds error:{}", e.getMessage());
            result = new RestfulResponse<>(-1, "启用失败", null);
        }
        return result;
    }

    /**
     * 修改品牌信息
     *
     * @return RestfulResponse
     */
    @ApiOperation(value = "修改品牌信息", notes = "修改品牌信息")
    @RequestMapping(value = "/updateById", method = RequestMethod.POST)
    public RestfulResponse updateById(@RequestBody BrandInfo brandInfo) {
        LOGGER.info("BrandInfoController.updateById OTD dtoJSON:{}", JSONObject.toJSONString(brandInfo));
        RestfulResponse result = new RestfulResponse<>(0, "success", null);
        try {
            brandInfoService.updateById(brandInfo);
        } catch (BaseException e) {
            LOGGER.error("BrandInfoController.updateById error:{}", e.getMessage());
            result = new RestfulResponse<>(-1, e.getMessage(), null);
        } catch (Exception e) {
            LOGGER.error("BrandInfoController.updateById error:{}", e.getMessage());
            result = new RestfulResponse<>(-1, "更新品牌信息失败", null);
        }
        return result;
    }

}

